### Builder Pattern

* 意图
    * 将一个复杂对象的创建与其表示分离，使得同样的构建构成可以创建不同的表示
    * 常常用于一个类有多个参数，其中有若干的可选参数的情况
* 适用性
    * 当创建复杂的算法应该独立于该对象的组成部分以及它们的装配方式时，
        当构建过程必须允许被构建对象又不同表示时

* 构成
    
    ![](../imgs/builder_composition.png)