### Factory Method Pattern

注意：与静态工厂方法不同（见java/efficient_java/chapter2/item1）

* 意图
    * 定义一个用于创建对象的接口，让子类决定实例化哪一个类，也就是说
        **factory method就是将一个类的实例化推迟到其子类**
        
* 适用性
    * 当一个类不知道其必须创建哪一个类对象时
    * 当一个类希望由其子类来指定它所创建的对象时
    * 当一个类创建对象的职责委托给多个帮助子类中的某一个，并且你希望将哪一个
        帮助子类是代理者这一信息局部化的时候。
    
* 构成

    ![](../imgs/fm_composition.png)
    