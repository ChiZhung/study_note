### Visitor Pattern

* 意图
    
    ![](../imgs/visitor_intent.png)
* 适用性

    ![](../imgs/visitor_adapt.png)
    
* 构成
    
    ![](../imgs/visitor_composition.png)
    
也就是元素类型数目固定，使用visitor模式来访问这些元素    
