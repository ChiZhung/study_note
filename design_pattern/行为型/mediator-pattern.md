### Mediator Pattern (终结者模式)

* 意图
    
    ![](../imgs/mediator_intent.png)
* 适用性

    ![](../imgs/mediator_adapt.png)

* 构成

    ![](../imgs/mediator_composition.png)
    