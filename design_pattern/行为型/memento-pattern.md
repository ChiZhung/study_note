### Memento Pattern (备忘录模式)

* 意图
    
    ![](../imgs/memo_intent.png)
* 适用性

    ![](../imgs/memo_adapt.png)

* 构成
    
    ![](../imgs/memento_composition.png)
