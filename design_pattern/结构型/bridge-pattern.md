### Bridge Pattern
对于抽象类或者是接口，一般就是维护一个维度上的变化，比如Photo接口，维护的就是比如实现（GifPhoto，PngPhoto），
单若是又有一个维度就是os的出现，也就是对于不同的照片在不同os上的表示是不一样的，怎么解决这个问题呢？引入桥接模式


* 意图
    * 将抽象与实现部分分离，使得它们都可以独立地变化。也就是抽象定义可以变化没然后实现部分也可以变化
    
* 构成
    * 抽象类（Abstraction）
        * 定义一个抽象类接口并维护一个Implementer（实现抽象类）的对象
    * 扩充抽象类（RefinedAbstraction）
        * 扩充由Abstraction定义的接口
    * 实现抽象类（Implementer）
        * 定义实现类的接口，这个接口不一定要与Abstraction接口完全一致，事实上这两个接口可以完全不同，一般来说，Implementer接口
            仅提供基本操作，而Abstraction定义的接口可能会做更多的更复杂的操作
    * 具体实现类（ConcreteImplementer）
        * 实现Implementer接口
    
    