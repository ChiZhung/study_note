### Flyweight Pattern(享元模式)

* 使用场景
    * 面向对象的思想很好地解决了抽象性的问题，一般不会出现性能上的问题，但是在某些场景下，对象的数量可能会很多，从而导致运行
        时的代价。那么如何取**避免大量细粒度的对象，而又不影响客户程序使用面向对象的方向进行操作**呢？
    * 细粒度对象：就是一个对象容纳的字段比较少，容纳的信息不多
* 意图：
    * 运用共享技术有效地支持大量细粒度的对象
