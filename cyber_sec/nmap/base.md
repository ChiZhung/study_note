### NMAP
* 功能
    1. 主机探测
    2. 端口扫描
    3. 版本检测
    4. 系统检测
    5. 支持探测脚本的编写
* 更新脚本库
    * nmap --script-update
    
#### 功能
1. 主机发现（Host Discovery）
    * -A 全部完整的扫描对于制定的主机
    * -P\*与-s\*这两个一个是用来用作主机发现，一个使用来发现主机上的开放端口
    * 选项-P*
        1. -P0(无ping) 0关1开，就是说不使用ping来发现主机
            * -Pn 功能跟-P0是一样的，就是不进行主机发现
        2. -PS -p \[portlist\] (tcp syn ping) 也就使用使用syn表示为空的tcp报文，默认端口80
            * `nmap -PS80,23,22,21 www.baidu.com`
            * `nmap -PS80,23,22,21 192.168.220.1-254`
            * 就是利用tcp连接三次握手的的第一次syn
            * 仅仅当主机目标端口开放且防火墙规则开启了防止syn攻击
        3. -PA -p \[portlist\] (tvp ack ping)
            * 利用的就是tcp三次握手的最后一次ack
        4. -PU -p \[portlist\] (udp ping)
            * 基于icmp
        5. -PE, -PP, -PM
            * -PE 是基于icmp的echo回应报文，一般的主机都会禁止这个
            * -PP 基于icmp的timestamp报文
            * -PM 基于icmp的mask报文
            * nmap -PP -PE -PM 192.168.1.0/24
            * nmap -PP -PE -PM 192.168.5-255.1-254 192.168.0.2/24
        6. -PR (ARP ping)
            * 一般是对于同一个内网的地址进行扫描，nmap的默认使用策略，若不想使用请输入--send-ip
            * nmap -PS80,443 --send-ip www.baidu.com/24
                * 不使用arp扫描，仅仅使用基于tcp syn的ping扫描
    * -p- 表示扫描所有的端口
    * -n 表示不对ip使用反向域名解析
    * -R表示永远对ip使用反向域名解析
    * -iL \[file\] (input filelist)
    * -iR \[hostnum\] 随机生成指定数目的随机ip，进行扫描
    * --exclude \[host1\[,host2\]\] 不去扫描指定的主机、网络
    * --excludefile \[file\]
    
    * -sS (TCP syn扫描)
    * -sT (tcp connect()扫描)
        * 这种方式更容易被检测到，更容易被防火墙屏蔽
    * -sU （UDP扫描）
        * 扫描速度慢
    * -sA (tcp ack)
        * 探测结果不太准
    * -sW (tcp窗口扫描)
        * 探测结果不太准
    * -sI host1 host2利用僵尸主机1扫描2
    * -sY/sZ