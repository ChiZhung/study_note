#### 自定义NAT
* 自定义一个NAT来管理特殊的VM
* 步骤
    1. 点击管理->全局设定
    2. 点击网络，并添加新的NAT
        
        ![](imgs/nat_add.png)
    3. 配置网段
            
        ![](imgs/nat_set_net.png)
    4. 配置特定的VM的NAT网络

        ![](imgs/nat_vm_set_net.png)
    5. 想在宿主机访问通过VM，需要配置端口转发(先点击管理->全局设定)
        
        ![](imgs/nat_set_port.png)