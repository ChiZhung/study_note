```text
1. 在/etc/docker/daemon.json中写入
{
  "registry-mirrors": [
       "https://mirror.ccs.tencentyun.com"
  ]
}
2. 
    sudo systemctl daemon-reload
    sudo systemctl restart docker
   # Ubuntu16.04 请执行 sudo systemctl restart dockerd 命令
3. 执行 docker info 命令，返回结果中包含以下内容，则说明配置成功。
Registry Mirrors:
 https://mirror.ccs.tencentyun.com
```