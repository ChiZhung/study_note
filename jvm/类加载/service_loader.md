#### Service Provider Framework
* 组成部分：
    1. service interface
        * 定义服务的接口/抽象类
        * 类似JDBC的Connection
    2. provider registration API
        * 使用注册API来将provider（用于提供service实例）的实现注册
        * 类似JDBC的DriverManager.registerDriver(Driver)
            * 将Driver实例（Driver实例可以获取Connection实例）注册
    3. service access API
        * 获取service实例的API
        * 类似JDBC的DriverManager.getConnection(String url)
            * 使用已注册的Driver来获取Connection
    4. (可选)service provider interface
        * 定义的provider接口，其实例用于获取服务实例
        * 类似JDBC的Driver接口

#### ServiceLoader类
* 而ServiceLoader用于加载那些实现了服务接口/抽象类的子类实例
* e.g.
    ```java
    // 加载所有实现了Driver服务的子类
    ServiceLoader<Driver> serviceLoader = ServiceLoader.load(Driver.class);
    Iterator<Driver> iterator = serviceLoader.iterator();
    while (iterator.hasNext()) {
        Driver driver = iterator.next();
        System.out.println(driver.getClass());
    }
    ```
* 注意想通过ServiceLoader来加载提供者类有以下要求
    1. 提供者类有一个无参数的构造函数
    2. 在所在包的META-INF/services/目录下，提供一个名为服务接口/抽象类的全限定名文件
        * e.g. META-INF/services/java.sql.Driver
    3. 在该文件中提供对服务接口实现的类的全限定名
    ```
    # 在META-INF/services/java.sql.Driver文件中
    com.mysql.jdbc.Driver
    com.mysql.fabric.jdbc.FabricMySQLDriver
    ```
    4. 该文件中忽视空格与tab，注释用#，采用UTF-8编码
    
#### JDBC之mysql举例
* 在老版本的步骤
    1. 通过Class.forName("com.mysql.jdbc.Driver")来完成反射，在这个Driver类的static块中，
        会直接将Driver这个类的实例注册到DriverManager中.
    2. 通过DriverManager.getConnection即可
* 在新版本的步骤（兼容老版本）
    * 世界通过DriverManager.getConnection即可
        * 在DriverManager的static块中，回去加载实现Driver接口的子类，首先通过service loader的方式
            从特定目录中反射**加载实例化子类**，然后在通过sys properties的方式**加载初始化子类**，故二者都存在
            主动使用mysql Driver，故其static语句块会得到使用，并注册到DriverManager中