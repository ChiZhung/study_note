### Copy-on-write Tree(Append-only Tree)
一个盘上数据管理数据结构

一个典型的b+树，key与数据存储在leaf节点（一个page）上
![img.png](img.png)

Copy-on-write Tree每个page是以顺序地在写database的文件中；增加新的page数量等价于增加文件的offset，
同时meta page包含了一个指向root page的指针，以及相关的统计信息；
![img_1.png](img_1.png)
当文件被打开后，通过从前向后scan，查找到一个有效的meta page；比如当更新leaf 8page的数值时，
并不是在page8进行原地数据替换；而是通过append一个新的page在file末尾；同时新page的所有parent page以及root都新增并append到文件
末尾；

![img_2.png](img_2.png)

![img_3.png](img_3.png)

对于这个情况，对于一个page的修改导致了4个new page被append到文件末尾，
这样会造成磁盘的浪费，但是磁盘的顺序读写的性能远大于随机读；此外也不需要一个事务
日志，因为database文件本身就是一个事务日志；