#### MAVEN
* 参见网址
    * https://juejin.im/post/5cbf27a95188250a505c2cd2
* 构建过程
    1. maven clean: 清理旧的class字节码
    2. maven compile: 编译class字节码
    3. maven test: 测试执行test代码
    4. maven report: 报告测试结果
    5. maven package: 打成jar，war包
    6. maven install: 将包复制到仓库
    7. maven deploy: 将war包部署到servlet容器中
    * 注意：对于mvn有三个生命周期：clean（清除），default（用于构建），site（用于站点生成）
        ，对于每个生命周期都由若干阶段组成
    * 通过maven phrase 来执行某一阶段，，比如就拿default中的install阶段，若执行maven install
         就会执行从default生命周期中从头的阶段process-resource到compile。。。到install，每个生命周期之间
         是彼此独立的,同时每一个阶段通常会绑定mvn，比如对于default生命周期中的package阶段绑定了对应的
         maven-jar-plugin:version:jar，其中这个jar就是目标也可以理解为一个动作。对于每个阶段的自定义绑定
         见网页文档（也就是执行这个阶段做绑定的插件的默认目标）
    * 可以通过maven prefix:goal 来调用指定的prefix插件（maven-prefix-plugin/prefix-maven-plugin）
        的goal目标（动作）
* 依赖范围
    * 依赖范围指的是：在不同依赖范围情况下，是否将相关依赖纳入classpath中
    1. **compile编译依赖范围**
        * 编译，测试，运行（compile，test，runtime）
    2. **provided已提供依赖范围**
        * 编译，测试（compile，test）
    3. **runtime运行依赖范围**
        * 测试，运行（compile，runtime）
        * 使用场景：在本proj中存在调用第三方的SPI，比如mysql的JDBC的驱动，编译时用不到。
    4. **test测试依赖范围**
        * 测试（test）
    5. system系统依赖范围
        * 见网址文档
    6. import导入依赖范围
        * 见网址文档
    * 对于将本项目打jar包，若使用maven-shade插件，它将会把包含运行时范围的第三方jar包打入本proj的jar，
        则所有第三方jar都不包含的就是original-xxx.jar
* 直接依赖与传递依赖
    * 比如A依赖于B，B依赖于C，侧A对C就是传递依赖
    
    ![](imgs/dependence.png)
* 依赖调解
    1. 最短路径优先：就是对于同一个包（不同版本），则优先导入最近路径的那个包
        ```
        A->B->C->X(1.0)
        A->D->X(2.0) // 最短路径
        ```
    2. 当依赖声明位于不同pom时，第一声明者优先原则:就是当路径相同时，导入先声明的
        ```
        A->B->Y(1.0) // 优先
        A->C->Y(2.0)
        ```
        * 注意：若在同一pom文件中声明了两个相同jar包，则采用复写原则，就是采取后面那一个jar
    3. 可选依赖
        * A->B、B->X（可选）、B->Y（可选）。项目 A 依赖于项目 B，项目 B 依赖于项目 X 和 Y。理论上项目 A 中，会把 B、X、Y 项目都依赖进来。但是 X、Y 两个依赖对于 B 来讲可能是互斥的，如 B 是数据库隔离包，支持多种数据库 MySQL、Oracle，在构建 B 项目时，需要这两种数据库的支持，但在使用这个工具包时，只会依赖一个数据库。此时就需要在 B 项目 pom 文件中将 X、Y 声明为可选依赖，如下：
        ```
        <dependency>  
           <groupId>com.X</groupId>  
           <artifactId>X</artifactId>  
           <version>1.0</version>  
           <optionnal>true</optionnal>
        </dependency>
       
        <dependency>  
           <groupId>com.Y</groupId>  
           <artifactId>Y</artifactId>  
           <version>1.0</version>  
           <optionnal>true</optionnal>
        </dependency>
        ```
        * 复制代码使用 optionnal 元素标识以后，只会对当前项目 B 产生影响，当其他的项目依赖 B 项目时，这两个依赖都不会被传递。项目 A 依赖于项目 B，如果实际应用数据库是 X， 则在 A 的 pom 中就需要显式地声明 X 依赖。
* 仓库
    * 仓库分类：包括本地仓库和远程仓库。其中远程仓库包括：私服和中央仓库。搜索构建的顺序：
    1. 本地仓库
    2. maven settings profile 中的 repository；
    3. pom.xml 中 profile 中定义的repository；
    4. pom.xml 中 repositorys (按定义顺序找)；
    5. maven settings mirror；
    6. central 中央仓库；       
    


    