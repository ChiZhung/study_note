### starUML

一个在windows环境下的开源的UML设计工具

下载网址https://download.csdn.net/download/weixin_39306574/11029807

#### 使用startUML构建UML图
* 创建一个package
    
    ![](../imgs/create_a_package.png)
* 创建一个类图

    ![](../imgs/create_a_class-digram.png)
* 创建自己需要的类、接口后创建其方法

    ![](../imgs/create_a_method.png)
* 为需要的添加注释

    ![](../imgs/add_a_note.png)
    
* 添加一个序列图

    ![](../imgs/add_a_seq_digram.png)