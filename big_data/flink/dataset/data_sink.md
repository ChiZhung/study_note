### Data Sinks


data sink用于消费dataset，sink用于存储并但会dataset。data sink的操作可以用OutputFormat来描述，flink内建了许多
output formats，它们封装了在Dataset上的操作

#### 一些封装好的OutputFormat
* writeAsText -> TextOutputFormat
    * 将元素按行按字符串（通过调用toString）写入
* writeAsFormattedText() -> TextOutputFormat
    * 较之于上者通过调用用户自定义的format来实现对元素格式化字符串
* writeAsCsv(...) -> CsvOutputFormat
    * 将tuple按逗号隔开的方式写入值文件，行与字段的分隔符可以自定义。每一个字段的字符串值通过跳用toString方法实现。
* print() / printToErr() / print(String msg) / printToErr(String msg)
    * 打印元素通过调用toString到输出，同时提供一个可选操作：就是调用prefix(msg)来生成输出前缀（常用来区分caller）。
        若并行度大于一，则输出会带上任务的ID。
* write() -> FileOutputFormat
    * 自定义文件输出，支持自定义序列化操作
* output() -> OutputFormat
    * 对于那些不是基于文件的data sinks而言，最通用的方法（比如存储在DB中的）
    
#### 基本
Dataset可以写往多个操作，且程序可以执行额外的转运操作的同时去写或者打印这些数据集

```java
// 标准的data sink方法
// text data
DataSet<String> textData = // [...]

// write DataSet to a file on the local file system
textData.writeAsText("file:///my/result/on/localFS");

// write DataSet to a file on a HDFS with a namenode running at nnHost:nnPort
textData.writeAsText("hdfs://nnHost:nnPort/my/result/on/localFS");

// write DataSet to a file and overwrite the file if it exists
textData.writeAsText("file:///my/result/on/localFS", WriteMode.OVERWRITE);

// tuples as lines with pipe as the separator "a|b|c"
DataSet<Tuple3<String, Integer, Double>> values = // [...]
values.writeAsCsv("file:///path/to/the/result/file", "\n", "|");

// this writes tuples in the text formatting "(a, b, c)", rather than as CSV lines
values.writeAsText("file:///path/to/the/result/file");

// this writes values as strings using a user-defined TextFormatter object
values.writeAsFormattedText("file:///path/to/the/result/file",
    new TextFormatter<Tuple2<Integer, Integer>>() {
        public String format (Tuple2<Integer, Integer> value) {
            return value.f1 + " - " + value.f0;
        }
    });

// 自定义输出格式

DataSet<Tuple3<String, Integer, Double>> myResult = [...]

// write Tuple DataSet to a relational database
myResult.output(
    // build and configure OutputFormat
    JDBCOutputFormat.buildJDBCOutputFormat()
                    .setDrivername("org.apache.derby.jdbc.EmbeddedDriver")
                    .setDBUrl("jdbc:derby:memory:persons")
                    .setQuery("insert into persons (name, age, height) values (?,?,?)")
                    .finish()
    );


// 本地的排序输出
DataSet<Tuple3<Integer, String, Double>> tData = // [...]
DataSet<Tuple2<BookPojo, Double>> pData = // [...]
DataSet<String> sData = // [...]

// sort output on String field in ascending order
tData.sortPartition(1, Order.ASCENDING).print();

// sort output on Double field in descending and Integer field in ascending order
tData.sortPartition(2, Order.DESCENDING).sortPartition(0, Order.ASCENDING).print();

// sort output on the "author" field of nested BookPojo in descending order
pData.sortPartition("f0.author", Order.DESCENDING).writeAsText(...);

// sort output on the full tuple in ascending order
tData.sortPartition("*", Order.ASCENDING).writeAsCsv(...);

// sort atomic type (String) output in descending order
sData.sortPartition("*", Order.DESCENDING).writeAsText(...);

// ****注意****全局的排序输出目前是不支持的
```    
