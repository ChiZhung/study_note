### Flink DataSet API编程指南（Flink1.9）

在dataset上支持的算子包括：filtering，mapping，joining，grouping等

dataset的数据源支持：文件源，本地集合源

dataset处理的结果由sink返回

#### 例子
```java
public class WordCountExample {
    public static void main(String[] args) throws Exception {
        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        DataSet<String> text = env.fromElements(
            "Who's there?",
            "I think I hear them. Stand, ho! Who's there?");

        DataSet<Tuple2<String, Integer>> wordCounts = text
            .flatMap(new LineSplitter())
            .groupBy(0)
            .sum(1);

        wordCounts.print();
    }

    public static class LineSplitter implements FlatMapFunction<String, Tuple2<String, Integer>> {
        @Override
        public void flatMap(String line, Collector<Tuple2<String, Integer>> out) {
            for (String word : line.split(" ")) {
                out.collect(new Tuple2<String, Integer>(word, 1));
            }
        }
    }
}
```

#### 函数中的数据对象处理
* 对象不可重用（默认）
    * 默认情况下，对象的重用是关闭的，这种模式确保了一个函数接受到的总是一个新的对象实例，这种模式
        使得对象的使用更加安全。然而，这样的模式也带来的一定的处理开销，可能带来更多的GC
    * 用户函数如何在数据对象不可重用模式下对输入输出对象的访问
        * 读取输入对象：在一个方法调用中，保证一个输入对象的值不会发生改变，这包括迭代器中的对象。
            注意：对象可能在方法调用结束之后被修改，所以**不要让一个对象跨函数调用**
        * 修改输入对象
            * 你可能修改输入的对象实例
        * emit输入对象，一个输入对象的值可能在其被发射之后被修改。因此，**读取一个发射之后的对象是不安全的** 
        * 读输出对象，一个对象可能被传给收集器或者直接作为方法返回，**读取一个输出对象是不安全的**  
        * 修改输出对象，你可能在一个对象发射之后修改它，并再次发射它
    * **注意点**
        1. 不要保存/读取跨方法调用的输入对象
        2. 在你发射对象之后，不要读取这些对象
* 可重用对象
    * 在对象可重用模式下，flink运行时最小化obj数量，这个模式的启用通过调用ExecutionConfig.enableObjectReuse()
    * 操作与保证及限制
        * 从一般方法的常规参数的读取接受输入元素
            * 从方法作为参数传入的对象，不要在一个函数调用中修改它，对象可能会在方法调用退出后会被修改，跨函数调用的对象是不安全的
        * 从一般方法的可迭代参数的读取接受输入元素
            * 一个迭代器可能会服务一个相同的对象实例多次，在迭代之间记录下对象是不安全的
        * 修改输入对象
            * **一定不要修改输入的对象**，除非输入对象来自MapFunction/FlatMapFunction，MapPartitionFunction, GroupReduceFunction, 
                GroupCombineFunction, CoGroupFunction, and InputFormat.next(reuse)
        * 发射输入对象
            * **一定不要发射输入对象**，除非输入对象来自MapFunction, FlatMapFunction, MapPartitionFunction, GroupReduceFunction, 
                GroupCombineFunction, CoGroupFunction, and InputFormat.next(reuse).
        * 读取输出对象
            * 读取输出对象是不安全的
        * 修改输出对象
            * 你可能在一个对象发射之后修改它，并再次发射它

#### Debug

https://ci.apache.org/projects/flink/flink-docs-release-1.9/zh/dev/batch/#debugging

#### 语义注解
https://ci.apache.org/projects/flink/flink-docs-release-1.9/zh/dev/batch/#semantic-annotations

通过注解，告知系统一个函数输入的哪些字段是函数需要读取计算的，告知哪些字段是不可修改的（即可以随意前向传播）。
语义注解是一个强有力的工具来加速处理，因为这**允许系统来重用排序顺序或者跨操作的分区**，使用语义注解可能最终可以
**使得程序免于不必要的shuffle与sort，极大地提高的系统的性能**

注意：语义注解的使用是可选的，但是绝对是一个强有力的工具，**不正确的语义注解可能会使得错误的结果**。
**若一个算子的行为是不明确的，则推荐的做法是不实用语义注解**

---

支持的语义注解如下：

* Forwarded Fields Annotation
    * 前向字段注解用于输入字段是在一个函数中不做修改的，并前向导出到输出的同一/不一字段上。
  
#### 广播变量（用于单个算子）
https://ci.apache.org/projects/flink/flink-docs-release-1.9/zh/dev/batch/#broadcast-variables

广播变量可以使得一个数据集在一个操作的所有并行实例中都可以访问，通常作为一个辅助数据集，或者数据依赖的参数化。

广播集合通过调用withBroadcastSet(DataSet, String)来完成注册，并通过调用getRuntimeContext().getBroadcastVariable(String)
来在目标算子中访问这个广播集合

```java
// 1. The DataSet to be broadcast
DataSet<Integer> toBroadcast = env.fromElements(1, 2, 3);

DataSet<String> data = env.fromElements("a", "b");

data.map(new RichMapFunction<String, String>() {
    @Override
    public void open(Configuration parameters) throws Exception {
      // 3. Access the broadcast DataSet as a Collection
      Collection<Integer> broadcastSet = getRuntimeContext().getBroadcastVariable("broadcastSetName");
    }


    @Override
    public String map(String value) throws Exception {
        ...
    }
}).withBroadcastSet(toBroadcast, "broadcastSetName"); // 2. Broadcast the DataSet
```

#### 分布式缓存（用于全局）
https://ci.apache.org/projects/flink/flink-docs-release-1.9/zh/dev/batch/#distributed-cache

flink提供像hadoop一样的分布式缓存，对于用户函数的并行实例可以访问本地化的文件，这个功能使得可以用来共享文件，这些文件
包含了静态的外部数据，像是字典，机器学习的回归模型。

缓存工作在场景如下：一个程序在ExecutionEnvironment中以一个特定的缓存名称注册一个HDFS/S3的文件或者目录，当程序执行时，
flink会自动地从remote fs中拷贝到本地fs中。

```java
// 注册文件或者目录到ExecutionEnvironment
ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

// register a file from HDFS
env.registerCachedFile("hdfs:///path/to/your/file", "hdfsFile")

// register a local executable file (script, executable, ...)
env.registerCachedFile("file:///path/to/exec/file", "localExecFile", true)

// define your program and execute
...
DataSet<String> input = ...
DataSet<Integer> result = input.map(new MyMapper());
...
env.execute();

// 从RuntimeContext访问缓存
// extend a RichFunction to have access to the RuntimeContext
public final class MyMapper extends RichMapFunction<String, Integer> {

    @Override
    public void open(Configuration config) {

      // access cached file via RuntimeContext and DistributedCache
      File myFile = getRuntimeContext().getDistributedCache().getFile("hdfsFile");
      // read the file (or navigate the directory)
      ...
    }

    @Override
    public Integer map(String value) throws Exception {
      // use content of cached file
      ...
    }
}
```

#### 向函数中传递参数
https://ci.apache.org/projects/flink/flink-docs-release-1.9/zh/dev/batch/#passing-parameters-to-functions

将参数传递给函数，可以通过构造器或者调用withParameters(Configuration)方法，参数序列化作为函数对象的一部分，装船到所有的并行任务实例。

```java
// 通过构造器

DataSet<Integer> toFilter = env.fromElements(1, 2, 3);

toFilter.filter(new MyFilter(2));

private static class MyFilter implements FilterFunction<Integer> {

  private final int limit;

  public MyFilter(int limit) {
    this.limit = limit;
  }

  @Override
  public boolean filter(Integer value) throws Exception {
    return value > limit;
  }
}

// 通过withParameters(Configuration)
DataSet<Integer> toFilter = env.fromElements(1, 2, 3);

Configuration config = new Configuration();
config.setInteger("limit", 2);

toFilter.filter(new RichFilterFunction<Integer>() {
    private int limit;

    @Override
    public void open(Configuration parameters) throws Exception {
      limit = parameters.getInteger("limit", 0);
    }

    @Override
    public boolean filter(Integer value) throws Exception {
      return value > limit;
    }
}).withParameters(config);
```

* 也可以通过全局的ExecutionConfig
    ```java
    // 注册
    Configuration conf = new Configuration();
    conf.setString("mykey","myvalue");
    final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
    env.getConfig().setGlobalJobParameters(conf);
  
    // 取
    public static final class Tokenizer extends RichFlatMapFunction<String, Tuple2<String, Integer>> {
    
        private String mykey;
        @Override
        public void open(Configuration parameters) throws Exception {
          super.open(parameters);
          ExecutionConfig.GlobalJobParameters globalParams = getRuntimeContext().getExecutionConfig().getGlobalJobParameters();
          Configuration globConf = (Configuration) globalParams;
          mykey = globConf.getString("mykey", null);
        }
        // ... more here ...
    }
    ``` 
   