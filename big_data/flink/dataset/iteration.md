### 迭代（Iteration）

为了实现在大数据领域中对数据筛选我们有用的信息，向迭代算法就变得十分重要了，随着在超大数据集上执行这类算法的
迫切性，这就要求以高并发的方式去迭代遍历数据集。

Flink程序通过定义一个Step Function来实现可迭代所发，在一个特定的迭代算子中嵌入它。这个算子有两个变种：
一是Iterate，而是DeltaIterate。这两个算子都可以在当前的迭代中重复调用step func直到到达一个特定的终止条件。

![](imgs/iterate_delta-iterate.png)

#### Iterate Operator

Iterate Operator覆盖了迭代的简单类型：在每一次迭代中，step func会消费整个输入（也就是上一次迭代的输出/初始数据集），
并计算partial solution的下一个版本（e.g. map, reduce, join, etc..）

![](imgs/iterate_operator.png)

* 迭代输入：从数据集或者上一个算子获取的初始输入
* step func：step func会在每一次迭代中执行，这是由像map,reduce,join等这样的算子组成的一个任意数据流，这依赖于你手动指定的任务。
* next partial solution：在每一次迭代中，step func返回的输出会回灌到下一次迭代
* 迭代结果：将最后一次迭代的输出写到data sink，或者作为输入写到下一个算子

----

**指定迭代的终结条件**

* 迭代的最大次数
* 自定义聚集器或者汇聚器：迭代允许自定义一个聚集器于汇聚标准比如sum聚集emit记录的数量（聚集器）并若这个数量为0时就终止（汇集标准）

```java
// 迭代的伪代码
IterationState state = getInitialState();

while (!terminationCriterion()) {
	state = step(state);
}

setFinalState(state);
```

---

* 例子：自增数字
    
    ![](imgs/increment_num.png)
    
    * Iteration Input: 1-5这5个数字
    * Step Function: 由单个map函数组成，也就是i++的操作
    * Next Partial Solution: 回灌到下一次迭代
    * Iteration Result: 10次迭代，也就是结果就是11到15
    
#### Delta Iterate Operator

delta iterate operator包含了自增迭代器的例子，自增迭代器是选择性地去修改他们solution中的元素，并更新solution而不是
全部对其进行重新计算

由于并不是每一个元素在solution集合中的都会在每一次迭代中发生改变，这就允许我们去聚焦solution中的热点部分，并将热点部分与冷点
部分隔离开来。经常，solution的大多数相对来说会快速的cool down，后续的迭代仅仅会在一个小的子数据集中进行操作处理。

![](imgs/delta_iterate_operator.png)

* Iteration Input: 初始的workset与solution set从数据源或者上一个算子中读取输入到第一个迭代
* Step Function: step func会在每一次迭代中执行，这是由像map,reduce,join等这样的算子组成的一个任意数据流，这依赖于你手动指定的任务。
* Next Workset/Update Solution Set: The next workset drives the iterative computation and will be fed back into the next iteration. 
    Furthermore, the solution set will be updated and implicitly forwarded (it is not required to be rebuild). Both data sets can be updated by different operators of the step function.
* Iteration Result: 将最后一次迭代的输出写到data sink，或者作为输入写到下一个算子

默认的终结条件是空workset汇集标准与迭代的最大次数。迭代将会在一个产生的next workset为空或者到达了最大迭代次数，也可以自定义聚集器与汇集标准

```java
// delta iterate operator
IterationState workset = getInitialState();
IterationState solution = getInitialSolution();

while (!terminationCriterion()) {
	(delta, workset) = step(workset, solution);

	solution.update(delta)
}

setFinalState(solution);
```

---
例子：在途中传播最小值

![](imgs/prop_min.png)

在本例中，每一个顶点都有一个ID与一个颜色，每一个顶点都会把其ID传给其邻居顶点，**目标**就是将子图中的最小顶点分配给子图中的
每一个顶点。若对于当前顶点，其收到的ID小于自身ID，我们就将自身的颜色改为那个较小ID对应节点的颜色。这个应用可以用在群落分析，
链接的组件计算

初始输入是workset与solution set，在上图中，所有颜色可以看作可更新的solution set，伴随着每一次迭代，最小ID的那个节点的颜色
在子图中扩散。与此同时，work的数量（交换与比较节点ID）在每一次迭代后变少，这就对应于逐渐减少的workset。需要注意到的是，更小的子图
会比大子图更快的汇聚，这个对于workset来说，其会捕捉到这一点。

#### Superstep Sync

在并行环境下，step func的多实例并行执行在迭代状态的不同分区上，多个并行实例在step func的一次计算叫做superstep，这也是需要同步保证的。
所以，一次迭代的所有并行任务需要在下一次superstep将被初始化之前计算superstep。终止标准会在superstep barrier中得到计算。

![](imgs/barrier_sync.png)