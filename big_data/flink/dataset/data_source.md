### Data Sources

数据源从文件或者java集合中创建数据集，创建数据集的一般抽象是InputFormat，一些内建的format通过ExecutionEnvironment.的
方式获取

#### 内建的InputFormat
* 基于文件的
    * readTextFile(path) -> TextInputFormat
        * 基于行以字符串形式读取文件内容
    * readTextFileWithValue(path) -> TextValueInputFormat
        * 基于行的返回，返回的是StringValues实例，这个类是可修改的
    * readCsvFile(path) -> CsvInputFormat
        * 解析以逗号为分隔符（或者其他）的文件，返回以tuple或者POJO的数据集。支持java的基本数据类型
    * readFileOfPrimitives(path, Class) -> PrimitiveInputFormat
        * 解析以换行符（或者其他）作为元素分隔符的文件，文件元素类型比如String，Integer
    * readFileOfPrimitives(path, delimiter, Class) -> PrimitiveInputFormat
* 基于集合的
    * fromCollection(Collection)
    * fromCollection(Iterator, Class)
    * fromElements(T ...)
    * fromParallelCollection(SplittableIterator, Class)
    * generateSequence(from, to)
        * 以给定的间隔生生序列，并行的
* 通用的
    * readFile(inputFormat, path) -> FileInputFormat
    * createInput(inputFormat) -> InputFormat
        * 接受一个通用的输入格式
```java
ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

// read text file from local files system
DataSet<String> localLines = env.readTextFile("file:///path/to/my/textfile");

// read text file from a HDFS running at nnHost:nnPort
DataSet<String> hdfsLines = env.readTextFile("hdfs://nnHost:nnPort/path/to/my/textfile");

// read a CSV file with three fields
DataSet<Tuple3<Integer, String, Double>> csvInput = env.readCsvFile("hdfs:///the/CSV/file")
	                       .types(Integer.class, String.class, Double.class);

// read a CSV file with five fields, taking only two of them
DataSet<Tuple2<String, Double>> csvInput = env.readCsvFile("hdfs:///the/CSV/file")
                               .includeFields("10010")  // take the first and the fourth field
	                       .types(String.class, Double.class);

// read a CSV file with three fields into a POJO (Person.class) with corresponding fields
DataSet<Person>> csvInput = env.readCsvFile("hdfs:///the/CSV/file")
                         .pojoType(Person.class, "name", "age", "zipcode");

// read a file from the specified path of type SequenceFileInputFormat
DataSet<Tuple2<IntWritable, Text>> tuples =
 env.createInput(HadoopInputs.readSequenceFile(IntWritable.class, Text.class, "hdfs://nnHost:nnPort/path/to/file"));

// creates a set from some given elements
DataSet<String> value = env.fromElements("Foo", "bar", "foobar", "fubar");

// generate a number sequence
DataSet<Long> numbers = env.generateSequence(1, 10000000);

// Read data from a relational database using the JDBC input format
DataSet<Tuple2<String, Integer> dbData =
    env.createInput(
      JDBCInputFormat.buildJDBCInputFormat()
                     .setDrivername("org.apache.derby.jdbc.EmbeddedDriver")
                     .setDBUrl("jdbc:derby:memory:persons")
                     .setQuery("select name, age from persons")
                     .setRowTypeInfo(new RowTypeInfo(BasicTypeInfo.STRING_TYPE_INFO, BasicTypeInfo.INT_TYPE_INFO))
                     .finish()
    );
// 注意： 需要提供数据类型
```
