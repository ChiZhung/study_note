### Data Types
在flink1.9之前，Flink’s Table & SQL API data types与TypeInformation紧密耦合，但是对于TypeInformation
其是用来描述所有要序列化的对象类型，然而TypeInformation并不是设计用来代表一个实际jvm类的独立逻辑类型

在一点九版本开始，对于Table & SQL API使用一个新的类型系统来作为一个长期方案，并不是每一个planner与数据类型的组合都是支持的。

注意：在使用一个数据类型之前，请见[planner的兼容性](https://ci.apache.org/projects/flink/flink-docs-release-1.9/zh/dev/table/types.html#planner-compatibility)
与限制

* 数据类型举例：
    * INT, INT NOT NULL, INTERVAL DAY TO SECOND(3)
    * 所有预定义的数据类型见[here](https://ci.apache.org/projects/flink/flink-docs-release-1.9/zh/dev/table/types.html#list-of-data-types)
* 数据类型实例的作用
    * 声明一个逻辑类型
    * 告知planner实际物理代表
     