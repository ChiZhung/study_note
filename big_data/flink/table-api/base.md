### Table API and SQL

一般使用的情况比如：使用CEP（complex extracting processing(https://ci.apache.org/projects/flink/flink-docs-release-1.9/dev/libs/cep.html)）,
从data stream中数据筛选，然后使用table api来分析这个模式，或者你可以scan，filter，聚集一个批表使用一个sql查询

#### 依赖结构
* flink-table-common: 一个公共的用自定义函数，格式等来拓展表生态的模块
* flink-table-api-java: 一个纯净的表程序
* flink-table-api-scala：
* flink-table-api-java-bridge: 有着data stream与data set api支持
* flink-table-api-scala-bridge：
* flink-table-planner: The table program planner and runtime.
  flink-table-uber: 将上述的模块打成一个发行版jar，这个jar位于flink目录的opt目录下，有需要时可以移到lib目录下
  
#### 依赖
* 定义table & sql的管线
```xml
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-table-planner_2.11</artifactId>
  <version>1.9.0</version>
</dependency>
```
* 根据不同的编程语言，需要添加的
```xml
<!--java-->
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-table-api-java-bridge_2.11</artifactId>
  <version>1.9.0</version>
</dependency>
<!--scala-->
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-table-api-scala-bridge_2.11</artifactId>
  <version>1.9.0</version>
</dependency>
```
* 由于表生态内部有些实现是依赖于scala的，所以添加以下为了流式或者批式
```xml
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-streaming-scala_2.11</artifactId>
  <version>1.9.0</version>
</dependency>
```
* 若想要实现一个自定义格式用于与kafka或者一系列自定义函数交互的话，请添加以下依赖
```xml
<dependency>
  <groupId>org.apache.flink</groupId>
  <artifactId>flink-table-common</artifactId>
  <version>1.9.0</version>
</dependency>
```

#### 概念与通用API
* Table
    * 服务于输入，查询的输出
    
* 两种planner（旧的planner与blink planner）
    * blink把批式对待成特殊的流式，像这样的话，table跟dataset之间的转换是不允许的，批式job不可以转为dataset程序但是
        可以转为datastream程序（像流式job一样）
    * blink planner不支持BatchTableSource，代替的是，使用StreamTableSource
    * blink panner仅仅支持new Catalog不支持过时的ExternalCatalog
    * FilterableTableSource的实现对于老的planner与blink planner来说是不兼容的，旧的planner是将PlannerExpressions
        push down FilterableTableSource而对于blink planner来说，其是Expressions
    * 基于string kv的配置仅仅用于blink planner
    * 在两个planner中的PlannerConfig的实现CalciteConfig是不同的
    * 在TableEnvironment而不是StreamTableEnvironment下，blink planner会把多sink优化入一个DAG中，旧的planner一个sink在一个DAG中
    * 旧的planner不支持catalog统计，但是blink planner支持

#### Table API and SQL程序的结构
* 对于流式和批式，使用的是同一个模式
    ```java
    // create a TableEnvironment for specific planner batch or streaming
    TableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section
    
    // register a Table
    tableEnv.registerTable("table1", ...)            // or
    tableEnv.registerTableSource("table2", ...);     // or
    tableEnv.registerExternalCatalog("extCat", ...);
    // register an output Table
    tableEnv.registerTableSink("outputTable", ...);
    
    // create a Table from a Table API query
    Table tapiResult = tableEnv.scan("table1").select(...);
    // create a Table from a SQL query
    Table sqlResult  = tableEnv.sqlQuery("SELECT ... FROM table2 ... ");
    
    // emit a Table API result Table to a TableSink, same for SQL result
    tapiResult.insertInto("outputTable");
    
    // execute
    tableEnv.execute("java_job");
    ```
  * 注意：table api跟sql可以简单地嵌入datastream跟dataset程序中
#### [Old planner与Blink planner](https://www.jianshu.com/p/770248b29338)
![](imgs/blink-planner_old-planner.png)

在 Flink Table 的新架构中，有两个查询处理器：Flink Query Processor 和 Blink Query Processor，分别对应两个Planner，
我们称之为 Old Planner 和 Blink Planner。查询处理器是 Planner 的具体实现， 通过parser(解析器)、optimizer(优化器)、codegen(代码生成技术)等流程
将 Table API & SQL作业转换成 Flink Runtime 可识别的 Transformation DAG (由Transformation组成的有向无环图，表示作业的转换逻辑)，
最终由 Flink Runtime 进行作业的调度和执行。

old planner将批流分开处理，对于blink planner将批流统一。

Flink 的查询处理器（old planner）针对流计算和批处理作业有不同的分支处理，流计算作业底层的 API 是 DataStream API， 批处理作业底层的 API 是 DataSet API；
而 Blink 的查询处理器（blink planner）则实现流批作业接口的统一，底层的 API 都是Transformation。


#### 创建一个TableEnvironment
* TableEnvironment是Table API & SQL地核心概念，其负责：
    * 在内部catalog中注册一个table
    * 注册一个外部catalog
    * 执行sql查询
    * 注册用于自定义的函数
    * 将datastream/dataset转运为table
    * 拿着一个ExecutionEnvironment或者StreamExecutionEnvironment的引用

* 一个表总是绑定一个特定的TableEnvironment，不可以在同一个查询中将不同TableEnvironments的表进行combine，比如执行join，union
* 通过调用静态方法BatchTableEnvironment.create() or StreamTableEnvironment.create()来创建一个TableEnvironment，可以带可选的TableConfig。
    TableConfig用于配置TableEnvironment或者自定义查询优化与翻译处理
```java
// **********************
// FLINK STREAMING QUERY
// **********************
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.java.StreamTableEnvironment;

// 确保使用的特定planner BatchTableEnvironment/StreamTableEnvironment是匹配当前使用的编程语言（注入引入的jar包）
// 默认情况两个planner jar都在classpath下，你应该明确的指明使用哪个planner

// 使用old planner，使用流模式：StreamTableEnvironment
EnvironmentSettings fsSettings = EnvironmentSettings.newInstance().useOldPlanner().inStreamingMode().build();
StreamExecutionEnvironment fsEnv = StreamExecutionEnvironment.getExecutionEnvironment();
StreamTableEnvironment fsTableEnv = StreamTableEnvironment.create(fsEnv, fsSettings);
// or TableEnvironment fsTableEnv = TableEnvironment.create(fsSettings);

// ******************
// FLINK BATCH QUERY
// ******************
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.table.api.java.BatchTableEnvironment;

ExecutionEnvironment fbEnv = ExecutionEnvironment.getExecutionEnvironment();
BatchTableEnvironment fbTableEnv = BatchTableEnvironment.create(fbEnv);

// **********************
// BLINK STREAMING QUERY
// **********************
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.java.StreamTableEnvironment;

StreamExecutionEnvironment bsEnv = StreamExecutionEnvironment.getExecutionEnvironment();
EnvironmentSettings bsSettings = EnvironmentSettings.newInstance().useBlinkPlanner().inStreamingMode().build();
StreamTableEnvironment bsTableEnv = StreamTableEnvironment.create(bsEnv, bsSettings);
// or TableEnvironment bsTableEnv = TableEnvironment.create(bsSettings);

// ******************
// BLINK BATCH QUERY
// ******************
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;

EnvironmentSettings bbSettings = EnvironmentSettings.newInstance().useBlinkPlanner().inBatchMode().build();
TableEnvironment bbTableEnv = TableEnvironment.create(bbSettings);
```

* 注意：若仅仅只有一个planner的jar位于lib目录下，你可以使用useAnyPlanner方法来常见一个特定的EnvironmentSettings

---

#### 在catalog中注册table
一个TableEnvironment包含着一个table的目录（catalog），这个表由name来注册，对于table有两个类型：一个是input table，一个是
output table。对于输入表，其可以被table api跟sql引用，并提供输入数据。输出表可以用于将table api跟sql的结果发射到外部系统中。

* 一个输入表可以从多种不同的源注册：
    * 一个已经存在的table对象，通常是table api跟sql查询的结果
    * 一个TableSource，访问外部数据，像是一个文件，数据库，消息系统
    * 一个datastream（stream job），dataset（仅仅支持old planner翻译batch job）
* 输出表的注册使用TableSink

#### 注册一个表
```java
// get a TableEnvironment
TableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

// table is the result of a simple projection query 
Table projTable = tableEnv.scan("X").select(...);

// register the Table projTable as table "projectedTable"
tableEnv.registerTable("projectedTable", projTable);
```
 
一个已经注册了的表像关系型数据库一样被对待成一个视图，若多个查询引用同一个表，那么这些查询将会被内联并执行多次，注册了的表结果是不会共享的。

#### 注册一个表源
TableSource提供了一个访问外部数据的方式，外部数据存储比如mysql，hbase，一个特定编码的文件比如csv，一个消息系统比如kafka

flink旨在提供一个TableSource作为公共的数据格式

```java
// get a TableEnvironment
TableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

// create a TableSource
TableSource csvSource = new CsvTableSource("/path/to/file", ...);

// register the TableSource as table "CsvTable"
tableEnv.registerTableSource("CsvTable", csvSource);

// 注意：对于使用blink planner的TableEnvironment，其仅仅接受StreamTableSource, LookupableTableSource and InputFormatTableSource。
// 且对于blink planner在处理批时，StreamTableSource必须是有界的。
```

#### 注册一个TableSink
一个已经注册了的TableSink可以用来发射table api and sql在外部数据存储系统上的查询结果。

```java
// get a TableEnvironment
TableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

// create a TableSink
TableSink csvSink = new CsvTableSink("/path/to/file", ...);

// define the field names and types
String[] fieldNames = {"a", "b", "c"};
TypeInformation[] fieldTypes = {Types.INT, Types.STRING, Types.LONG};

// register the TableSink as table "CsvSinkTable"
tableEnv.registerTableSink("CsvSinkTable", fieldNames, fieldTypes, csvSink);
```
---

#### 注册一个外部的目录
一个外部的目录提供外部数据库/表的信息，像其name，schema，统计与信息，用来告知如何访问存储在外部数据库/表，文件中的数据

一个外部目录可以直接通过接口的实现来创建，并注册到TableEnvironment

```java
// get a TableEnvironment
TableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

// create an external catalog
ExternalCatalog catalog = new InMemoryExternalCatalog();

// register the ExternalCatalog catalog
tableEnv.registerExternalCatalog("InMemCatalog", catalog);
```

一旦注册到TableEnvironment，所有的表就被定义在ExternalCatalog中，并可以通过table api or sql来查询访问

当前，flink提供了InMemoryExternalCatalog来作为demo并测试，然而，ExternalCatalog interface can also be used to connect catalogs 
like HCatalog or Metastore to the Table API.
    
注意：blink planner不支持外部目录

---

#### 查询一个table
* Table API
    * 不像sql由一个string标示查询，而是通过链式方法组成的
* API基于Table类，代表这一个table（流或者是批），并提供相应的方法来作用于相关的算子。这些方法返回一个Table对象，其代表着在输入Table
    上施加操作后返回的结果。
    
```java
// get a TableEnvironment
TableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

// register Orders table

// scan registered Orders table
Table orders = tableEnv.scan("Orders");
// compute revenue for all customers from France
Table revenue = orders
  .filter("cCountry === 'FRANCE'")
  .groupBy("cID, cName")
  .select("cID, cName, revenue.sum AS revSum");

// emit or convert Table
// execute query
```    

#### SQL
```java
// get a TableEnvironment
TableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

// register Orders table

// compute revenue for all customers from France
Table revenue = tableEnv.sqlQuery(
    "SELECT cID, cName, SUM(revenue) AS revSum " +
    "FROM Orders " +
    "WHERE cCountry = 'FRANCE' " +
    "GROUP BY cID, cName"
  );

// emit or convert Table
// execute query

// update and insert
// get a TableEnvironment
TableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

// register "Orders" table
// register "RevenueFrance" output table

// compute revenue for all customers from France and emit to "RevenueFrance"
tableEnv.sqlUpdate(
    "INSERT INTO RevenueFrance " +
    "SELECT cID, cName, SUM(revenue) AS revSum " +
    "FROM Orders " +
    "WHERE cCountry = 'FRANCE' " +
    "GROUP BY cID, cName"
  );

// execute query
```

* 可以简单的将table api语法与sql相结合，因为它们最终都会返回table对象
    * 一个由sql返回的table对象，可以在其上使用table api进行查询
    * 将结果table注册到TableEnvironment，sql查询可以定义在一个table api查询的结果之上，并可以从sql的from语句中引用它。
    
#### 发射Table
听过定一个TableSink来发射一个Table，TableSink是一个广泛支持各种文件格式/存储系统/消息系统的通用接口，一个批式Table仅能被
BatchTableSink来写入，对于streaming table可以使用AppendStreamTableSink, a RetractStreamTableSink, or an UpsertStreamTableSink。

通过Table.insertInto(String tableName)来将Table发射给一个注册好的TableSink中，这个方法会lookup目录，并取出同名的table，并确保table的
schema与table sink中的schema一致

```java
// get a TableEnvironment
TableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

// create a TableSink
TableSink sink = new CsvTableSink("/path/to/file", fieldDelim = "|");

// register the TableSink with a specific schema
String[] fieldNames = {"a", "b", "c"};
TypeInformation[] fieldTypes = {Types.INT, Types.STRING, Types.LONG};
tableEnv.registerTableSink("CsvSinkTable", fieldNames, fieldTypes, sink);

// compute a result Table using Table API operators and/or SQL queries
Table result = ...
// emit the result Table to the registered TableSink
result.insertInto("CsvSinkTable");

// execute the program
```

#### 翻译并执行一个查询
对于old/blink planner，翻译/执行查询是不同的
* 对于old planner
    * table api与sql被翻译成datastream or dataset程序，依赖与其输入是否为流式or批式来确定。
        一个查询内部由一个逻辑查询plan标示，其翻译有两个阶段：
        1. 逻辑plan优化
        2. 翻译成datastream/dataset程序
    * 一个table api or sql查询被翻译当：
        1. 一个table被发射给一个tablesink，比如：Table.insertInto()被调用
        2. 一个指定的sql update，比如：TableEnvironment.sqlUpdate() is called
        3. 一个Table被转成DataStream or DataSet
    * 一旦被翻译，一个table api or sql其实就是像一个常规的Datastream/dataset来处理

* 对于blink planner
    * 两个阶段
    * 对于TableEnvironment and StreamTableEnvironment，这二者的翻译行为是不同的
        1. 对于TableEnvironment，仅仅当TableEnvironment.execute()被执行时，才被翻译，由于TableEnvironment将
            多个sink优化进单个DAG
        2. 对于StreamTableEnvironment，翻译执行行为同old planner
    
#### [将Table APi and SQL 与DataStream and DataSet API整合](https://ci.apache.org/projects/flink/flink-docs-release-1.9/zh/dev/table/common.html#integration-with-datastream-and-dataset-api)

不管是old planner还是blink planner，这二者对于流式API都是可以整合的，仅仅只有old planner可以将table api and sql与
批式API相整合，而blink则不能（注意：下面关于dataset的讨论均在在old planner在批式api的处理）

table api and sql可以简单的与Datastream/dataset程序相整合，比如：先通过一个外部表（e.g. RDBMS）来查询，然后
做一线预处理，比如filter/project/aggregate/join，然后在进一步通过datastream/dataset API（以及任何构建在其上的lib，比如CEP（complex extracting processing） or Gelly）来处理.
此外，对于datastream/dataset的结果也可以使用table API and SQL来处理。

---

#### 将一个datastream/dataset注册为table
A DataStream or DataSet can be registered in a TableEnvironment as a Table.
结果table的schema依赖于注册到DataStream or DataSet中的数据类型，更多可见[数据类型到表schema的映射](https://ci.apache.org/projects/flink/flink-docs-release-1.9/zh/dev/table/common.html#mapping-of-data-types-to-table-schema)

```java
// get StreamTableEnvironment
// registration of a DataSet in a BatchTableEnvironment is equivalent
StreamTableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

DataStream<Tuple2<Long, String>> stream = ...

// register the DataStream as Table "myTable" with fields "f0", "f1"
tableEnv.registerDataStream("myTable", stream);

// register the DataStream as table "myTable2" with fields "myLong", "myString"
tableEnv.registerDataStream("myTable2", stream, "myLong, myString");
```
* 注意：一个DataStream Table一定不可以匹配模式^_DataStreamTable_\[0-9\]+且一个DataSet Table的
    名字也不可以匹配模式^_DataSetTable_\[0-9\]+
    这些模式内部使用的
  
#### 转运DataStream or DataSet到Table
代替将其注册到TableEnvironment，而是直接将其转为一个Table，这就使得执行table api跟sql变得方便

```java
// get StreamTableEnvironment
// registration of a DataSet in a BatchTableEnvironment is equivalent
StreamTableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

DataStream<Tuple2<Long, String>> stream = ...

// Convert the DataStream into a Table with default fields "f0", "f1"
Table table1 = tableEnv.fromDataStream(stream);

// Convert the DataStream into a Table with fields "myLong", "myString"
Table table2 = tableEnv.fromDataStream(stream, "myLong, myString");
```

---

#### 将Table转为DataStream or DataSet
将table转为DataStream or DataSet，你需要指明结果的DataStream or DataSet的数据类型

* Row: 通过table中的位置来映射字段，任意数量的字段，支持null，非类型安全访问
* POJO：字段通过name来映射，任意数量的字段，支持null，类型安全访问
* Case Class: fields are mapped by position, no support for null values, type-safe access.
* Tuple: fields are mapped by position, limitation to 22 (Scala) or 25 (Java) fields, no support for null values, type-safe access.
* Atomic Type: Table must have a single field, no support for null values, type-safe access.

#### table2datastream
一个作为流式查询结果的表是会被动态更新的（比如：新的记录到达一个查询的输入流），一个动态查询被转到到这样一个datastream是
需要编码表的更新（因为在后面Datastream中即会收到旧的数据又会收到修改后的新数据，所以需要对修改的数据进行标示）

* table2datastream的两种模式
    * Append Mode：当动态table仅仅通过insert修改之时，这个模式才起作用：即append insert，对前面emit的记录没有影响
    * Retract Mode：这个模式常被使用，通过一个bool flag来标示记录的改变INSERT/DELETE
    
```java
// get StreamTableEnvironment. 
StreamTableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

// Table with two fields (String name, Integer age)
Table table = ...

// convert the Table into an append DataStream of Row by specifying the class
DataStream<Row> dsRow = tableEnv.toAppendStream(table, Row.class);

// convert the Table into an append DataStream of Tuple2<String, Integer> 
//   via a TypeInformation
TupleTypeInfo<Tuple2<String, Integer>> tupleType = new TupleTypeInfo<>(
  Types.STRING(),
  Types.INT());
DataStream<Tuple2<String, Integer>> dsTuple = 
  tableEnv.toAppendStream(table, tupleType);

// convert the Table into a retract DataStream of Row.
//   A retract stream of type X is a DataStream<Tuple2<Boolean, X>>. 
//   The boolean field indicates the type of the change. 
//   True is INSERT, false is DELETE.
DataStream<Tuple2<Boolean, Row>> retractStream = 
  tableEnv.toRetractStream(table, Row.class);
```

* 更多信息见[动态表](https://ci.apache.org/projects/flink/flink-docs-release-1.9/zh/dev/table/streaming/dynamic_tables.html)

#### table2dataset
```java
// get BatchTableEnvironment
BatchTableEnvironment tableEnv = BatchTableEnvironment.create(env);

// Table with two fields (String name, Integer age)
Table table = ...

// convert the Table into a DataSet of Row by specifying a class
DataSet<Row> dsRow = tableEnv.toDataSet(table, Row.class);

// convert the Table into a DataSet of Tuple2<String, Integer> via a TypeInformation
TupleTypeInfo<Tuple2<String, Integer>> tupleType = new TupleTypeInfo<>(
  Types.STRING(),
  Types.INT());
DataSet<Tuple2<String, Integer>> dsTuple = 
  tableEnv.toDataSet(table, tupleType);
```

#### 数据类型映射到table schema
将数据类型映射到table以两种方式进行：基于字段位置，基于字段名

* 基于字段位置
    * 像tuple, row以及scala的case class可以使用基于位置的映射，对于pojo只能使用基于字段名字的映射。
        字段可以使用映射但是不可以使用as来rename
    * 注意使用基于位置的映射在调用方法一定不要使用名字参数，不然程序为以为你在用基于字段名称的映射
    
```java
// get a StreamTableEnvironment, works for BatchTableEnvironment equivalently
StreamTableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section;

DataStream<Tuple2<Long, Integer>> stream = ...

// convert DataStream into Table with default field names "f0" and "f1"
Table table = tableEnv.fromDataStream(stream);

// convert DataStream into Table with field "myLong" only
Table table = tableEnv.fromDataStream(stream, "myLong");

// convert DataStream into Table with field names "myLong" and "myInt"
Table table = tableEnv.fromDataStream(stream, "myLong, myInt");
```

* 基于字段名的映射
```java
// get a StreamTableEnvironment, works for BatchTableEnvironment equivalently
StreamTableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

DataStream<Tuple2<Long, Integer>> stream = ...

// convert DataStream into Table with default field names "f0" and "f1"
Table table = tableEnv.fromDataStream(stream);

// convert DataStream into Table with field "f1" only
Table table = tableEnv.fromDataStream(stream, "f1");

// convert DataStream into Table with swapped fields
Table table = tableEnv.fromDataStream(stream, "f1, f0");

// convert DataStream into Table with swapped fields and field names "myInt" and "myLong"
Table table = tableEnv.fromDataStream(stream, "f1 as myInt, f0 as myLong");

// ****Tuple***

// get a StreamTableEnvironment, works for BatchTableEnvironment equivalently
StreamTableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

DataStream<Tuple2<Long, String>> stream = ...

// convert DataStream into Table with default field names "f0", "f1"
Table table = tableEnv.fromDataStream(stream);

// convert DataStream into Table with renamed field names "myLong", "myString" (position-based)
Table table = tableEnv.fromDataStream(stream, "myLong, myString");

// convert DataStream into Table with reordered fields "f1", "f0" (name-based)
Table table = tableEnv.fromDataStream(stream, "f1, f0");

// convert DataStream into Table with projected field "f1" (name-based)
Table table = tableEnv.fromDataStream(stream, "f1");

// convert DataStream into Table with reordered and aliased fields "myString", "myLong" (name-based)
Table table = tableEnv.fromDataStream(stream, "f1 as 'myString', f0 as 'myLong'");

// POJO
// get a StreamTableEnvironment, works for BatchTableEnvironment equivalently
StreamTableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

// Person is a POJO with fields "name" and "age"
DataStream<Person> stream = ...

// convert DataStream into Table with default field names "age", "name" (fields are ordered by name!)
Table table = tableEnv.fromDataStream(stream);

// convert DataStream into Table with renamed fields "myAge", "myName" (name-based)
Table table = tableEnv.fromDataStream(stream, "age as myAge, name as myName");

// convert DataStream into Table with projected field "name" (name-based)
Table table = tableEnv.fromDataStream(stream, "name");

// convert DataStream into Table with projected and renamed field "myName" (name-based)
Table table = tableEnv.fromDataStream(stream, "name as myName");

// ROW
// get a StreamTableEnvironment, works for BatchTableEnvironment equivalently
StreamTableEnvironment tableEnv = ...; // see "Create a TableEnvironment" section

// DataStream of Row with two fields "name" and "age" specified in `RowTypeInfo`
DataStream<Row> stream = ...

// convert DataStream into Table with default field names "name", "age"
Table table = tableEnv.fromDataStream(stream);

// convert DataStream into Table with renamed field names "myName", "myAge" (position-based)
Table table = tableEnv.fromDataStream(stream, "myName, myAge");

// convert DataStream into Table with renamed fields "myName", "myAge" (name-based)
Table table = tableEnv.fromDataStream(stream, "name as myName, age as myAge");

// convert DataStream into Table with projected field "name" (name-based)
Table table = tableEnv.fromDataStream(stream, "name");

// convert DataStream into Table with projected and renamed field "myName" (name-based)
Table table = tableEnv.fromDataStream(stream, "name as myName");
```
