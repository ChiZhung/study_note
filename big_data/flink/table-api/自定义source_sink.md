### User-defined Sources & Sinks
https://ci.apache.org/projects/flink/flink-docs-release-1.9/dev/table/sourceSinks.html

TableSource提供了一个对外部系统的访问，当一个table source被注册到table env后，其就可以被table api与sql访问，
Table Sink将table emit到外部存储，TableFactory的具体实现来拆分到不同外部系统连接。一个table factory可以从normalize，
基于string的属性来创建table sources/sinks的配置化实例。这个配置可以使用Descriptor来配置或者是一个外部的YAML配置文件

一个用于流式的TableSource继承StreamTableSource接口，批式继承BatchTableSource接口。也可以同时继承两个接口
```java
TableSource<T> {
  // 返回表的schema，比如table字段的名字与类型，字段类型的定义通过使用Flink's TypeInformation
  public TableSchema getTableSchema();

  // 返回记录的物理类型
  public TypeInformation<T> getReturnType();

  // 返回一个字符串来用描述TableSource，可选的，仅仅是用于展示作用
  public String explainSource();
}
```
TableSource接口将逻辑表schema从数据流/批的真实物理类型拆分出来，所以，所有表schema的字段必须可以映射到其真实的返回类型上，
默认就是基于字段名字


