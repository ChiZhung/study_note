### Data Sinks

用于消费datastream的data sink可以直接将数据导入文件，外部系统，或者打印它们。data sink的操作可以用OutputFormat来描述，
flink内建了许多output formats，它们封装了在Datastream上的操作

#### 一些封装好的OutputFormat
* writeAsText -> TextOutputFormat
    * 将元素按行按字符串（通过调用toString）写入
* writeAsCsv(...) -> CsvOutputFormat
    * 将tuple按逗号隔开的方式写入值文件，行与字段的分隔符可以自定义。每一个字段的字符串值通过跳用toString方法实现。
* print() / printToErr()
    * 打印元素通过调用toString到输出，同时提供一个可选操作：就是调用prefix(msg)来生成输出前缀（常用来区分caller）。
        若并行度大于一，则输出会带上任务的ID。
* writeUsingOutputFormat() -> FileOutputFormat
    * 自定义文件输出，支持自定义序列化操作
* writeToSocket
    * 根据SerializationSchema将元素写入到套接字 
* addSink
    * 调用一个自定义的sink函数可以通过连接器将flink流绑定到其他系统（比如kafka），这些系统的实现就是作为sink函数
    
**注意：** 所有在datastream上以write开头的执行的方法主要的意图就是用来debug，所以它们并不参与flink的检查点。
这就意味着这些方法就带有at-least-once的语义，对于数据到目标系统的刷入这取决于OutputFormat的实现。这就意味着并不是数据
一发到output format就会被立即发送给下游链接的目标系统。所以，当失败时，这些还没有刷入的数据可能会丢失。

为了可靠性，对于从流写入文件，请使用flink-connector-filesystem连接器，当然，通过addSink传入的自定义的实现是会参与flink
exactly-once语义检查点的。
#### 基本
Dataset可以写往多个操作，且程序可以执行额外的转运操作的同时去写或者打印这些数据集

```java
// 标准的data sink方法
// text data
DataSet<String> textData = // [...]

// write DataSet to a file on the local file system
textData.writeAsText("file:///my/result/on/localFS");

// write DataSet to a file on a HDFS with a namenode running at nnHost:nnPort
textData.writeAsText("hdfs://nnHost:nnPort/my/result/on/localFS");

// write DataSet to a file and overwrite the file if it exists
textData.writeAsText("file:///my/result/on/localFS", WriteMode.OVERWRITE);

// tuples as lines with pipe as the separator "a|b|c"
DataSet<Tuple3<String, Integer, Double>> values = // [...]
values.writeAsCsv("file:///path/to/the/result/file", "\n", "|");

// this writes tuples in the text formatting "(a, b, c)", rather than as CSV lines
values.writeAsText("file:///path/to/the/result/file");

// this writes values as strings using a user-defined TextFormatter object
values.writeAsFormattedText("file:///path/to/the/result/file",
    new TextFormatter<Tuple2<Integer, Integer>>() {
        public String format (Tuple2<Integer, Integer> value) {
            return value.f1 + " - " + value.f0;
        }
    });

// 自定义输出格式

DataSet<Tuple3<String, Integer, Double>> myResult = [...]

// write Tuple DataSet to a relational database
myResult.output(
    // build and configure OutputFormat
    JDBCOutputFormat.buildJDBCOutputFormat()
                    .setDrivername("org.apache.derby.jdbc.EmbeddedDriver")
                    .setDBUrl("jdbc:derby:memory:persons")
                    .setQuery("insert into persons (name, age, height) values (?,?,?)")
                    .finish()
    );


// 本地的排序输出
DataSet<Tuple3<Integer, String, Double>> tData = // [...]
DataSet<Tuple2<BookPojo, Double>> pData = // [...]
DataSet<String> sData = // [...]

// sort output on String field in ascending order
tData.sortPartition(1, Order.ASCENDING).print();

// sort output on Double field in descending and Integer field in ascending order
tData.sortPartition(2, Order.DESCENDING).sortPartition(0, Order.ASCENDING).print();

// sort output on the "author" field of nested BookPojo in descending order
pData.sortPartition("f0.author", Order.DESCENDING).writeAsText(...);

// sort output on the full tuple in ascending order
tData.sortPartition("*", Order.ASCENDING).writeAsCsv(...);

// sort atomic type (String) output in descending order
sData.sortPartition("*", Order.DESCENDING).writeAsText(...);

// ****注意****全局的排序输出目前是不支持的
```    
