### Data Sources

数据源绑定通过StreamExecutionEnvironment.addSource(sourceFunction)

可以对于非并行源通过自定义实现SourceFunction，对于并行源，实现ParallelSourceFunction或者继承RichParallelSourceFunction

#### 内建的源StreamExecutionEnvironment.xxx
* 基于文件的
    * readTextFile(path) -> TextInputFormat
        * 基于行以字符串形式读取文件内容
    * readFile(fileInputFormat, path)
        * 以指定的输入格式去读取文件
    * readFile(fileInputFormat, path, watchType, interval, pathFilter, typeInfo)
        * 这是一个内部调用的方法，依赖于给定的监视类型
            * watchType=FileProcessingMode.PROCESS_CONTINUOUSLY
                这个源可以周期性地（间隔interval毫秒）来为新数据来监测path，
            * watchType=FileProcessingMode.PROCESS_ONCE,处理path的数据一次然后退出
        * 使用pathFilter，用户可以排除被处理的文件
        * 实现细节
            * flink将文件的读处理分为两个子任务（目录监测与读取数据），每一个子任务都由一个单独的实体来实现。目录的监测通过
                一个单线程来实现，然而读取数据则由多并发实现（并发度等于job的并发度）。单线程监视器的任务就是扫描目标目录，发现需要处理的
                文件，将文件分块，将这些分块分配给下游readers（会读取实际数据）。每一个分块被单独一个reader读取，但是一个reader可以读取多个分块
            * 实现笔记
                1. 若watchType被设置为FileProcessingMode.PROCESS_CONTINUOUSLY，当一个文件被修改，它的内容就会被全部处理，这个就会打破
                    exactly-one的语义，比如将数据追加到一个文件的末尾，将会导致其所有内容全部被处理
                2. 若watchType被设置为FileProcessingMode.PROCESS_ONCE，则源扫描仅仅扫描路径一次并退出，并不去等待reader读取文件内容结束。
    * 基于socket的
        * socketTextStream，从socket中读，元素通过分隔符分割读取
* 基于集合的
    * fromCollection(Collection)
    * fromCollection(Iterator, Class)
    * fromElements(T ...)
    * fromParallelCollection(SplittableIterator, Class)
    * generateSequence(from, to)
        * 以给定的间隔生生序列，并行的
* 自定义
    * addSource，绑定一个新的源函数，比如，从kafka中读你可以使用addSource(new FlinkKafkaConsumer08<>(...))
        **更多建连接器.md(https://ci.apache.org/projects/flink/flink-docs-release-1.9/dev/connectors/index.html)**
```java
ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

// read text file from local files system
DataSet<String> localLines = env.readTextFile("file:///path/to/my/textfile");

// read text file from a HDFS running at nnHost:nnPort
DataSet<String> hdfsLines = env.readTextFile("hdfs://nnHost:nnPort/path/to/my/textfile");

// read a CSV file with three fields
DataSet<Tuple3<Integer, String, Double>> csvInput = env.readCsvFile("hdfs:///the/CSV/file")
	                       .types(Integer.class, String.class, Double.class);

// read a CSV file with five fields, taking only two of them
DataSet<Tuple2<String, Double>> csvInput = env.readCsvFile("hdfs:///the/CSV/file")
                               .includeFields("10010")  // take the first and the fourth field
	                       .types(String.class, Double.class);

// read a CSV file with three fields into a POJO (Person.class) with corresponding fields
DataSet<Person>> csvInput = env.readCsvFile("hdfs:///the/CSV/file")
                         .pojoType(Person.class, "name", "age", "zipcode");

// read a file from the specified path of type SequenceFileInputFormat
DataSet<Tuple2<IntWritable, Text>> tuples =
 env.createInput(HadoopInputs.readSequenceFile(IntWritable.class, Text.class, "hdfs://nnHost:nnPort/path/to/file"));

// creates a set from some given elements
DataSet<String> value = env.fromElements("Foo", "bar", "foobar", "fubar");

// generate a number sequence
DataSet<Long> numbers = env.generateSequence(1, 10000000);

// Read data from a relational database using the JDBC input format
DataSet<Tuple2<String, Integer> dbData =
    env.createInput(
      JDBCInputFormat.buildJDBCInputFormat()
                     .setDrivername("org.apache.derby.jdbc.EmbeddedDriver")
                     .setDBUrl("jdbc:derby:memory:persons")
                     .setQuery("select name, age from persons")
                     .setRowTypeInfo(new RowTypeInfo(BasicTypeInfo.STRING_TYPE_INFO, BasicTypeInfo.INT_TYPE_INFO))
                     .finish()
    );
// 注意： 需要提供数据类型
```
