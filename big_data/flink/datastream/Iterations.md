### Iterations

https://ci.apache.org/projects/flink/flink-docs-release-1.9/zh/dev/datastream_api.html#iterations

可迭代的流程序实现了一个step func并把其嵌入到一个IterativeStream，由于DataStream不会介乎，
所以就没有迭代的最大值。代替的是，你必须指定流中的哪些部分是要回灌到迭代中，并指定哪部分直接传到
下游流（使用split转运或者filter）

```java
// ******模式例子******

// 创建可迭代源
IterativeStream<Integer> iteration = input.iterate();

// 在迭代源上施加（多）算子（这里以单算子map举例）
DataStream<Integer> iterationBody = iteration.map(/* this is executed many times */);

// 对于迭代流式数据源，可以执行满足过滤条件A后的回灌继续迭代，或者满足过滤条件B的前向传播到下一个流节点继续处理（即结束退出迭代）

// 定义满足过滤条件A后的回灌继续迭代
iteration.closeWith(iterationBody.filter(/* one part of the stream */));

// 满足过滤条件B的前向传播到下一个流节点继续处理（即结束退出迭代）
DataStream<Integer> output = iterationBody.filter(/* some other part of the stream */);

// *******具体例子（将序列0-1000，中的每个数值每次迭代减一，直到数值为0退出迭代）******
DataStream<Long> someIntegers = env.generateSequence(0, 1000);

IterativeStream<Long> iteration = someIntegers.iterate();

DataStream<Long> minusOne = iteration.map(new MapFunction<Long, Long>() {
  @Override
  public Long map(Long value) throws Exception {
    return value - 1 ;
  }
});

DataStream<Long> stillGreaterThanZero = minusOne.filter(new FilterFunction<Long>() {
  @Override
  public boolean filter(Long value) throws Exception {
    return (value > 0);
  }
});

iteration.closeWith(stillGreaterThanZero);

DataStream<Long> lessThanZero = minusOne.filter(new FilterFunction<Long>() {
  @Override
  public boolean filter(Long value) throws Exception {
    return (value <= 0);
  }
});

```
