### Apache Calcite

是一个动态的数据管理框架，其主要是用来协调app与数据存储与数据处理引擎之间的联系。
比如：使用可以先使用FlinkCEP（complex extracting processing）来筛选模式，然后使用Table API来分析这个模式，
或者你可以在预处理数据运行一个Gelly图算法之前使用SQL查询来扫描/过滤/聚集一个batch表。

注意：Table API与SQL现在还没有拥有所有的下层特征，不是所有的输入combinatin都被支持