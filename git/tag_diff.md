#### 历史快照TAG
* 标签是全局的，可以在任何分支上查看等,是静态的
* git标签有两种形式：
    1. 轻量级标签
        * 创建标签
            * git tag v1.0
    2. 带有附注的标签
        * 创建标签
            * git tag -a v1.0 -m "release version"
* 查看所有标签
    * git tag
* 查看某标签的详细信息
    * git show <tag>
        * e.g.  git show v1.0
* 以通配符模式查找标签
    * git tag -l <pattern>
        * e.g. git tag -l 'v1*' # 以v1开头的所有标签
* 删除标签
    * git tag -d <tag-name>
        * e.g. git tag -d v1.0
        
* 推送标签到远程
    * 远程就会有这些标签的zip包等
    * e.g. git push origin v1.0 v2.0 v3.0
    * 将本地尚未推送过的虽有标签都推送
        * git push origin --tags

#### git diff
* 首先关于unix的diff命令
    1. 两个文件a.txt与b.txt
    ```
    # 文件a.txt内容如下
    --------------------  
    hello
    a.txt
    --------------------
  
    # 文件b.txt的内容如下
    --------------------
    hello
    b.txt
    --------------------
    ```
    2. 执行unix命令`diff -u a.txt b.txt`的输出结果如下
    ```
    --- a.txt       2020-04-21 15:48:32.139252700 +0800  # --- file UTC 源文件
    +++ b.txt       2020-04-21 15:48:32.140253200 +0800  # +++ file UTC 目标文件
    @@ -1,2 +1,2 @@   # @@ -源文件开始的行号,源文件总行数 +源文件开始的行号,目标文件总行数 @@
     hello            # 源文件与目标文件相同的内容
    -a.txt            # 将源文件的这个内容删去
    +b.txt            # 向源文件添加这些内容，最后源文件就会跟目标文件一样
    ```
* git diff
    1. git diff \[file..\]: 用于比较处于工作区与暂存区中的文件的区别
    2. git diff <commit-id> \[file..\]: 用于比较处于工作区与版本库中的文件的区别
    3. git diff --cached \[<commit-id>\] \[file..\]: 用于比较处于暂存区与版本库中的文件的区别

