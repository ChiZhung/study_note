#### Git Branch
* 注意：分支只负责版本库，对于暂存区与工作区都是所有分支的公共区域

* 基本操作
    1. 查看分支 git branch
        * 查看包含远程分支在内的所有分支,以及每个分支的HEAD commit-id以及msg
            * git branch -av
    2. 创建分支 git branch <new-branch-name>
        * 注意新创建的分支的所有内容与从哪个分支上创建的内容是一致的，比如在dev分支上创建
            test分支，则test分支的**所有初始文件内容**是与dev一致的，则与master不一致。
    3. 切换分支 git checkout <branch-name>，切换上一分支git checkout -
    4. 删除分支
        * 删除分支时注意不可以在即将删除的分支上执行删除操作，故先切换再删除
        1. git branch -d <branch-name>
            * 当要删除的分支做了改动就要求，需删除的分支已经合并过了
        2. git branch -D <branch-name> 强制删除分支
    5. 创建并切换到新分支上
        * git checkout -b <new-branch-name>
    6. 合并分支
        * \[dst-branch-name\] git merge <src-branch-name>
            * 在dst-branch-name分支上执行该命令就会将src-branch-name分支合并到dst-branch-name上。
        * 合并分支的冲突解决
            * 示例
            ```
            # 1.txt in branch A
            hello
            A
          
            # 1.txt in branch B
            hello
            B
          
            # 当执行在分支A上执行git marge B
            hello
            <<<<<<<< HEAD   # 表示在A分支上文件1.txt的起始不同位置
            A
            ========        # 表示两个分支文件的不同分割界限
            B
            >>>>>>>> B      # 表示在B分支上文件1.txt的终止不同位置
          
            # 修改做法：
            # 1. 将自己需要保存的内容保留下来并将<<<<< HEAD ====== >>>>> B等标识冲突的作用域删除
            # 2. 将解决冲突后的文件进行git add和git commit
            ```   
        * 出现在git merge上的fast-forward
        
            ![](imgs/git_fast-forward.png)
            * 可能原先的dev版本与master版本存在冲突，但是master与dev在同一版本线上，
                但master的版本新于dev，则在当通过吧master合并上dev上时，就会直接将dev的指针移到master上
                （他们的commit ID就是master所指的commit ID）。
            * **注意：**对于处于同一时间线的版本库A与B，但B新于A，为了不使用fast-forward（快进），
                在分支A上执行git merge --no-ff B,此时就不会仅仅将A移到B上,而是会得出一个新的commit ID。
                ![](imgs/git_no-fast-forward.png)
        
    7. 查看当前分支的最新一次操作
        * git branch -v
    8. 分支改名
        * git branch -m <old> <new>
    9. 远程分支的操作见remote_github.md
    
#### Git Rebase
* 简而言之就是，将多线分支变为单线分支

![](imgs/git_rebase.png)

![](imgs/git_rebase_.png)    
* 注意：不要对master分支执行rebase,否则会引起很多问题（由于master分支一般是生产分支比较稳定，不要轻易做修改），
    一般来说，执行rebase的分支都是自己本地的分支，没有推送到remote repo中（因为推送到remote repo
        ，其他人再pull，就会有大量修改的存在），故一般涉及远程的使用git merge。
        
* rebase步骤
    * 比如对于分支br1与br2，在br2分支上执行rebase br1，就会首先寻找br1与br2的最近公共祖先版本A，
        然后正常情况就是(A, br2]接到br1后面（前提保证所有冲突解决）
    1. \[br2\] git rebase br1
    2. 解决冲突，执行以下之一
        1. git rebase --continue
            * 解决冲突修改后，执行git add然后这个git rebase --continue
        2. git rebase --skip
            * 舍弃本轮属于(A, br2]中一个版本节点的修改
        3. git rebase --abort
            * 终止rebase并回到初始状态
    3. 完成rebase
 
#### Git Cherry-Pick
* 运用场景
    * 比如你应该在dev分支做A,B两个操作，但是事实是你在master分支做了这几个操作，
        通过Cherry-Pick通过在master分支上的A,B操作的commit-id来完成在dev分支做
        同样的操作。
    ```
    branch dev:    *<-----*(op A, Commit-ID=aaaa)<-------*(op B, Commit-ID=bbbb)  
  
    branch master: *<-(1)-*(op A)<---------(2)-----------*(op B)
  
    (1) [master] git cherry-pick aaaa
    (2) [master] git cherry-pick bbbb
    ```
    * 注：尽量不要在需要cherry-pick的分支上进行跳commit-id的操作（比如：直接执行git cherry-pick bbbb
        而省略git cherry-pick aaaa（不推荐））
    * 对于原先那个做错的分支上，比如举例的master分支上，那么可以通过执行git reset来完成恢复    
         
