#### git中的两个指针
1. 访问HEAD指针
    * 表示当前在某分支上的所处的访问位置
2. 分支指针
    * 表示git某分支的头位置
    
    ![](imgs/git_two-pointers.png)
#### 标识commit-id的方法
1. HEAD当前位置的commit-id
2. \[HEAD^|\HEAD^^] 上一个/两个版本的commit-id
3. HEAD~n 上n个版本的commit-id
4. \<commit-id\> 指定的commit-id(其一般至少4位来标识)
#### 版本控制Git Reset（移动分支HEAD指针与访问HEAD指针）
1. git reset --soft <commit-id>
    * 移动当前**分支HEAD指针**到commit-id上，保留暂存区与工作区的所有修改
2. git reset \[--mixed\] <commit-id>
    * 默认采用mixed模式
    * 移动当前**分支HEAD指针**到commit-id上，移除暂存区到工作过去并保留工作区的所有修改
    * 注：git reset \[--mixed\] <commit-id> <file>...
        * 就是移动当前**分支HEAD指针**到commit-id上，移除暂存区中的file..到工作过去并保留工作区的所有修改
3. git reset --hard <commit-id>
    * 移动当前**分支HEAD指针**到commit-id上，丢弃暂存区与工作区的所有修改
#### 版本控制Git Checkout（仅仅移动访问HEAD指针）
1. git checkout -- <file>..
    * 恢复处于工作区的文件到前一状态（暂存状态/版本库HEAD状态）
2. git checkout -f
    * 恢复处于暂存区与工作过去的所有文件到版本库HEAD状态
3. git checkout <branch-name/commit-id>（其实branch就是commit-id的一个特例，
    也就是切换到分支HEAD所指向的那个commit-id）
    * 切换到新的分支/commit-id的区别，当切换到的commit-id不在任何一个分支的HEAD commit-id上，此时
        的访问HEAD就没有与任何一个分支HEAD指针重合，这样访问HEAD就处于一个游离的状态。
    * 在切换到新的分支/commit-id，当先后两分支处于不同版本时，需要保证切换过程中暂存区与工作区中的文件不存在被重新覆盖的冲突，
        这样才可以切换成功。
        ```
             *<--------*
             A         B
        B分支的1.txt文件内容为b,而A分支1.txt文件的内容为a，二者1.txt文件有一个处于未提交的状态，
        则彼此切换失败
        ```
        * 对于这种情况，可以先通过git stash命令先将这个修改暂时保存起来并清空暂存区与工作区，然后再切换到其他分支，
            当在其他分支任务完成。回到本分支通过git stash命令在恢复先前保存的暂存区与工作区中的内容（注意在恢复之前
            请保证暂存区与工作区干净（使用git add & commit or git stash））。
            1. git stash保存当前暂存区与工作区的修改并清空暂存区与工作区
            2. git stash list查看历史的所有尚存的stash保存记录
            3. git stash pop从最新的保存恢复到暂存区与工作区，并在stash list中删除该保存条目
            4. git stash apply从最新的保存恢复到暂存区与工作区，不在stash list中删除该保存条目
            5. git stash apply stash@{n}从第n条保存恢复到暂存区与工作区，不在stash list中删除该保存条目
            6. git stash drop stash@{n}删除第n条保存的条目
            * 恢复（自动采用merge）后可能存在冲突，参见branch.md中关于合并分支git merge的介绍
* git checkout -b <new-branch-name> \[\[--track\] <remote-branch>\]
    * 举例new-branch-name为dev，remote-branch为origin/dev
    * 新建一个名为dev的分支，并切换到dev分支上，若指定了--track origin/dev则表示这个新建的分支
        时追踪远程分支origin/dev的
    

