#### Git裸库
* git裸库就是一个没有工作区的repo，它常常用于在server端仅仅用于存储一些不做更改的代码
* 创建方式
    * git init --bare
#### Git Submodule
* 解决的常见场景
    * 比如一个项目中需要引入另一个项目，常见做法（以java举例）就是将另一个项目打成jar包
        放到公共nexus库中，本项目通过maven/gradle将这个jar包导入本项目中。
    * 存在的问题
        * 当这个我们引入的另一个项目是频发更新的且我们也需要这个更新，这就会导致，我们本地的
        jar包管理需要经常更新。
    * 其他解决办法
        1. 将另一个项目代码拷贝到本项目中（最不好的方式）
        2. 将另一个项目代码引入本项目中
* 示意图

    ![](imgs/git_submodule.png)
* 在本repo中添加submodule
    * git submodule add <git-repo-url/ssh> <no-exist-dir>
        * 表示将git-repo-url/ssh所标识的其他某个repo中的文件放置在no-exist-dir
            （这个文件夹是不能事先存在的）中
* 更新子模块的内容
    1. 更新单个子模块的内容
        * 当子模块那个repo发生版本更新，只需要在本repo，进入这个子模块，再执行git pull就可以完成
            子模块的更新
    2. 更新所有子模块的内容
        * 在本repo的最顶级目录下，执行git submodule foreach git pull
        
* 对于一个第三方克隆父repo
    1. 方法一
        1. git clone <parent-repo-url/ssh> <local-repo-name>
        2. 虽然吧父repo克隆下来了，但是其中的子模块文件夹是空的，故
        3. 在父repo顶层目录中，执行git submodule init
        4. 再执行git submodule update --recursive
    2. 方法二
        * git clone <parent-repo-url/ssh> <local-repo-name> --recursive
* 移除submodule（git没有提供命令支持）
    * git rm <submodule-dir>
    * 删除.gitmodules文件中的对应项（或者直接删除这个文件 git rm .gitmodules）
    * git commit

#### Git Subtree（推荐使用）   
* subtree出现时间晚于submodule，它们解决相同的问题，但是submodule存在一些问题
    1. submodule在做parent repo中修改child repo中的代码，并提交存在诸多问题与不方便之处
    2. submodule的删除git不提供直接的命令支持
* 添加child repo作为parent repo的subtree
    * git subtree add (-P |--prefix=)<child-repo-dir> <child-repo-url/ssh> <child-repo-branch> --squash
        1. --prefix用来指定这个child repo存放的目录
        2. <child-repo-url/ssh>用来指定child repo的仓库地址（可以使用git remote add ..先来指定）
        3. --squash表明subtree需要merge时，将child repo的所有版本提交信息合并为一条，然后再与parent repo合并
            
            ![](imgs/git_squash.png)
            * **注意**：若在git subtree命令中一旦使用了--squash的选项，则在之后的所有subtree操作都带上这个选项
            * 主要该选项仅仅对于add, merge, and pull有用
* parent repo拉取child repo的更新
    * git subtree pull (-P |--prefix=)<child-repo-dir> <child-repo-url/ssh> <child-repo-branch> --squash
* 当在parent repo中修改了其subtree中child repo中的代码
    1. git add && git commit用来提交对child repo的修改到本地并git push到remote parent repo
    2. 将subtree的修改更新进行git subtree push (-P |--prefix=)<child-repo-dir> <child-repo-url/ssh> <child-repo-branch>
* git subtree split
    * 用处
        * 当一个项目中，想将其中一部分离出来作为一个新项目以供其他项目使用
    * 优势
        * 使用这个命令就会保存与这部分内容相关的提交信息  
