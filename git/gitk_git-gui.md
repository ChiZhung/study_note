#### Gitk
* 使用场景：当版本提交过多，不太好查看逻辑就使用这个工具查看信息，主要用来查看分支提交图,
    更加直观。
1. 启动方法
    * 在你的repo下直接键入gitk即可
    * 注
    ```
    # 若出现如下报错
    Error in startup script: unknown color name "lime"
    ...
    
    # 解决办法, 命令行键入
    brew cask install tcl 
    ```
2. 为了方便起见
    * 使用`git config --global alias.ui '!gitk'`
    * 就可以使用`git ui`来启动gitk了
#### Git GUI
* 通过gui的方式；来执行git命令
* 为了方便起见
    * 使用`git config --global alias.gui '!git-gui'`
    * 就可以使用`git gui`来启动git-gui了