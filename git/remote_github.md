#### git远程与github
* git remote add origin https:xxxxx/xx.git
    * 往本地repo中添加一个远程repo链接取名为origin
* git remote rm origin
    * 移除远程repo
* git remote show
    * 展示远程repo
* git push -u origin master
    * 推送到remote origin/master上，并进行关联，下次使用git push即可
* 配置远程默认的push分支
    1. git config --global push.default matching
        * 指的是将本分支推送到远程同名且存在的分支上
    2. git config --global push.default simple
        * git2.0+采用的默认推送策略
        * 指的是将本分支推送到pull到本地的远程那个分支上

* 基于git分支的开发模型
    1. develop分支（频繁变化的分支）
    2. test分支（供测试与产品等人员使用的一个分支，变化不是特别的频繁）
    3. master分支（生产发布分支，变化非常不频繁的一个分支）
    4. bugfix(hotfix)分支（生产系统当中出现了紧急bug，用于紧急修复的分支）

* 使用ssh访问远程repo
    * 直接将本地的ssh公钥复制到repo settings中的deploy keys中即可
    * github中全局使用ssh，在账户settings中配置

* git clone <git-repo-addr> \[new-repo-name\]
    * 从远程克隆仓库到本地，若指定new-repo-name则本地的仓库名就是new-repo-name，
        否则就是远程仓库名
        
* 当向remote push时，若远程已经的比本仓库更新的版本时，就会push error
    1. 通过git pull进行git fetch+git merge远程版本到本地repo
    2. 若自动merge失败，则需要手动解决conflict,参见branch.md合并分支部分
    3. 合并成功/解决冲突后再进行添加提交并push
* 新建远程分支
    * 比如本地新建一个dev分支，然后将其内容推送到远程新的dev分支上
        * git push -u origin dev
* 删除远程分支
    * 以删除远程dev分支举例，remote以origin举例，两种方式
    1. git push origin :dev
        * 参见git_base.md的git push
    2. git push origin --delete dev
* 重命名远程分支
    * 在步骤如下，以远程分支dev1举例，本地分支dev1举例
    1. 删除远程
        * git push origin --delete dev1
    2. 创建远程分支
        * git push -u origin \[dev1:\]dev


#### github
* 设置github的repo多用户协助

    