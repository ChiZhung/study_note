#### git基础
* 注意：git只对已追踪的文件进行管理
* git的文件分类
    1. 未追踪
        * 不受git控制
        * 对于未追踪的文件，通过git add命令直接进入暂存区
    2. 已追踪
        * 受git控制
        1. 已修改(modified/modified，工作区)
            * 当暂存区/对象库中的文件修改后->工作区(modified)
        2. 已暂存(staged，暂存区)
        3. 已提交(committed，对象库)
* git常用命令
    1. 获得版本
        * git init
        * git clone
    2. 版本管理
        * git add
            * 添加处于工作区的已追踪或未追踪的文件到暂存区
            * **git add \*与git add .的区别**
                1. git add .将会add所有本目录下的修改或者添加的所有文件，**并会扫描.gitignore文件**
                2. git add *将会add所有本目录下的修改或者添加的所有文件，**不会扫描.gitignore文件**
        * git commit
            1. 提交所有位于暂存区的文件
                * git commit -m "xxx"
            2. 修改上一次的msg
                * git commit --amend -m "yyy"
            3. 以另一身份提交上一次的信息
                * git commit --amend --reset-author
            4. git commit -am "xxx"
                * add并提交位于工作区已追踪的文件
        * git checkout
            * git checkout -- <file>...
                * 将那些已经修改的文件，丢弃修改，即丢弃所有位于暂存区的先前修改
            * git checkout -f
                * 丢弃所有的暂存区的修改,恢复到
        * git stash
            * 保存当前的暂存区与工作区的状态，然后清空它们。见version_c-r.md####版本控制Git Checkout（仅仅移动访问HEAD指针）中
                关于git stash的论述
        * git rm
            * git rm <file>...
                * 将文件从工作树目录（对象库）中删除（真实目录中也会删除这个文件），使用git commit提交这个删除
                    (此时这个删除处于暂存区)
                * 可以使用git restore --staged|-S <file>...
                    * 将位于暂存区中处于删除状态的文件->位于工作区处于删除状态的文件
            * git rm --cached <file>...
                * 当file已经通过git add，当想把文件从暂存区->工作区，就执行这个命令(仅用于add)
        * git reset <index> <file>...
            * 将当前git工作树目录恢复到index版本状态，将处于暂存区中的改变file...文件移至工作区
            * 详见version_checkout-reset.md#### 版本控制Git Reset（移动分支HEAD指针与访问HEAD指针）
        * git merge
            * 详见branch.md合并分支
        * git mv <file1> <file2>
            * 移动/重命名文件
            * 一个放置在暂存区中的操作
            * 等价于2个动作
                1. mv file1 file2
                2. git add file1 file2
            * 恢复
                ```shell script
                git restore -S file1 file2 & git restore -W file1 & rm -f file2
                ```
        ```
        # 示例1. 工作区中存在新增的文件2.txt, 暂存区中存在已被删除文件1.txt，恢复被删除的文件1.txt
        1. git checkout -f        
        2. git reset HEAD 1.txt/git restore --staged 1.txt  && # 前一步骤用于staged->unstaged
            git checkout -- 1.txt/git restore \[-W|--worktree\] 1.txt # 后一骤用于从unstage状态恢复
        
        ```
        * **总结**
            1. 恢复index/staged（将当前位于暂存区（staged）的内容全部恢复至先前工作区的状态（worktree，unstaged））
                * 常见恢复index的操作
                1. git restore --staged|-S <file>...
                2. git reset HEAD <file>...
                3. git rm --cached <file>...
                    * 注：这个恢复操作只支持add操作之后从staged->unstaged
            2. 恢复工作区(worktree, 也是从处于工作区的修改丢弃，恢复至先前的状态)
                1. git restore \[--worktree|-W\] <file>...
                2. git checkout -- <file>...
            * 直接将位于暂存区与工作区的所有文件变为HEAD版本状态
                * git checkout -f
    3. 查看信息
        * git help <cmd>
            * 查看相关命令的使用文档
        * git log
            * 查看git提交的日志，同时能看到一个提交的摘要sha1的id值
            * git log -n(n是数字)表示查看前n条log
            * git log --graph: 以图像化的方式查看提交框图
        * git reflog
            * 注意当回退到历史版本后，其git log的打印则那个版本之前的log,为了解决commit ID的丢失，
                使用该命令来查看所有的git操作。
                ![](imgs/git_reflog.png)
        * git diff
            * 查看文件的不同
            * 详见tag_diff.md#### git diff
        * git blame \[option\] <file>
            * 查看一个文件上**每一行的修改以及修改作者是谁**，常常用于当代码出现问题，追踪溯源
        * git status
            * 查看当前local repo中状态
        * git branch
            * 查看分支
    4. 远程协作
        * git pull \[origin src-remote-branch:dst-local-branch\]
            * 会完成git fetch与git merge两个动作
            * 会拉取remote repo中的虽有改变，包括remote新的分支等等
        * git push -u|--up-stream <remote> <\[src-local-branch:\]dst-remote-
        branch>
            * 以remote为origin, dst-branch为dev举例:
            1. 表示将本地repo的当前访问HEAD指向的分支绑定到origin上的dev分支上
            2. 若远程没有dev分支则在推送后会在远程自动创建dev分支
            * 接下来就是将本地这个分支的（修改）内容推送到dev分支上
        * git push \[origin src:dst\]
            * 就是将src分支推送到origin/dst分支上，默认就是当前分支推送得到先前-u绑定的分支上
            * 可以通过**git push origin :dst或者git push origin --delete dst**,前者就是将空白推送给remote，
                就完成了删除远程的dst分支，后者也是完成远程分支删除
            * git push仅仅是完成把本地分支推送到远程同名分支
        
    5. repo配置
        * git config
            1. 配置当前host的git的全局用户名以及邮箱（位于/etc/gitconfig）
                * git config --system user.name "abc"
                * git config --system user.email abc@gmail.com
            2. (常用)配置当前host用户git的全局用户名以及邮箱（位于~/.gitconfig）
                * git config --global user.name "abc"
                * git config --global user.email abc@gmail.com
            3. (常用)配置当前git repo的用户名以及邮箱（位于.git/config）
                * git config --local user.name "abc"
                * git config --local user.email abc@gmail.com
            * 删除某个/全部config的配置
                * git config \[--local/global/system\] --unset/unset-all <variable>
            * 配置git内部命令别名
                * git config \[--local/global/system\] alias.<alias-cmd> <cmd>
                * e.g. git config --global alias.br branch
                    * 就可以使用git br等价于命令git branch
                * e.g. git config --global alias.unstage 'reset HEAD' 
            * 配置git外部命令别名
                * git config \[--local/global/system\] alias.<alias-cmd> <!cmd>
                * e.g. git config --global alias.l '!ls -l' (注意使用单引号引起来)
                    * 就可以使用git l等价于命令ls -l
    6. .gitignore
        ```gitignore
        # 注意所有的文件或者目录均不能以./开头不然无法识别
        # 要么使用xxx/xxx/yyy或者/xxx/xxx/yyy
       
        # 这的是忽略本repo中所有第一级目录下的1.txt文件
        /*/1.txt
        
        # 这的是忽略本repo中所有第n级目录下的1.txt文件(n=1,2,...) 
        /**/1.txt
       
        # 忽略除a.b文件之外的所有以.b结尾的文件
        *.b
        !a.b
        ```
    
        
                