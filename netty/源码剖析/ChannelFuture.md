#### ChannelFuture
* interface ChannelFuture extends Future\<Void\>
* 由于java的Future接口仅仅提供了结果的同步获取方法，以及宽泛的isDone方法判断，故对于Netty，其对java
    Future做了一定程度的加强，首先不仅支持异步回调（通过执行netty Future中的addListener方法），也支持
    跟java Future一样的同步等待，还提供了cause方法来获取异常。
* Future中的sync方法与await方法的区别，首先他们都是完成等待Future的计算完成，但是前者会
    保留计算失败所抛出的异常之后可以通过cause()方法来获取，而后者这不会抛出失败异常并保留。
#### ChannelPromise
* interface ChannelPromise extends ChannelFuture, Promise<Void>
* 是可写入的ChannelFuture，可写入的功能就是Promise的接口，其中Promise是Future的子接口

#### jdk的Future与netty的Future，Promise之间的关系与区别
* netty的Future继承与java的Future，netty的Promise继承与netty的Future
* 对于java的Future，其仅仅提供了手工的方式去检查执行结果（get方法），而这个操作是会阻塞的，
    netty则对其ChannelFuture进行了增强，通过ChannelFutureListener以回调的方式来获取执行结果。
    这样做去除了手工检查阻塞的操作。ChannelFutureListener的operationComplete方法是由IO线程执行的
    故在这个方法中不要去执行耗时的操作，不然会卡顿IO线程影响其性能，故这种情况需要通过另一个线程来执行（线程池）。
* ChannelFuture通过channel方法获取与之相关的那个Channel，当计算完成时，ChannelFuture就会遍历注册到
    其上的所有ChannelFutureListener的operationComplete(Future),这个传入的Future就是ChannelFuture本身，
    这样在operationComplete(Future)内部就可以获取到Channel对象了
    