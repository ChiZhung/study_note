#### EventLoop相关
* interface EventExecutorGroup extends ScheduledExecutorService, Iterable<EventExecutor>
    * EventExecutorGroup是通过调用其next方法来提供EventExecutor。此外，他们还负责EventExecutor的
        整个生命周期，以及以一个全局的方式去停止EventExecutor。
    * 注意到EventExecutorGroup管理着EventExecutor
    * 抽象方法
        1. EventExecutor next()
            * 用于返回EventExecutorGroup中的一个EventExecutor
* interface **EventLoopGroup** extends EventExecutorGroup
    * 是EventExecutorGroup的一个特化，用于channel的注册，当在event loop
        中执行selection操作的时候，channel就会的到处理。
    * EventLoopGroup管理着EventLoop
    * 抽象方法
        1. EventLoop next()
            * 用于返回下一个EventLoop
        2. ChannelFuture register(Channel ch)
            * 将一个通道注册到evt loop，返回的ChannelFuture将在注册完成时立刻得到通知。（非阻塞）
        3. ChannelFuture register(ChannelPromise cp)
            * interface **ChannelPromise** extends ChannelFuture, Promise<Void>
            * ChannelPromise实现类包含一个Channel的引用
            * 使用ChannelFuture将一个通道注册到evt loop，传入的ChannelFuture将在注册完成时立刻得到通知，
                它也会被返回。 
* class NioEventLoopGroup extends MultithreadEventLoopGroup
    * 基于MultithreadEventLoopGroup的一个实现基于NIO Selector的Channels
    * 对其创建实例，底层使用NioEventLoopGroup(int nThreads, Executor e, SelectorProvider sp, SelectStrategyFactory ssf)
        * SelectorProvider（参见java/nio/Selector.md####SelectorProvider）
* EventLoopGroup当中会包含一个或多个EventLoop，一个EventLoop在他整个生命周期中，都只会与一个Thread进行绑定。
    所有由EventLoop所处理的各种IO事件都将在它所关联的那个Thread上进行处理。一个channel只会在它声明周期只会注册到一个EventLoop上
    ，一个EventLoop在运行过程中，会被分配给一个或者多个Channel。
    * 对于channel，通过从event loop group中选择event loop采用的就是round-robin方式选出一个event loop，
        然后再把channel注册其上。
    * **注意**：由于一个event loop上可能绑定着多个channel，故我们在channel handler中一定不要做耗时的任务，不然
        不仅会卡住本channel，也可能会卡住其他线程。 
* 当你在一个channel上执行一个操作，netty会检测这个操作的线程是不是本channel所绑定的那个event loop，若不是netty则会将这个操作提交到
    event loop group中。netty会保证对于一个channel的操作是保证先后顺序的，netty底层通过队列来保证。
    netty的channel是线程安全的，故可以在外部任意一个存储这个channel（比如通过channel handler context获取这个channel）
    并对其操作，
#### 业务线程池的常见两种实现方式
1. 在channel handler的回调方法中使用自定义的业务线程池
2. 借助netty提供的向channel pipeline时调用addLast方法传入EventExecutorGroup（就像最先预定义bossGroup，workerGroup一样
）