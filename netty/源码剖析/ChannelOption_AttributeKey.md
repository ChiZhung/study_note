#### ChannelOption
* class **ChannelOption**<T> extends AbstractConstant<ChannelOption<T>>
    * 介绍
        * ChannelOption允许以一种类型安全的方式来配置ChannelConfig，支持哪一种ChannelOption依赖于
            实际的ChannelConfig实现并且也可能依赖于其所属的传输层的传输类别。
        * 其一般就是来配置诸如传输层TCP的一些配置项
        * 泛型T指的是选项值的类型
    * 刚创建的ChannelOption常量其只是一个标识，其仅仅只有string name和int id，获取与设置这个ChannelOption是通过
        ChannelConfig来执行的，在ChannelConfig中就有这个特定ChannelOption与其代表的值的映射。
    * 也就是说ChannelOption就是一个常量key的封装，其是单例模式，可以通过在ConstantPool中通过
        string name来获取或者创建这个单例的ChannelOption
    * 全局唯一的存放ChannelOption的常量池就是ChannelOption.pool，通过ChannelOption.valueOf(String name)
        来获取这个name代表的ChannelOption
    * 对于ChannelOption的设置，是在Channel的实现子类中存在一个ChannelConfig的实现子类的字段，通过
        channel.config().setOption(ChannelOption opt, Object val)来完成设值。
* abstract class **AbstractConstant**<T extends AbstractConstant<T>> implements Constant<T>
    * 介绍
        * 其是Constant的一个基础实现
* interface **Constant**<T extends Constant<T>> extends Comparable<T>
    * 介绍
        * 其是一个单例，可以安全地使用==来用于比较，其通过ConstantPool来进行创建与管理
* abstract class ConstantPool<T extends Constant<T>>
    * 介绍
        * 常量池，T就是常量的类型
    * 字段
        1. ConcurrentMap<String, T> constants
            * 用于存放一些常量，string就是这个常量的名字，T就是常量的类型
        2. AtomicInteger nextId
            * 用于在constants中添加新的常量时赋予其id（使用自增的手段）
#### AttributeKey与Attribute
* class **AttributeKey**<T> extends AbstractConstant<AttributeKey<T>>
    * AttributeKey类似于ChannelOption仅仅作为单例的key常量
    * T就是Attribute的类型，也就是Attribute所包含value的类型
    * 全局唯一的存放AttributeKey的常量池就是AttributeKey.pool，通过AttributeKey.valueOf(String name)
              来获取这个name代表的AttributeKey
* interface Attribute<T>
    * 抽象方法
        1. AttributeKey<T> key()
        2. T get()
        3. void set(T value)
        
        ...
        
* 对于Attribute的设置，通过channel/channelHandlerCtx.attr(AttributeKey key).set(Object val)来完成Attribute实例的封装并设置。
    继承实现自AttributeMap的channel就包含一个AtomicReferenceArray<DefaultAttribute<?>> attrs字段，设置的属性
    就放置在这个字段中。
    * 这个AttributeMap实现就是实现与ConcurrentHashMap类似，使用一个链表数组，后者使用的是分段锁机制，而对于前者，
        注解中说考虑到后者的高内存消耗，就使用的是java.util包下的AtomicReferenceArray，通过这个来完成在数组特定
        '桶'上的原子操作（底层使用的就是Unsafe类的CAS操作）
* 对于Channel与ChannelHandlerContext中的AttributeMap
    * 二者的AttributeMap是独立的，但是在4.1版本之后，他们的attrs就是同一个
    * 在netty4.1之前的版本，这二者的attr是彼此独立的（比如：channel.attr(key).get() != channelHandlerCtx.attr(key).get()），
        而在4.1版本开始，channel.attr(key) == channelHandlerCtx.attr(key)
#### AttributeKey与ChannelOption的所处关系

![](imgs/channel-option_attribute.png)

![](imgs/channel_channel-opt_attr.png)            
            