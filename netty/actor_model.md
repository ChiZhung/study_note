### actor model(一个用于并发计算的模型)
* 参考网址
    * https://www.brianstorti.com/the-actor-model/
#### Actor
* 介绍：
    * 是一个计算的原子单位，收到一个msg然后对其做一些计算，这个想法跟面向对象的编程很像，
        一个对象收到一个msg（比如调用对象的一个方法），并做一些依赖这个msg的事情（基于调用的
        方法）。对于actor，其对于面向对象的主要不同就是，actors是完全彼此独立的，且彼此内存独立。
        一个actor可以维持一个私有的状态，这个私有的状态不会被其他actor直接更改。
* one actor is no actor
    * 在actor模型中，每一件事情都是一个actor，并且其是拥有地址的，故一个actor可以发送msg给
        另一个actor。
* actors have mailboxes
    * 尽管同一时刻，可有多个actor同时运行，但是一个actor会串行地处理给定的msg。这意味着
        若你发送给一个actor三个msg，它将会一时刻执行一个，为了使得三个msg可以得到并发的执行，
        你就需要创建三个actor来接受者三个msg。
    * msg是异步地发送给一个actor，故当一个actor还在处理另一个msg的时候，这就需要将接受
        的msg存储起来。mailbox就是存储这个msg的地方。
* actor做什么
    * 当一个actor收到一个msg，将会做以下三点：
        1. 创建更多的actor
        2. 发送msg给其他的actor
        3. 指定对下一条msg的所作所为
            * 就是修改自身的状态以接受下一条msg
    * 举例：一个类似计算器的actor，其初始状态就是数字0，当这个actor收到了一个msg（add（1））后，
        不是修改它初始状态，而是为了下一次收到的msg而指定，将state设置为1
#### 容错
* 每个代码都运行在一个process中，这些process是彼此独立的，同时又一个监视的process（它也是一个actor，所有process都是actor），
    当被监视的process发生崩溃时，就会通知监视process，其会对此作出回应。
* 这使得创建一个能够自我修复的系统成为可能，这意味着当一个actor处于一个异常状态或者是崩溃了，
    一个监视者就能够对此作出反映，以使得将那个actor再次处于一个一致性状态中。（有很多策略来
    实现这个目的，最常见的做法就是以初始状态去重启这个actor）
#### 分布式
* actor模式另一个有趣的方面就是其并不关心接受msg的那个actor所在的位置是在本地还是另一个节点上。
* 考虑到，若一个actor仅仅是一个包含一个mailbox（用于存储接下来处理msg）与一个内部状态的代码单元。
    且其仅仅对msg做出反应
* 这就允许我们创建一个来平衡多计算机与其中一个失败了帮助我来恢复。
#### next steps and other resources
       