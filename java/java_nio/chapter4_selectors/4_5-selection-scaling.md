### 4.5 Selection Scaling
通过一个线程来进行IO复用可以减少复杂度以及潜在地增强性能（通过淘汰多线程管理的开销）；
但是仅仅使用一个线程来服务所有的可选择channel是合适的么？这个得视情况而定；



#### 场景一：多channel服务，无高响应要求

* bad way
使用多个线程绑定多个selector来服务多个channel，**在大量channel上执行readiness选择是不费的，大部分的工作都是由底层OS来完成**；
**通过维持多个selector并通过随机分配channel给其中一个selector，这种方式并不是一个好的解决方式**

* good way
更好的解决方式是：仅使用一个selector来服务所有的channel，并对于ready的channel将其代理给其他线程来处理操作；

通过单一的一个点去监控channel的readiness以及使用一个解耦的工作线程池来处理即将到来的数据；根据部署的条件，线程池的大小可以被调整（自调或者是动态的）；
selectable channel的管理仍然是简单的，简单是一件好事；

#### 场景二：多channel服务，存在部分高响应要求

* bad way
有些channel对于响应是有着高要求的，这种场景可以通过使用两个selector来解决；一个selector用来处理高响应要求的channel，
另一个用来正常处理；

* good way
仍然使用一个selector来做选择，通过将ready channel根据其类型分发给不同类型的线程进行处理；

```java
public class SelectSockets {

    public static int PORT_NUMBER = 1234;

    void go(String[] args) throws IOException {

        int port = PORT_NUMBER;
        if (args.length > 0) {
            port = Integer.parseInt(args[0]);
        }

        System.out.println("listening on port " + port);

        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();

        Selector selector = Selector.open();

        serverSocketChannel.bind(new InetSocketAddress(port));

        serverSocketChannel.configureBlocking(false);

        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        while (true) {
            if (selector.select() == 0) {
                continue;
            }

            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey selectionKey = iterator.next();

                if (selectionKey.isAcceptable()) { // 表示到来了一个连接请求
                    serverSocketChannel = (ServerSocketChannel) selectionKey.channel();
                    SocketChannel socketChannel = serverSocketChannel.accept();
                    if (socketChannel != null) {
                        registerChannel(selector, socketChannel, OP_READ);
                        sayHello(socketChannel);
                    }
                }

                if (selectionKey.isReadable()) {
                    readDataFromSocket(selectionKey);
                }

                iterator.remove();
            }
        }
    }

    void registerChannel(Selector selector, SocketChannel channel, int ops) throws IOException {
        channel.configureBlocking(false);
        channel.register(selector, ops);
    }

    ByteBuffer buffer = ByteBuffer.allocate(1024);

    void sayHello(SocketChannel channel) throws IOException {
        buffer.clear();
        buffer.put("Hello!\r\n".getBytes());
        buffer.flip();
        channel.write(buffer);
    }

    void readDataFromSocket(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        int count;

        buffer.clear();

        while ((count = channel.read(buffer)) > 0) {
            buffer.flip();

            // Send the data; don't assume it goes all at once
            while (buffer.hasRemaining()) {
                channel.write(buffer);
            }
        }

        if (count < 0) { // -1 if the channel has reached end-of-stream
            channel.close(); // close channel, and invalidate the key
        }
    }
}

class SelectSocketsThreadPool extends SelectSockets {
    private static final int MAX_THREADS = 5;

    private ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREADS);

    public static void main(String[] args) throws IOException {
        new SelectSocketsThreadPool().go(args);
    }

    @Override
    void readDataFromSocket(SelectionKey key) throws IOException {
        // 由于是通过异步的方式来处理read，所以在处理之前将本channel的OP_READ去掉
        // 以达到：在异步处理这个channel read的时候，selector忽视这个channel的read-readiness
        key.interestOps(key.interestOps() & (~OP_READ));
        executorService.submit(() -> {
            try {
                drainChannel(key);
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    key.channel().close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    void drainChannel(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        int count;

        buffer.clear();

        while ((count = channel.read(buffer)) > 0) {
            buffer.flip();

            // Send the data; don't assume it goes all at once
            while (buffer.hasRemaining()) {
                channel.write(buffer);
            }
        }

        if (count < 0) { // -1 if the channel has reached end-of-stream
            channel.close(); // close channel, and invalidate the key
            return;
        }

        // 本操作处理完了，恢复兴趣集合
        key.interestOps(key.interestOps() | OP_READ);
        // 由于之前将对于这个channel的感兴趣集合已经去掉了READ，所以这个时候恢复后，需要把selector进行wakeup
        // 不wakeup可能会导致select一直blocking
        key.selector().wakeup();
    }
}

```







