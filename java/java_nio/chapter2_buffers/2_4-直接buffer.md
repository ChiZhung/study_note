### 2-4 直接buffer
当使用一个非direct ByteBuffer作为channel的写时，channel将会做：
1. 创建一个临时的direct ByteBuffer对象
2. 将nondirect buffer中的数据拷贝到临时的direct buffer中
3. 在临时的direct buffer上执行低级别的IO操作
4. 临时的direct buffer在退出作用域后最终被GC回收

不过对于这个，还是要看具体的VM实现，有些可能会在运行时对直接buffer进行cache以及重用或者其他trick的方式来提升吞吐；
若只是简单使用若干次nondirect buffer，则不必关心；但是要是在需要重复利用以及高性能场景下，一定要使用直接buffer已经对其重复利用；

虽然direct buffer对于IO来说是一种优化，但是它可能较之于nondirect buffer来说可能开销更大；
直接buffer使用的内存是通过原生的os特定的代码创建的，其避开了jvm heap，创建以及销毁direct buffer较之于
heap上的buffer分配与销毁可能会开销大很多；


#### 使用heap buffer还是off-heap buffer
off-heap mem的分配，使得你增加了jvm监控不到的内容；
还是依据老的软件开发原则：不要过早优化

jvm实现可能会优化buffer cache，使得依然可以得到不错的性能；

#### 内存映射buffer
mapped buffer是一种起数据元素存储在一个文件中的字节buffer，其通过内存映射来完成访问；mapped buffer是直接buffer，
且仅能通过FileChannel对象来创建；其使用类似direct buffer，但是其又有仅对于文件访问的特有特性；


