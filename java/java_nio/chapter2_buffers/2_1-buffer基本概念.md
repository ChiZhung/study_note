### 2-1 Buffer的基本概念
较之于基本类型数组而言，buffer类的好处是其封装了数据内容以及对于单个对象的数据信息；

#### 属性
* capacity
  * 这个buffer可以容纳的最大元素个数，当buffer被创建之后这个buffer的容量就被确定且不再改变
* limit
  * buffer中第limit位置的元素，是第一个不可以被读或写的位置；或句话说就是buffer中当前存活元素的个数
* position
  * buffer中下一个被读写的位置，这个值将被自动更新当调用了get或者put方法后
* mark
  * 一个被记忆的位置，调用了mark方法后将会设置mark=position，调用reset方法使得position=mark

总是存在一下的关系：0 <= mark <= position <= limit <= capacity

#### Buffer API
一个需要注意的方法是isReadOnly(),因为所有Buffer都是支持read的，但并不是所有Buffer都是可写的；
每一个Buffer的具体类都会去实现isReadOnly方法来表明这个buffer是否可以修改；
一些buffer类型可能并没有将其数据存储在数组中，比如MappedByteBuffer其实际是一个只读文件，你也可以
显示的创建一个只读的buffer，当对一个只读的buffer进行修改时将会抛出ReadOnlyBufferException；

* buffer的访问
  * 通过get方法从buffer读取一个元素，使用put方法来实现往buffer中写入一个元素；当使用过度使用put，
    导致position字段值大于limit将会抛出BufferOverflowException异常，当过度使用get
    导致position字段值大于等于limit将会抛出BufferUnderflowException异常
  * 当使用带有index的get or put进行绝对访问时，不会对position进行修改，但是可能会抛出java.lang.IndexOutOfBoundsException
    
* buffer的填充
* buffer的翻转flip
  * 当我们对buffer填充好数据准备给channel读的时候，我们需要调整buffer相关字段，比如buffer.limit(buffer.position).position(0);
    我们可以使用buffer.flip()方法来完成这个功能，将buffer的从fill状态转换到drain状态，以便channel进行读操作
  * 当使用flip两次时，将会使得buffer的limit与position字段都变成0
* buffer的rewind
  * 仅仅将position重置为0，用于flip之后的reread


* buffer draining


注意buffer是非线程安全的，一旦buffer已经被filled以及drained，则其是可以reuse的，通过调用clear
方法使得buffer变成empty state，将limit置为cap值，将position置为0


* buffer compact
  * 又是对于一个buffer，你并不想drain其中所有的内容，只是drain其中一部分内容，然后再fill它；
    比如先从buffer中read了一部分数据，然后将剩余数据compact（也就是挪动从0开始），然后在末尾write新数据
* buffer mark
  * 允许buffer记住一个位置（调用mark方法）并在后面重置回这个位置（调用reset方法），若是在reset之前没有调用mark方法，
    则会抛出InvalidMarkException，有些buffer的操作会丢弃mark（rewind，clear， flip）；当调用limit(index)
    或者position(index)方法，当设置的index小于当前实际值，也会丢弃mark；
* buffer比较
  * **是通过比较buffer中remaining数据来实现比较equal的，也就是说两者的buffer的cap不一定要一样大，以及pos，limit等index不一定要一致**