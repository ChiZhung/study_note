### 3.7节 channel工具类
NIO channel提供了一个新的，类stream的隐喻，但是咱们熟悉的byte stream以及char reader/writer仍然被
广泛地使用；channels的细节实现可能最终又落回到了java.io的包，但是java.io包下面关于stream以及reader/write
的API后面也是不会冲该包中移除的；

java.nio.channels.Channels这个工具类提供了将Channel与stream or reader/writer联系起来的静态工厂方法；

1. Channels.newChannel(InputStream in) -> ReadableByteChannel
2. Channels.newChannel(OutputStream out) -> WritableByteChannel
3. Channels.newInputStream(ReadableByteChannel ch) -> InputStream
4. Channels.newOutputStream(WritableByteChannel ch) -> OutputStream
5. Channels.newReader(ReadableByteChannel ch, CharsetDecoder dec, int minBufferCap) -> Reader
6. Channels.newReader(ReadableByteChannel ch, String csName) -> Reader
7. Channels.newWriter(WritableByteChannel ch, CharsetEncoder dec, int minBufferCap) -> Writer
8. Channels.newWriter(WritableByteChannel ch, String csName) -> Writer

