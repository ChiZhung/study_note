
## 第三章 通道
通道是用来连接byte buffer以及通道另外一侧实体（比如文件or套接字）的桥梁

### 3-1 通道基本概念
不像buffer，channel是使用接口来定义的，主要对于channel的实现对于不同的os实现差异巨大，所以channel的api
只是简单地描述能做什么；同时，channel的实现通常是使用原生代码实现的。channel接口使得你以一种
可控的轻便的方式去访问低层次IO服务；

在顶级channel接口中，仅有isOpen以及close两个接口方法；

![img.png](../imgs/img.png)


* InterruptibleChannel
  * 本接口是一个标记接口，表明实现本接口的子channel是可中断的，支持channel异步调用close，当一个线程等待在一个可中断
    通道的IO操作上时，当另一个线程调用了channel.close()则这个等待线程将会接受到一个AsynchronousCloseException
  * 大部分channel实现都是支持可中断的
* ReadableByteChannel and WritableByteChannel
  * 可以看出，channel都是基于byte buffer来进行操作的
  * 这样的层次设计使得当需要引入其他数据类型的channel时，可以通过继承Channel完成

* java.nio.channels.spi包下的抽象channel类
  * channel用户可以安全地无视SPI包下的中间类，同时SPI允许以一种控制的，模块化的方式来创建一个新的channel实现
    并通过可插拔的形式添加到JVM中；

#### 打开channel
IO服务主要可以概括为两类：文件IO以及stream IO，所以其对应于两种channel：file channel以及socket channel；
file channel: FileChannel；
socket channel：SocketChannel，ServerSocketChannel，DatagramChannel；

```java
SocketChannel sc = SocketChannel.open();
sc.connect (new InetSocketAddress ("somehost", someport));
ServerSocketChannel ssc = ServerSocketChannel.open(); ssc.socket().bind (new InetSocketAddress (somelocalport));
DatagramChannel dc = DatagramChannel.open();
RandomAccessFile raf = new RandomAccessFile ("somefile", "r"); FileChannel fc = raf.getChannel();
```
* file channel的创建方式
  * RandomAccessFile::getChannel() 
  * FileInputStream::getChannel() 
  * FileOutputStream::getChannel() 
* socket channel创建方式：
  * 可以通过工厂方法来直接创建socket channel


Socket，ServerSocket，InputStream，OutputStream是一套；然后SocketChannel与ByteBuffer是一套；
在数据IO上，socket通过in/output stream来完成数据IO交互，而对于channel是使用新的一套逻辑，
即使用direct buffer来完成数据交互；
```java
NativeDispatcher dispatcher;
dispatcher.read(fd, buffer_addr, data_len) or dispatcher.pread(fd, buffer_addr, data_len) // 底层基于native方法实现
```


```java
class SocketChannelImpl {
  // 通过适配器模式来架起channel与socket对象之前的桥梁
  public Socket socket() {
    synchronized (this.stateLock) {
      if (this.socket == null) {
        this.socket = SocketAdaptor.create(this);
      }

      return this.socket;
    }
  }
}
```
当使用socket.getChannel()方法尝试获取channel时，仅当这个socket是通过channel.socket()方法创建的情况下，才能拿到channel对象；
也就是说，channel->socket是单向决定的；

#### 使用channel
channel接口的两个子接口：ReadableByteChannel与WritableByteChannel，以及继承自这两个接口的子接口ByteChannel；
可以把继承实现了ByteChannel这个接口的子类视为双向的，只继承实现ReadableByteChannel与WritableByteChannel其中一个
的视为单向的；

可以看到所有的file与socket channel都有实现上述三个接口，鉴于类的定义，可以说所有的file，socket channel都是双向的；
但是事实上，socket总是双向的，但是file channel不一定；一个给定的文件在不同时刻是用着不同访问权限的，对于FileInputStream
对象使用getChannel方法可以得到FileChannel对象，但是得到的却是一个只读的（虽然其在类定义被标示为双向的）；当对于这个对象去
调用write方式的时候会抛出异常，因为FileInputStream打开的文件总是以只读的方式去打开文件；

还要注意的一点是，对于一个已经连接在IO服务上的一个channel，这个channel实例拥有的能力是受限于其连接的IO服务的特性；

channel可以以阻塞或者非阻塞模式进行操作；一个处于非阻塞模式的channel不会使得调用channel的线程陷入sleep；请求操作要么
立马完成要么返回一个表示nothing was done的结果；**目前面向流的channel，比如socket与pipe可以使用非阻塞模式**

socket channel类是继承于SelectableChannel，继承自SelectableChannel的类可以使用Selector；非阻塞io跟selector的
结合使得你的应用支持IO复用；


#### 关闭channel
不像buffer，channel是不支持重复使用的，一个打开的channel代表着一个IO服务的特定连接，其封装着连接的状态；
当一个channel关闭，那个连接将会丢失且这个channel不再连接到任何东西；

当一个channel调用了close方法，当channel关闭底层的io服务的时候会造成短暂的阻塞，甚至当使用非阻塞模式也仍会有这个情况；
当channel close而导致的阻塞，这个阻塞程度是与os，文件系统相关的；对于一个channel进行多次调用close是无害的；

当第一个线程调用close而处于阻塞状态，其他线程此时调用close也会阻塞在close方法上，直到第一个线程close完成；
调用一个已经关闭的channel的close方式，将什么都不做而直接返回；

通过调用isOpen方法来判断一个channel是否处于open状态；


当一个channel实现了InterruptibleChannel接口，则表明：当一个阻塞在channel上的线程被调用了interrupt方法，这个channel将会
被关闭，原来被阻塞的线程恢复执行的时候会抛出ClosedByInterruptException；此外，当一个线程被设置为中断状态，当其访问一个channel
的时候，这个channel将会立即关闭，同样抛出ClosedByInterruptException异常；当一个线程的interrupt方法被调用的时候，其被设置为
中断状态；通过isInterrupted方法来测试这个线程是否处于中断状态；当前线程的中断状态可以通过调用Thread.interrupted静态方法
来完成clear；

注意：不要将 中断睡眠在channel上的线程与中断睡眠在selector上的线程混淆了；前者shutdown channel，而后者不会；当一个sleep on
选择器上的线程被中断，当这个线程随后接触channel的时候，这个将会被close；

对于一个睡眠在channel上的一个线程被中断，就去shut down这个channel难免会觉得这样做显得苛刻；但这样就是作为NIO架构
的显式设计，经验表明：对于在所有操作系统上进行可靠处理被中断的IO操作都是不太可能的；

可中断的channel也都是可异步关闭的，一个实现了InterruptibleChannel接口的channel可以在任何时候被关闭，甚至是存在一个
线程仍阻塞在channel上等待IO完成；当一个channel被关闭，所有睡眠在这个channel上的线程都会被唤醒并收到一个AsynchronousCloseException；
这个channel随后会被关闭；


那些没有实现InterruptibleChannel的channel，它们是一些典型的非low level的，原生代码实现的有着特殊目的的channel；

















  