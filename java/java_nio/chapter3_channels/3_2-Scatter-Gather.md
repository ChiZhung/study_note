### 3.2节 Scatter or Gather(矢量IO)
矢量IO=Scatter/Gather：

在多个缓冲区上实现一个简单的IO操作。减少或避免了缓冲区拷贝和系统调用（IO)

write：Gather

数据从几个缓冲区顺序抽取并沿着通道发送，就好比全部缓冲区全部连接起来放入一个大的缓冲区进行发送，缓冲区本身不具备gather能力。

read：Scatter

从通道读取的数据会按顺序散布到多个缓冲区，直到缓冲区被填满或者通道数据读完。

* 使用矢量IO来完成面向协议传输的IO数据
  * 比如我们从socket中读取一段数据，其由header与body两部分组成
  ```java
  ByteBuffer header = ByteBuffer.allocateDirect (10); 
  ByteBuffer body = ByteBuffer.allocateDirect (80); 
  ByteBuffer [] buffers = { header, body };
  int bytesRead = channel.read (buffers);
  
  switch (header.getShort(0)) { 
      case TYPE_PING:
         break;
   case TYPE_FILE: body.flip();
  fileChannel.write (body); break;
  default:
  logUnknownPacket (header.getShort(0), header.getLong(2), body); break;
  }
  ```
  * 同时我们也可以基于此进行数据填充，并写回socket
  ```java
    body.clear();
    body.put("FOO".getBytes()).flip(); // "FOO" as bytes
    header.clear();
    header.putShort (TYPE_FILE).putLong (body.limit()).flip();
    long bytesWritten = channel.write (buffers);
  ```
  
使用矢量IO的好处：可以避免大量的无用bytes移动与拷贝，减少所要书写的代码与debug；

其实底层其实就是执行readv与writev函数，对应于IOVec（IO向量）




