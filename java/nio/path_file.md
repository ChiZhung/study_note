### java nio Path and Files

* 拿到一个文件的符号链接`Paths.get('file').toRealPath()`
* 使用简单的unix通配符用于选取目录树中所有的符合规则的文件`Files.newDirectoryStream(path, "*.java")`
* 遍历目录树`Files.walkFileTree(Path start, FileVisitor<? super Path> visitor)`，提供了简单默认实现`SimpleFileVisitor<T>`
  * 注意，遍历目录树中的文件时，不会跟随符号链接

#### Files
* 创建文件`Files.create(Paths.get("path"))`
  * 当创建的文件需要指定相关的文件属性`Files.createFile(path, PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rw-rw-rw-")))`
  * 注意：如果在创建文件时要指定访问许可，不要忽略其父目录强加给该文件的umask限制或受 限许可。比如说，你会发现即便你为新文件指定了rw-rw-rw许可，但由于目录的掩码， 实际上文件最终的访问许可却是rw-r--r--。
* 删除文件`Files.delete(path)`
* 文件复制`Files.copy(srcPath, destPath)`
  * 以覆盖的形式进行文件复制`Files.copy(srcPath, destPath, REPLACE_EXISING)`，其他的复制选项包括COPY_ATTRIBUTES(复制文件属性)和ATOMIC_MOVE(确保在两边的 操作都成功，否则回滚)。
* 文件移动`Files.move(srcPath, destPath)`
  * 通常在移动文件时，你想要用复制选项，此时便可以用`Files.move(Path source, Path target, CopyOptions...)`方法，但要注意变参的使用。
* 读取文件属性
  * 最后修改时间`Files.getLastModifiedTime(path)`
  * 文件大小`Files.size(path)`
  * 是否为符号链接`Files.isSymbolicLink(path)`
  * 是否为目录`Files.isDirectory(path)`
  * 获取文件属性`Files.readAttributes(path, "*")`

* 打开文件
  * Files.newBufferedReader
  * Files.readAllLines
  * Files.readAllBytes


#### 文件修改通知
在Java 7中可以用java.nio.file.WatchService类监测文件或目录的变化。该类用客户 线程监视注册文件或目录的变化，并且在检测到变化时返回一个事件。这种事件通知对于安全监
测、属性文件中的数据刷新等很多用例都很有用。是现在某些应用程序中常用的轮询机制(相对 而言性能较差)的理想替代品。
下面的代码用WatchService监测用户karianna主目录的变化，每当发现变化时就会在控制 台中输出一个事件通知。和很多持续轮询的设计一样，它也需要一个轻量的退出机制。
```java
  public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {

      WatchService watchService = FileSystems.getDefault().newWatchService();

      Path dir = Paths.get("/Users/bytedance/gitRepo/demo");
      WatchKey watchKey = dir.register(watchService, ENTRY_MODIFY);

      while (true) {
          watchKey = watchService.take();
          for (WatchEvent<?> event : watchKey.pollEvents()) {
              if (event.kind() == ENTRY_MODIFY) {
                  System.out.println(123);
              }
          }
          watchKey.reset();
      }


  }
```

#### 异步IO
Java 7引入SeekableByteChannel接口，是为了让开发人员能够改变字节通道的位置和大小。
JDK中有一个java.nio.channels.SeekableByteChannel接口的实现类——java.nio. channels.FileChannel。这个类可以在文件读取或写入时保持当前位置。比如说，你可能想 要写一段代码读取日志文件中的最后1000个字符，或者向一个文本文件中的特定位置写入一些价 格数据。

```java
// 读取文件中最后1000字节的数据
try (FileChannel channel = FileChannel.open(Paths.get("/Users/bytedance/gitRepo/demo/1.txt"), StandardOpenOption.READ)) {
    ByteBuffer buffer = ByteBuffer.allocate(1024);
    channel.read(buffer, channel.size() - 1000);
}
```

NIO.2另一个新特性是异步能力，这种能力对套接字和文件I/O都适用。异步I/O其实只是一 种在读写操作结束前允许进行其他操作的I/O处理。




