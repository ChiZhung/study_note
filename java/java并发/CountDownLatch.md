### CountDownLatch类

类似PV操作，解决一个线程等待其他线程完成之后再执行的场景

此外，**CountDownLatch实例的使用是单次的，一旦从构造器中传入的数值在count down减为0后，就不再重置**

使用基于AQS来实现，使用基于共享模式来实现，对于await，仅仅就是将线程park到FIFO队列上；对于countDown，其实就是执行tryReleaseShared（由于每一个线程执行这个方法就会让state--，然后返回state是否为0作为真假），Release到0的之后，就会unpark阻塞在FIFO队列上的线程；

```txt
                    +
                    |
+------------------>+
   thread1          |
                    |
                    |
+------------------>+
   thread2          +-------------------->
                    |      threadN+1
                    |
                    |
+-------------->    |
   threadN          |
                    +
                门栓(时间点)

先到达的时间点的线程会等待还没有到达的线程，就像等待threadN，一旦所有线程都达到之后，threadN+1就会立刻执行
```


```java
// 等待三个线程执行完之后主线程再执行
public class Demo {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(3);

        IntStream.rangeClosed(1, 3).forEach(i -> {
            new Thread(() -> {
                try {
                    Thread.sleep(1000);
                    System.out.println("Hello");
                } catch (InterruptedException e) {

                } finally {
                    countDownLatch.countDown(); // 注意：一定要放在finally中
                }
            }).start();
        });

        try {
            countDownLatch.await();
        } finally {
            System.out.println("Over");
        }
    }
}

```