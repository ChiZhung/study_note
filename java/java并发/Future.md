### interface Future
```java
public class Demo {
    public static void main(String[] args) {
        Callable<Integer> callable = () -> {
            System.out.println("before");
            int nextInt = new Random().nextInt();
            System.out.println("after");
            return nextInt;
        };

        RunnableFuture<Integer> task = new FutureTask<>(callable);
        
        new Thread(task).start();
        
        try {
            task.get();
        } catch (InterruptedException | ExecutionException e) {
            
        }
    }
}
```
* 这个接口定义的就是当使用get方法的时候，一旦计算结果没有得到，则会发生阻塞。

#### class CompletableFuture
从java8引入的弥补Future的不足，对于Future是一份伪异步的，当获取get时仍会阻塞，通过使用CompletableFuture，
通过执行
```java
public class Demo {
    public static void main(String[] args) throws InterruptedException {
        // 提供处理返回
        String s = CompletableFuture.supplyAsync(() -> "hello").thenApplyAsync(v -> v + " world").join();
        System.out.println(s);

        // ************
        // 提供异步消费
        CompletableFuture.supplyAsync(() -> "hello").thenAccept(System.out::println);
        // ************
        // 提供异步combine
        String s3 = CompletableFuture
                .supplyAsync(() -> "hello")
                .thenCombineAsync(
                        CompletableFuture.supplyAsync(() -> "world"),
                        (s1, s2) -> String.join(" ", s1, s2)
                ).join();
        System.out.println(s3);
        // ***********
        // 非阻塞
        CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(() -> {
            // 线程是daemon的，且是来自sync pool中的
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("run");
        });

        Thread.sleep(5000);

        // 当run async执行完毕，本回调就会执行
        completableFuture.whenComplete((v, a) -> System.out.println("over"));

    }
}

```