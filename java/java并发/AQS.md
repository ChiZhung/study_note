### AQS(AbstractQueueSynchronizer)

class AbstractQueueSynchronizer提供了一个实现阻塞锁与相关的同步器，这个实现依赖与FIFO的等待队列。
本类被设计用来作为一个基础的同步器，其依赖与一个原子int作为状态。子类必须实现protected方法来修改状态。
鉴于此，子类必须通过原子操作（setState and setState或者casState）来更新状态。

注意，子类应该定义为非公共的内部帮助类以用于实现一个其闭包类的同步属性，本抽象类没有实现任何抽象接口，
代替的是，其定义了像acquireInterruptibly。

这个类支持互斥或者共享模式，在互斥模式下已经获取时，另一个线程获取就不能成功。在共享模式下，若多个线程同时
获取的话可能成功。

**java自带的所有的Lock实现，其都是基于AQS的组合关系，也就是Lock的实现类中有一个AQS的字段，调用Lock的的方法其实就是调用AQS的方法**

#### AQS的宏观
```text
+-----------------------+
|                       |        +-------------------------+
|          AQS          +------->+        FIFO队列           |
|                       |        +-------------------------+
+-+-------+-------+----++
  |       |       |    |
  |       |       |    |
+-v-+    +v-+   +-v+  +v--+
|   |    |  |   |  |  |   |
+---+    +--+   +--+  +---+
|   |    |  |   |  |  |   |
+---+    +--+   +--+  +---+
|   |    |  |   |  |  |   |
+---+    +--+   +--+  +---+
|   |    |  |   |  |  |   |
+---+    +--+   +--+  +---+
|   |    |  |   |  |  |   |
+---+    +--+   +--+  +---+
cond1    cond2  condi condn

AQS就是维护若干条件变量与一个FIFO对象的关系，这个FIFO队列是使用基于线程封装Node的双向链表的实现（是一个带头节点的双向链表）；
这个AQS的FIFO可以理解为lock.lock()时候获得不到锁就排队阻塞在FIFO队列中，对于cond就可以理解为lock.newCondition()的关系
```

对于AQS，其最重要的一个字段就是int类型的state，用来表示状态，对于使用AQS来实现排他锁，则state的int值对于可重入场景就是一直加一，unlock就
减1，当减到0的时候就可以被其他线程征用了，对于读写锁场景，将32bit的state拆分为两个部分：高16bit就是要来标记读，低16位用来标记写；

#### AQS的基本方法
* tryAcquire(int arg)
    * 一个基于排他的锁获取
* tryRelease(int arg)
    * 一个基于排他的锁释放
* tryAcquireShared(int arg)
    * 一个基于共享的锁获取
* tryReleaseShared(int arg)
    * 一个基于共享的锁释放
* isHeldExclusive() ->boolean
    * 返回这个锁是共享还是排他锁
---
 
#### ReentrantLock 
* 公平锁与非公平锁
    * 公平锁就是在获取锁之前若FIFO不空时先进入FIFO中阻塞排队
    * 非公平锁就是，直接尝试获取锁，能获取就直接执行，否则进入FIFO排队阻塞
* ReentrantLock中基于AQS实现的FairSync，调用公平的ReentrantLock::lock就是调用FairSync::lock，也就是调用AQS::acquire(1)    
```java
// AQS::acquire
public final void acquire(int arg) {
    if (!tryAcquire(arg) &&
        acquireQueued(addWaiter(Node.EXCLUSIVE), arg)) // 将本线程加入FIFO并开始阻塞式的等待
        selfInterrupt(); // 中断本线程
}

// FairSync::tryAcquire
protected final boolean tryAcquire(int acquires) {
    final Thread current = Thread.currentThread();
    int c = getState();
    if (c == 0) { // 没有任何线程拿这个锁
        if (!hasQueuedPredecessors() && // 判断FIFO中getFirstQueuedThread() != Thread.currentThread() && hasQueuedThreads()
            compareAndSetState(0, acquires)) { // 将0改成了acquires
            setExclusiveOwnerThread(current); // 设置一个排他的owner线程
            return true;
        }
    }
    else if (current == getExclusiveOwnerThread()) { // 若为线程重入场景
        int nextc = c + acquires;
        if (nextc < 0)
            throw new Error("Maximum lock count exceeded");
        setState(nextc);
        return true;
    }
    return false;
}
```
* NonfairSync的实现
```java
// NonfairSync::lock
final void lock() {
    if (compareAndSetState(0, 1)) // 也就是来了就直接尝试获取这把锁通过CAS操作
        setExclusiveOwnerThread(Thread.currentThread()); // 将本线程设置为锁的排他线程
    else
        acquire(1); // 排队阻塞FIFO，AQS::acquire，里面使用的tryAcquire使用的是nonfairTryAcquire
}

// AQS::acquire
public final void acquire(int arg) {
    if (!tryAcquire(arg) &&
        acquireQueued(addWaiter(Node.EXCLUSIVE), arg))
        selfInterrupt(); // 表示阻塞在AQS中的FIFO阻塞队列的本线程遇到中断而被唤醒，且唤醒后又得到了锁
}


// NonfairSync::tryAcquire
protected final boolean tryAcquire(int acquires) {
    return nonfairTryAcquire(acquires);
}

// Sync::nonfairTryAcquire
final boolean nonfairTryAcquire(int acquires) {
    final Thread current = Thread.currentThread();
    int c = getState();
    if (c == 0) {
        if (compareAndSetState(0, acquires)) { // CAS直接尝试获取锁，不会排队FIFO
            setExclusiveOwnerThread(current);
            return true;
        }
    }
    else if (current == getExclusiveOwnerThread()) {
        int nextc = c + acquires;
        if (nextc < 0) // overflow
            throw new Error("Maximum lock count exceeded");
        setState(nextc);
        return true;
    }
    return false;
}
```

* ReentrantLock::unlock
    * AQS::release(int arg)
    
```java
// AQS::release
public final boolean release(int arg) {
    if (tryRelease(arg)) { // 锁完全释放
        Node h = head;
        if (h != null && h.waitStatus != 0)
            unparkSuccessor(h); // 唤醒FIFO后继节点
        return true;
    }
    return false;
}

// ReentrantLock.Sync::tryRelease
protected final boolean tryRelease(int releases) {
    int c = getState() - releases;
    if (Thread.currentThread() != getExclusiveOwnerThread()) // 保证获取锁与释放锁为同一个线程对象
        throw new IllegalMonitorStateException();
    boolean free = false;
    if (c == 0) {
        free = true;
        setExclusiveOwnerThread(null);
    }
    setState(c);
    return free;
}
```

* 过程
    * 调用lock.lock()首先根据锁的类型
        1. 对于非公平锁直接尝试获取锁，获取成功则执行，
            获取失败则挂在AQS的FIFO阻塞队列末尾（进入之前也会检查一次时候本次挂入的节点是否为头节点为头节点，
            为头节点又会尝试获取一次锁）;然后在FIFO中park，遇到中断或者唤醒在尝试获取锁直到成功
        2. 对于公平锁，就是在尝试获取锁的时候得先在AQS的FIFO队列排队，其他处理跟非公平锁一致的
    * 所以AQS的实现跟类似于synchronized关键字的底层cpp实现，sync关键字的实现是基于一个monitor对象来实现的，对于monitor
        其由一个entry list与一个wait set，entry list就类似由AQS中的FIFO阻塞队列，对于wait set就类似于AQS中的对个cond条件等待队列
        
#### ReentrantReadWriteLock
对于读写锁使用的AQS，其32位state字段逻辑上分为两部分，高16位用于读，低16位用于写

```java
// 读锁lock
protected final int tryAcquireShared(int unused) {
    Thread current = Thread.currentThread();
    int c = getState();  // 获得当前state的值
    if (exclusiveCount(c) != 0 && // 当前第16位写状态不为0（也是写锁已经被占）
        getExclusiveOwnerThread() != current) // 当前的线程不是已经占锁的线程
        return -1;  // 失败
    int r = sharedCount(c); // 获取共享的数量
    if (!readerShouldBlock() && // 当前线程可以不用再FIFO中阻塞等待，直接开始尝试拿读锁
        r < MAX_COUNT && // 拿读锁不超过最大限制数量
        compareAndSetState(c, c + SHARED_UNIT)) {   // 将state的高16位写状态+1
        if (r == 0) {   // 可以拿读锁，且读锁现在没有被任何线程持有
            firstReader = current; // 标记本线程为第一个持有者
            firstReaderHoldCount = 1;  // 第一个持有者可冲入持有的次数为1
        } else if (firstReader == current) { // 读锁已经被持有，但是第一个持有线程是本线程
            firstReaderHoldCount++; // 冲入持有的次数为1
        } else { // 本线程可以持有这个读锁，但是其不是第一个持有者
            // HoldCounter是一个标识某线程已经重入锁的次数
            HoldCounter rh = cachedHoldCounter; // cachedHoldCounter是上一次访问tryAcquireShared方法缓存下来的hc
            if (rh == null || rh.tid != getThreadId(current)) // rh不存在或者以存在的rh不是本线程对应的rh
                cachedHoldCounter = rh = readHolds.get(); // readHolds是一个ThreadLocal，获取本线程对应的rh
            else if (rh.count == 0) // 本线程还没没有重入次数
                readHolds.set(rh);  // 将本rh设置到thread local中
            rh.count++; // 重入次数+1
        }
        return 1; // 成功
    }
    return fullTryAcquireShared(current); // 进一步尝试获取读锁
}
// 对于读锁来说，state字段的高16位标识读锁的状态，其取值范围为(0-65535)表示为最多可以同时持有读锁的线程数
// 对于每个已经持有读锁的线程，通过HoldCounter实例来标识这个线程已经重入的次数(holdCounter.count)，HoldCounter跟线程的绑定时通过
// ThreadLocal来实现的，其中holdCounter.tid用来标识线程的唯一id

// 写锁lock
protected final boolean tryAcquire(int acquires) {
    Thread current = Thread.currentThread();
    int c = getState();
    int w = exclusiveCount(c); // 获取低16位的值
    if (c != 0) { // 也就是存在已有的读锁或者写锁
        // (Note: if c != 0 and w == 0 then shared count != 0)
        if (w == 0 || current != getExclusiveOwnerThread())  // 也就是存在读锁 或者 存在写锁时非本线程的写
            return false; // 失败
        if (w + exclusiveCount(acquires) > MAX_COUNT) // 指的就是本次写为本线程的重入写，但是可重入次数不得超过0xffff
            throw new Error("Maximum lock count exceeded");
        // Reentrant acquire
        setState(c + acquires); // 设置写state
        return true; // 成功
    }
    // c == 0也就是即不存在读也没有写
    if (writerShouldBlock() || // 判断本次写是否需要排队阻塞在AQS的FIFO队列中 
        !compareAndSetState(c, c + acquires)) // CAS设置写state失败
        return false; // 失败
    setExclusiveOwnerThread(current); // 将本线程设为互斥独占线程
    return true; // 成功
}

// 对于state字段中的低16位用来标识写的状态，这个状态值仅仅用来标识占用写锁的那个线程已经重入的次数
```

下边是对于读写锁的unlock
```java
// 读锁的unlock
protected final boolean tryReleaseShared(int unused) {
    Thread current = Thread.currentThread();
    if (firstReader == current) { // 本线程时第一个占用读锁的线程
        // assert firstReaderHoldCount > 0;
        if (firstReaderHoldCount == 1) // 本线程（第一个占用读锁的线程）的重入锁计数为1
            firstReader = null; // 将第一个占用线程置null
        else
            firstReaderHoldCount--; // 将第一个线程占用的读锁的重入次数减一
    } else { // 本线程不是第一个占用读锁的线程
        HoldCounter rh = cachedHoldCounter; // 获取上一轮的已缓存的hc
        if (rh == null || rh.tid != getThreadId(current)) // 当没有缓存或者缓存不是本线程
            rh = readHolds.get(); // rh设置为本线程绑定的hold counter
        int count = rh.count; // 获取本线程的重入次数
        if (count <= 1) { // 若次数为<=1
            readHolds.remove(); // 在thread local中移除本thread绑定的hold counter
            if (count <= 0) // 若次数为<=0
                throw unmatchedUnlockException();
        }
        --rh.count; // 将重入次数减一
    }
    for (;;) { // 通过死循环确保CAS操作成功
        int c = getState(); // 获取state字段的值
        int nextc = c - SHARED_UNIT; // 将state的读域减一
        if (compareAndSetState(c, nextc)) // 通过CAS更新成功
            // Releasing the read lock has no effect on readers,
            // but it may allow waiting writers to proceed if
            // both read and write locks are now free.
            return nextc == 0; // 通过state字段等于0的时候返回true，表示这个时候资源是可写状态
    }
}

// 写锁的unlock
protected final boolean tryRelease(int releases) {
    if (!isHeldExclusively()) // 表示当前线程是否获得了互斥锁
        throw new IllegalMonitorStateException();
    int nextc = getState() - releases;  // 更新后的状态
    boolean free = exclusiveCount(nextc) == 0; // 获取更新后的state的低16位
    if (free) // 也就是state为0
        setExclusiveOwnerThread(null);
    setState(nextc);
    return free;
}
```

#### 对于Condition的await与signal
对于await，就是将线程等待一个AQS上一个条件队列上，当signal后，会取出这个条件队列中的头，（就是唤醒原来执行cond.await的某个线程）
然后就追加到AQS的阻塞队列的末尾；（也类似sync一样，一个线程从wait中苏醒并不一定可以立即执行，首先要拿到对象锁才能恢复执行）

#### Lock与synchronized的对比
它们之间的对比，其是就是Lock所基于的AQS的实现与synchronized基于的底层c++的实现的区别：

* synchronized
    * 首先，sync关键字的实现，就是每个java对象其都会依赖与cpp层面的一个monitor对象，这个对象存在在java的对象头中，
        这个monitor对象会由两个集合，一个是WaitSet，一个是EntryList，首先WaitSet中的就是调用java对象的wait方法后等待在这个集合上的线程
        （每一个线程被封装后才成cpp中的一个Node对象）
    * 而对于EntryList这个集合，其存放的就是获取monitor的互斥锁获取不到而陷入阻塞状态的线程对象
    * 当一个处于等待状态的线程被notify后，它就会从WaitSet，移到EntryList中，移到entry list后，就根据争抢策略来争抢互斥锁
* AQS
    * AQS中存在两种队列，一种是条件队形，一种是AQS自身的阻塞队列
    * 当从条件队列上的一个线程被signal后，其会首先从这个等待队列中移除，然后加入到AQS阻塞队列
* 区别
    * 对于sync与AQS，synchronized基于的**仅仅是排它锁**，而对于AQS，其**既支持排他锁有支持共享锁**的实现（比如可重入读写锁的实现）
    * 对于sync，仅仅只能通过调用对象的wait跟notify来等待或者唤醒（一个WaitSet），而对于AQS，由于其支持n多个条件（对应于多个条件队列）