#### synchronized关键字的详解
* 在字节码中，对于sync关键字修饰，语句块与方法是不一样的：
    对于sync修饰的语句块，在字节码中显示为monitorenter与monitorexit字节码指令包围，
    而对于sync修饰的方法，则不会出现monitorentor与monitorexit字节码，仅仅在方法访问符标志位
    则多一个ACC_SYNCHRONIZED。

jvm中获得同步是基于进入与退出监视器对象（管程对象，Monitor）来实现的，每个对象实例都会有一个monitor对象，monitor对象
会和java对象一同创建，并销毁，monitor对象是由c++实现的。

当多个线程同时访问一段同步代码的时候，这些线程就会被放到一个EntryList集合中，处于阻塞状态的线程就会被放到该列表中来，
接下来，当线程获取到该对象的monitor时，monitor是依赖于底层os的mutex lock来实现互斥的，线程获取mutex成功，则
会持有该mutex，这时其他线程就无法再获取到该mutex

若线程调用了wait方法，那么该线程会释放掉所有的mutex，并且该线程会进入到waitset中，等待下一次被其他线程调用notify/notifyAll
唤醒，若当前线程顺利执行完毕方法，那么它会释放所持有的mutex。

对于对象的排他锁的争用产生的阻塞等待的集合就是EntryList，而对于obj的wait方法的等待集合（其实也可以称为阻塞集合）为WaitSet

#### Monitor对象
monitor对象就是由c++实现的，对于jvm在object monitor的实现，见openjdk的/src/share/vm/runtime/objectMonitor.hpp and 
/src/share/vm/runtime/objectMonitor.cpp

由于一个java对象绑定一个cpp的monitor对象，其实这个monitor对象存在于java的对象头中
```cpp
    // cpp中ObjectMonitor类的构造函数

  // initialize the monitor, exception the semaphore, all other fields
  // are simple integers or pointers
  ObjectMonitor() {
    _header       = NULL;
    _count        = 0;  // 引用计数，用于阻止回收，约等于 |_WaitSet| + |_EntryList|
    _waiters      = 0,
    _recursions   = 0;  // 用于标记monitor的重入的次数，初始值是0
    _object       = NULL;
    _owner        = NULL; // void* 类型，用于指向拥有本monitor的owner（thread or BasicLock）
    _WaitSet      = NULL; // wait set， 其是ObjectWaiter类（是线程的代理，继承自StackObj，其实底层基于链表实现）
    _WaitSetLock  = 0 ; // 保护等待队列(也就是_WaitSet, openjdk的等待队列的实现就是一个双向循环链表)，是一个简单的自旋锁
    _Responsible  = NULL ;
    _succ         = NULL ;
    _cxq          = NULL ;  // 最近到达的阻塞在entry上的线程列表，这个列表的元素其实是WaitNode，是对线程的代理
    FreeNext      = NULL ;
    _EntryList    = NULL ; // 获得本monitor但是没有获取到的那些线程放置在这里，也是ObjectWaiter类型的
    _SpinFreq     = 0 ; // 以下两个字段，当一个线程在到达sync时拿不到monitor，先会先自旋一会要是还拿不到锁，就会进入entry list
    _SpinClock    = 0 ;
    OwnerIsThread = 0 ; // 测试owner是否为线程还是BasicLock
    _previous_owner_tid = 0;
  }


// ObjectWaiter类（一个双向链表，每个节点就是一个线程的代理）
class ObjectWaiter : public StackObj {
 public:
  enum TStates { TS_UNDEF, TS_READY, TS_RUN, TS_WAIT, TS_ENTER, TS_CXQ } ;
  enum Sorted  { PREPEND, APPEND, SORTED } ;
  ObjectWaiter * volatile _next;
  ObjectWaiter * volatile _prev;
  Thread*       _thread;
  jlong         _notifier_tid;
  ParkEvent *   _event;
  volatile int  _notified ;
  volatile TStates TState ;
  Sorted        _Sorted ;           // List placement disposition
  bool          _active ;           // Contention monitoring is enabled
 public:
  ObjectWaiter(Thread* thread);

  void wait_reenter_begin(ObjectMonitor *mon);
  void wait_reenter_end(ObjectMonitor *mon);
};
```

![](imgs/sync_img.png)