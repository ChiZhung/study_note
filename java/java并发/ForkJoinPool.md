### ForkJoinPool
ForkJoinPool跟ThreadPoolExecutor属于同一层次的Pool，都是继承自AbstractExecutorService

当存在于这样一个场景：一个大任务一个小任务，当使用ThreadPoolExecutor的时候，小的任务很快就执行完了，但是对于大任务，半天才执行完；
这就会导致线程池中有的线程处于忙碌状态，而有的线程则处于闲置状态，而对于ForkJoinPool就像想将一个大任务拆分成若干的小任务，从而提高线程的利用率，
这个如何拆分的判定是有程序来指定的。
    
    
* 对于ForkJoinPool其使用自身特有的ForkJoinTask作为任务封装
    * 两个重要子类
        * 两个类的区别可以对比理解为Runnable跟Callable，若一个提交没有返回结果，继承使用RecursiveAction；
            若有返回结果就继承使用RecursiveTask
        1. RecursiveAction
        2. RecursiveTask
        ```java
        public class Main {
        
            public static void main(String[] args) {
                final ForkJoinPool pool = new ForkJoinPool(2);
                int rst = pool.invoke(new MyTask(1, 100));
                System.out.println(rst);
            }
        }
        
        class MyTask extends RecursiveTask<Integer> {
        
            private final int limit = 4;
            private final int start;
            private final int end;
        
            MyTask(int start, int end) {
                this.start =start;
                this.end = end;
            }
        
            @Override
            protected Integer compute() {
                int rst = 0;
                final int gap = end - start;
                if (gap <= limit) {
                    for (int i = start; i <= end; i++) {
                        rst += i;
                    }
                } else {
                    final int mid = (start + end) >> 1;
                    final MyTask left = new MyTask(start, mid);
                    final MyTask right = new MyTask(mid + 1, end);
                    invokeAll(left, right);
                    rst += (left.join() + right.join());
                }
                return rst;
            }
        }
        ```
        
        
#### CompletionService
这个接口提供了一个用于解耦任务产生与任务产生结果的消费，生产者会提交用于执行的任务，消费者会拿到已经完成了的任务，并以任务完成的时间先后顺序
来处理这些结果；一个CompletionService实例可以用于管理异步IO。

**也就是我们将一系列的任务扔到CompletionService中，他会将这些任务完成的顺序将结果Future放置到其内部队列中，并可以通过take方法来等待获取**

典型地，一个CompletionService实际上单独依赖一个Executor来执行若干请求，这个情况下，CompletionService实例仅仅管理一个内部完成队列；
ExecutorCompletionService实现类就提供了这种方式的实现。

* 方法
    1. submit(Callable/Runnable/Runnable, T) -> Future<T>
    2. take() -> Future<T>
        * 从内部完成队列队列检索并移除代表下一个完成任务（也就是队列中标识的next）的Future实例，若没有则等待
    3. poll() -> Future<T>
        * poll是非等待的take版本
    4. poll(long, TimeUnit) -> Future<T>
        * 等待若干时间，若还是没有就返回null

* 实现类ExecutorCompletionService
    * 三个内部字段
        ```java
        private final Executor executor; // 用于执行任务的线程池(ThreadPoolExecutor/ForkJoinPool)或者是普通的自定义的执行器
        // 这个主要就是判断上面的executor是不是AES实例，是的话就进行转换，这样做的原因就是需要将用户提交的比如Callable封装成RunnableFuture，
        // 对于ThreadPoolExecutor来说，就是生成一个FutureTask，对于ForkJoinPool就是生成一个ForkJoinTask
        private final AbstractExecutorService aes;    
        // 放入的机制就是提交完任务后，对任务首先进行包装为FutureTask/ForkJoinTask，然后再使用wrapper模式套了一层变成了QueueingTask，重写了
        // RunnableFuture中的done方法，使得这个时候将结果Future放置到队列中
        private final BlockingQueue<Future<V>> completionQueue; // 用于存放任务执行完后按时间序列的队列
        ```
