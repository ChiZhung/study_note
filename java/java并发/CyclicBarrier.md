### CyclicBarrier类

使用的场景就是：若干个线程一起执行，当在一个阶段所有线程都到达一个点的时候（已经到达点的线程会阻塞等到没有到达的线程），
所有线程才会一起继续往下执行

使用可重入锁+一个条件实现，每一个线程调用await就停在条件上，每一次都会对于一个计数变量count--；当其等于0就会对条件进行signalAll；每一次reset就会进入下一代，也就会重置count的值为初始值；

**对于CyclicBarrier的实例，其跟CountDownLatch不同，一旦从构造器中传入的数值在一次次调用await方法减到0后，会自己重置为初始值**

```text
             +                   +                 +
             |                   |                 |
+----------->+     +------------>+    +------->    |
   thread1   |         thread1   |     thread1     |
             |                   |                 |
             |                   |                 |
+----------->+     +------->     |    +----------->+
   thread2   |         thread2   |     thread2     |
             |                   |                 |
             |                   |                 |
+-------->   |     +------------>+    +----------->+
   threadN   |         threadN   |     threadN     |
             |                   |                 |
             +                   +                 +
           时刻1                时刻2              时刻3


先到达的时间点的线程会等待还没有到达的线程，就像等待threadN，一旦最后达到的线程N到达后，就通知其他的所有线程，所有线程会都恢复执行
```


```java
// 等待三个线程执行完之后主线程再执行
public class Demo {
    public static void main(String[] args) throws InterruptedException {
        CyclicBarrier cb = new CyclicBarrier(3);
        Random r = new Random();

        IntStream.rangeClosed(1, 3).forEach(i -> {
            new Thread(() -> {
                try {
                    Thread.sleep(r.nextLong(2000));
                    System.out.println("before " + r.nextInt(3));
    
                    cb.await(); // 注意：一定要放在finally中

                    System.out.println("after " + r.nextInt(3));
                } catch (InterruptedException e) {

                }
            }).start();
        });
    }
}

```

* 构造器
    * (int)
    * (int, Runnable)
        * Runnable指的是在每一轮最后一个线程达到后触发