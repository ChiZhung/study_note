#### ThreadLocalRandom
使用Random是生产随机数会产生性能上的问题：比如两个线程由于并发同时拿到了同一个老的种子值，由于随机计算的值是通过函数计算得到的，
这也就造成了两个得到的结果一致；random为了解决这个问题，通过引入cas机制，就可以使得每个线程都拿到不同的随机数。由于使用了cas机制，可能
会使得在大量线程获取随机数的时候会大量执行频繁的cas操作；
```java
protected int next(int bits) {
    long oldseed, nextseed;
    AtomicLong seed = this.seed;
    do {
        oldseed = seed.get();
        nextseed = (oldseed * multiplier + addend) & mask;
    } while (!seed.compareAndSet(oldseed, nextseed)); // 使用cas机制
    return (int)(nextseed >>> (48 - bits));
}
```

为了解决random的问题，在java7的时候引入了ThreadLocalRandom类，这个类的随机数生成时线程隔离的；对于随机值的获取，其主要由两部分组成：
一个是带状态的种子，一个是不带状态用于求随机值的函数；所以对于thread local random的实现，对于seed的存放是放置在Thread类中的字段中的（
既线程依赖的），但是对于计算随机值的函数则是出于公共的

```java
public class Main {
    public static void main(String[] args) {
        final ThreadLocalRandom random = ThreadLocalRandom.current(); // 获取全局唯一的thread local random实例
        final int i = random.nextInt();
    }
}
```