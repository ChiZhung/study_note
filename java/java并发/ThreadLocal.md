#### class ThreadLocal
```text
                                 +-----------------------------------------------+
                                 | +-------------------------------------------+ |
                                 | |             |                |            | |
                                 | |             |  ThreadLocal   |    Key     | |
                +-----+          | |             |   （弱引用）     |            | |
    get/set     |     |          | |             |                |            | |
+<--------------+     |          | +-------------------------------------------+ |
|               +-----+          | |             |                |            | |
|             ThreadLocal        | |             |       V        |    Value   | |
|                                | |             |                |            +<---------+Entry[]
|                                | |             |                |            | |
|           +-----------------+  | +-------------------------------------------+ |
|           |                 |  +-----------------------------------------------+
|           | ThreadLocalMap  |           ^     ThreadLocalMap
+---------------->tlm +-------------------+
            |                 |
            +-----------------+
                  Thread

一般想法就是：一个ThreadLocal实例中维护一个map，map的key就是thread，val就是线程独有的值（这样的侧重方就是ThreadLocal），
但是这会带来一些问题：比如多线程对于同一个map访问的并发控制，已经map维护所带来的强引用造成的内存泄漏的问题

但是实际是：侧重点在Thread上，每一个Thread都有一个ThreadLocalMap，存放所有跟
本线程有关的ThreadLocal，也就是说在取一个ThreadLocal实例中与本线程绑定的那个变量，首先，取出与本线程绑定的ThreadLocalMap，然后在
这个map中根据本ThreadLocal作为key取出val（也就是让thread来侧重管理该线程的所有的变量）

对于ThreadLocalMap的设计不是使用拉链法，而是使用简单的线性探测法

由于ThreadLocalMap被一个线程所持有，故只要一个线程不退出，则thread local map一直得不到回收，也使得对于thread local map的key（thread local）来说，
存在引用，这就是为什么对于ThreadLocalMap中的Entry要继承WeakReference

static class Entry extends WeakReference<ThreadLocal<?>> {
    /** The value associated with this ThreadLocal. */
    Object value;

    Entry(ThreadLocal<?> k, Object v) {
        super(k); // 让key绑定弱引用
        value = v;
    }
}

一旦key thread local失去引用后(也就是这个程序中除了local map的key引用外，没有其他的引用时)，
在下次GC，这个entry的key就会被回收，那val怎么办呢，只要访问thread local实例（get/set），就会附加扫描entry table
将所有k为空的entry的val置空，然后给remove这个entry，由于使用的是线性探测法，所以需要对回收之后的区域进行rehash；

在使用thread local

try {
    // 使用到thread local的代码
} finally {
    // 不再使用thread local
    threadLocal.remove(); // 从本线程中移除threadLocal，并清理所有key为null的val    
}
```