#### Object::wait()

调用这个方法后，调用这个方法的那个线程将会一直等待，指导另一个线程调用这个对象的notify或者notifyAll方法，
同时，wait()等价于wait(0).在执行wait之前，必须拥有这个这个对象的排他锁（monitor），在wait后，本线程将处于等待
状态，**并暂时释放排他锁**，直到被其他线程notify时，再次恢复执行并又重新获得排他锁。
* wait(long timeout)从等待在对象的线程等待集合上恢复到去争夺对象的排斥锁状态：
    1. 另一个线程执行这个对象的notify方法，碰巧本线程被唤醒
    2. 另一线程调用本对象上的notifyAll方法
    3. 另一个线程调用本线程实例的interrupts方法
    4. 等待timeout时间，当timeout为0时等价于wait()方法
    * 接下来原本等待在本对象的线程等待集合中的那个线程会从本对象的线程等待集合中被剔除，然后从之前的obj.wait()方法恢复
    （由于进入wait状态时，本线程会释放对象排他锁），当恢复后（即从等待集合中剔除时，
    这个线程它会再次竞争夺取这个对象的排他锁，当得到这个对象的排他锁时，就可以恢复执行了（也就是此时的线程状态与在进入wait
    之前的同步状态一致）。
* **注意：仍然可能存在一个线程在不满足以上四种情况下的极端恢复情况（称之为伪唤醒spurious wakeup）,虽然这种情况发生的
    概率很小，但是仍然要代码保证同步安全，通过一个while(等待条件){obj.wait();}的方式来保证**
    ```java
    synchronized(obj) {
      while (someConditions()) {
          obj.wait(timeout);
      }
    }
    ```    
    * 更多有关的内容可以见Doug Lea写的《java并发编程》3.2.3节与《高效java编程》item50
* 若一个线程处于等待或者等待之前收到一个中断，则其会抛出中断异常。
* 只执行obj.wait之前应该获得obj的排他锁（用sync语句快包围）
* wait的cpp实现等价于ObjectWaiter::wait(jlong millis, bool interruptible, TRAPS)
    * 关键代码如下
    ```cpp
    // 这个Self就是调用wait的java线程
    ObjectWaiter node(Self); // 得到线程代理ObjectWaiter实例node
    // 设置当前线程的状态
    node.TState = ObjectWaiter::TS_WAIT;
    
    // 进入等待队列中（_WaitSet），这个队列在这里实现是一个双向循环链表，其还可以是一个优先队列，一个任意的数据结构
    // _WaitSetLock保护这个等待队列（_WaitSet）,通常这个等待队列仅仅被这个monitor的拥有者来访问
    Thread::SpinAcquire(&_WaitSetLock, "WaitSet - add");    // 通常这个过程很快，所以使用自旋锁
    // 将节点node放入wait set
    AddWaiter(node);    // 线程安全的添加
    Thread::SpinRelease (&_WaitSetLock); // 释放自旋锁
  
    // 释放monitor
    exit(true, Self); // exit(bool not_suspended, TRAPS)
    ```
    
#### Thread::sleep()

**在sleep期间，不会丢失任何排他锁的所有权**，这就是跟wait的显著区别

#### Object::notify()

拥有本对象排他锁时去任意通知等待在本对象的线程等待集合中的一个线程，当obj.notify()后从等待集合中剔除的线程，然后将这个线程放置到entry list当中，
然后根据调度策略来争夺obj这个排他锁（因为要阻塞等待在obj的排他锁上，等obj.notify退出它的sync语句快后释放obj的排他锁后排他锁才开始真正的争夺）
    
**千万分清java的的等待与阻塞概念：等待是对于obj.wait,notify,Thread.sleep而言的，也就是区分对象排他锁与对象的线程等待队列/集合的概念**

* notify方法的cpp实现ObjectWaiter::notify(TRAPS)   (TRAPS等价于java线程)
    * 关键代码
    ```cpp
    // 就是通知哪一个线程的调度策略
    int Policy = Knob_MoveNotifyee ;
  
    // notify也是一个不耗时的操作，所以使用自旋锁（不去使用重量级锁）
    Thread::SpinAcquire (&_WaitSetLock, "WaitSet - notify") ;
    // DequeueWaiter()方法就是取出_WaitSet中的第一个waiter，也就是队列的队首
    ObjectWaiter * iterator = DequeueWaiter() ;
  
    // 然后根据调度策略Policy来从iterator中来选取其中的waiter，对其进行通知
  
    //  使用os层面的唤醒
    ev->unpark() ;
    ```

#### Object::notifyAll()

唤醒等待在本对象上的所有线程
