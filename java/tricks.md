#### 一些小技巧
* 判断一个数字是否为2的幂
    * (val & -val) == val
    * 利用的就是负数的补码特性
    * 对于无符号数，可以使用val&(val-1)==0来判断是否为2的幂
* 对于round-robin(就是按顺序取数组中的值)
    * i = n++ % array.length
    * i = n++ & (array.length - 1)
        * 当array的长度是2的幂时，这个做法就是round-robin但是较之于取模效率更高
* 求一个正整数的掩码
    * 比如0b1001的掩码就是0b1111
    ```java
    static int mask(int n) {
        if (n <= 0) {
            throw new RuntimeException();
        }

        for (int i = 1; i <= 16; i <<= 1) {
            n |= (n >> i);
        }

        return n;
    }
    ```
* 对于一个正整数，求大于等于其的最小2次幂
    ```java
    static int minPower2(int n) {
        if (n <= 0) {
            throw new RuntimeException();
        }

        int ret = 1;
        while (ret < n) { // 最多计算31次
            ret <<= 1;
        }
        return ret;
    }

    static int minPower2_(int n) {
        if (n <= 0) {
            throw new RuntimeException();
        }
    
        // 使用n-1的目的就是为了解决n恰好为2的幂的情况，统一处理
        return mask(n - 1) + 1; // 使用掩码计算最多计算7次
    }
    ```
* 从数据流中读取数据的自适应的buf
    * 介绍
        * The RecvByteBufAllocator 可以根据反馈自动增加或者减少预测buf的大小，若之前的从某源中读取的字节
            完全填满已分配的buf，则会逐渐增加本buf的大小，若连续两次从某源中读取的字节不能填充到本buf的某一
            特定大小的界限，则会逐渐减少本buf的大小。否则会维持相同的预测值。
    * 默认的buf大小的静态值
        1. static final int DEFAULT_MINIMUM = 64;
            * buf默认最小值
        2. static final int DEFAULT_INITIAL = 1024; 
            * buf默认初始值
        3. static final int DEFAULT_MAXIMUM = 65536;
            * buf的默认最大值
    * SIZE_TABLE(用于调节buf大小的辅助数组)
        * int[] SIZE_TABLE = [i=16, .., i+=16, .., 512, .., i*=2, .., 0x7fffffff)
* 判断所有值均是非负值
    * a | b | ... | x < 0
* 对于一个对象其可以绑定一个或者多个实例（数组、列表等），若出现一个实例的情形较多的时候，就可以采取将本对象创建为Object类型，
    单个实例就直接赋给它，对于多个对象，就才用数组、列表的方法。（比如Netty中对于Future才用观察者模式的实现，用于存放的Listeners的对象就是
    一个Object类型，对于是单个监听器实例还是一个专门的容器使用instanceof来判断）