### 第44节 偏向于使用标准函数接口
随着java引入了lambda表达式，对于API的编写上已经发生了相当大的变化。比如对于之前我们
可能会使用模板方法模式来实现放子类实现具体的方法，但是现在这样已经有些落伍了。
替代方法就是提供一个静态工厂或者构造器通过接受一个函数对象来完成相同的功能。更一般的情况，你可以写更多的
构造器以及方法使得将函数对象作为其参数。同时使用正确的函数参数类型是需要关心的。

考虑LinkedHashMap，你可以使用这个类来实现一个cache的功能通过重写removeEldestEntry方法，这个
方法每当put方法被调用的时候就会被调用。当这个方法返回true的时候，则map就会移除最老的元素。
```
protected boolean removeEldestEntry(Map.Entry<K,V> eldest) { 
    return size() > 100;
}
```
虽然这个技术可以很好的工作，但是放在现在通过lambda可以让LinkedHashMap工作的更好。若今天重写LinkedHashMap，
它将会有一个持有函数对象的静态工厂或者构造器。看着removeEldestEntry方法的声明，你可能会认为函数对象参数为Map.Entry<K, V>然后
返回一个boolean值。而事实由于函数中会使用到LinkedHashMap的其他实例方法，所以定义上会函数对象接受两个参数
Map<K, V>作为LinkedHashMap自身，另外一个Map.Entry<K, V>作为最老元素。
```
// Unnecessary functional interface; use a standard one instead.
@FunctionalInterface 
interface EldestEntryRemovalFunction<K, V> {
    boolean remove(Map<K,V> map, Map.Entry<K,V> eldest); 
}
```
这个接口看上去可以很好的工作，但是不建议这样使用。因为没必要去声明一个新的接口来实现这样的功能。而建议
去使用java.util.function包提供的大量的标准函数接口。其中的一个原因有标准函数接口有提供默认方法实现。
在上面的情况，建议使用BiPredicate<Map<K, V>, Map.Entry<K, V>>来代替使用EldestEntryRemovalFunction。

存在43个函数接口在java.util.function包中，没必要去记住他们所有，但是需要记住6中基本的函数接口。

|接口|函数签名|例子|
|---|---|---|
|UnaryOperator<T>|T apply(T t)|String::toLowerCase|
|BinaryOperator<T>|T apply<T t1, T t2>|BigInteger::add|
|Predicate<T>|boolean test(T t)|Collection::isEmpty()|
|Function<T, R>|R apply(T t)|Arrays::asList|
|Supplier<T>|T get()|Instant::now|
|Consumer<T>|void accept(T t)|System.out::println|

同时对于这六种情况，对于int,long,double存在不同的三个变种。所以不要去使用装箱之后的基础函数接口，
直接使用原子类型函数借口即可。即较之于原子类型，不要去使用包装之后的原子类型，使用包装原子类型而造成的
性能开销可能是巨大的。

#### 什么时候需要自定义函数接口呢？
* 比如标准函数接口中不存在，或者存在但是自己的函数借口需要抛出异常
* OR
    1. 新的接口被广泛使用，且可以从其表述的命名上获益
    2. 这个接口有很强的协议与之相关
    3. 它可以极大的从其自定义默认方法中受益
    
当你选择写一个自定义函数接口的时候，一定要记住它是一个接口，所以其应该得到小心的设计


对于注解@FunctionalInterface，这个类似@Override，这个注解主要有三个目的。
一是其告知这个类以及其文档的读者，这个接口支持lambda。
二是这个接口中当仅有一个抽象方法的时候才会编译通过。
三是这个借口不支持添加第二个抽象方法。

所以对于你的函数接口请带上注解@FunctionalInterface

#### 使用函数接口的注意点
最后一个在API中对于函数接口的使用注意点，**不要在一个方法的不同重载版本中的相同参数位置使用可能造歧义吃的函数接口**。
正不仅仅只是一个理论问题，比如ExecutorService中的submit方法，其存在既可以传入Callable<T>或者Runnable的两种重载版本。
这对于客户端编写来说，再使用lambda后可能还要再加上强转声明才可以表明确切使用的是哪个重载版本。
一个简单避免这个问题的方法可以见第52节--明智地使用重载




