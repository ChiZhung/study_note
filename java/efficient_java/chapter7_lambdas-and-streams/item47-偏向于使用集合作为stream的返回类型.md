### 第47节 偏向于使用集合作为stream的返回类型
很多方法是返回元素序列的，在早于java8的时候像这些方法主要返回的数据类型是集合/迭代/数组类型。
通常，可以容易的确定返回什么数据元素类型。

* 不同情况使用不同的元素序列返回形式
    * 使用迭代器：若一个函数返回的元素序列支持foreach或者不支持实现集合接口(比如contains)，则使用Iterable接口是不错的。
    * 使用数组：若返回的元素是原子类型且有着严格的性能需求，那么这种情况则建议使用数组。
    * 使用集合

在java8中，java平台添加了stream，这使对于使用序列作为返回值的方法，在选在何种类型作为返回类型的时候，大大
增加了选择的难度。

你可能听说stream现在是作为序列返回一个明显的选择，但是在第45节的讨论中，因为stream并没有使得iteration过时，
所以写一个好的代码需要将stream与iteration进行理智地组合。当你一个函数的返回值设置为Stream的时候，若
用户想使用for-loop进行遍历的时候，用户会有些崩溃。因为for-loop是针对数组或者是Iterable对象的，更令人抓狂的
是，对于Stream它存在跟Iterable接口中同样的抽象方法iterator声明。由于BaseStream接口没有继承Iterable所以
对于Stream不可以直接使用for-loop，若想使用for-loop见下：
```java
public class Demo {
    public static void main(String[] args) {
        IntStream stream = IntStream.of(1, 2, 3, 4);
        // 不推荐的使用方式
        forLoop1(stream);
        // 推荐的方式
        forLoop2(stream);
    }
    
    private static void forLoop1(IntStream stream) {
        // 这种方式比较trick，不建议使用
        for (Integer integer : (Iterable<Integer>) stream::iterator) {
            System.out.println(integer);
        }
    }
    
    public static <T> Iterable<T> iterableOf(BaseStream<T, ?> stream) {
        return stream::iterator;
    }

    private static void forLoop2(IntStream stream) {
        // 推荐使用适配器模式
        for (Integer integer : iterableOf(stream)) {
            System.out.println(integer);
        }
    }
}
```

在第45节的统计字谜的stream版本代码中，使用Files.lines方法来读字典文件，然而迭代版本是使用
scanner。使用Files.lines方法比使用Scanner的方式更优越，因为前者屏蔽了读取文件产生的所有异常。
若一个API对于序列仅仅提供stream的方式进行访问，那么程序员将作出妥协，仅通过forEach来完成对stream
的遍历。

相反，若程序员想通过stream的方式来处理一个Iterable对象，虽然jdk没有提供一个iterable到stream的
适配器，但是可以自定义一个适配器方法如下：
```java
public static <T> Stream<T> streamOf(Iterable<T> iterable) {
    return StreamSupport.stream(iterable.spliterator(), false);
}
```

若你知道一个方法返回的序列使用在stream pipeline中的，那么你就自然地选择Stream作为返回值。
若一个方法返回的序列仅仅用于迭代，那么这个方法返回的就应该是类型Iterable。
但是若你正在写的一个其方法返回一个序列的公共API，**你应该为那些想写stream pipeline的用户以及想用foreach语句
提供这些功能**，除非你肯定大部分你的用户都只使用同一种机制。

**Collection是Iterable的子类型并且有一个stream方法，所以对于集合来说，其即提供了迭代又提供了stream的访问方式。
所以，Collection或者其子类型通常应该作为一个公共的，以序列作为返回结果的方法返回类型。**

对于数组，通过Arrays.asList方法提供迭代功能，通过Stream.of提供了stream访问。
**若一个你返回的序列足够小到可以轻松地放在内存中，最好返回标准的集合实现，比如ArrayList/HashSet，
但是不要将一个大序列存储在内存中，并将其以集合返回。** 若一个你返回的序列很大，但是可以以简单的形式表达，可以考虑
实现一个特殊实现的集合。比如你要返回一个集合的幂集，比如{a,b}的幂集就是{{},{a},{b},{a,b}}，也就是说一个元素个数为n的集合，
其幂集的大小为2^n个，所以不可能考虑将幂集存储在标准集合实现中，可以通过AbstractList的帮助来实现，trick手段是将幂集中的元素
与该元素的索引进行映射，我们将索引用一个n比特向量表示，这个向量中任一位上是1表示对应的元素存在，否则不存在。
```text
S = {a, b, c}
n = 3
000 -> {}
001 -> {a}
010 -> {b}
011 -> {a,b}
100 -> {c}
101 -> {a,c}
110 -> {b,c}
111 -> {a,b,c}
```
代码实现如下：
```java
public class Demo {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>(Arrays.asList("a", "b", "c", "d"));
        Set<Set<String>> powerSet = PowerSet.of(set);
        System.out.println(powerSet);
    }
}

class PowerSet<E> extends AbstractSet<Set<E>> {

    List<E> src;
    int size;

    private PowerSet(Set<E> set) {
        src = new ArrayList<>(set);
        size = 1 << src.size();
    }

    public static <E> PowerSet<E> of(Set<E> set) {
        Objects.requireNonNull(set);
        if (set.size() > 30) {
            throw new IllegalArgumentException("The size of original set is too large.");
        }
        return new PowerSet<>(set);
    }

    private Set<E> get(int idx) {
        Set<E> ret = new HashSet<>();
        for (int i = 0; idx != 0; i++, idx >>= 1) {
            if ((idx & 1) == 1) {
                ret.add(src.get(i));
            }
        }
        return ret;
    }

    @Override
    public Iterator<Set<E>> iterator() {
        return new Iterator<Set<E>>() {
            int i = 0;

            @Override
            public boolean hasNext() {
                return i < PowerSet.this.size();
            }

            @Override
            public Set<E> next() {
                return PowerSet.this.get(i++);
            }
        };
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean contains(Object o) {
        return o instanceof Set && src.containsAll((Collection<?>) o);
    }
}
```

在上述实现中，除了要实现Iterable接口之外，还要实现诸如contains与size方法。
当碰到实现这些方法不可行时，比如要处理的序列是一个不可提前知道其内容的，这个返回的值不是
Collection而是选用Iterable或者Stream更好，或者你也可以返回二者通过不同的方法。

有时你会仅根据实现的难易程度来选择返回类型。比如，加入你要写一个方法，这个方法要返回一个输入列表的所有子列表。
若简单地通过返回一个集合来实现，则可能由于子列表过大而内存中放不下，虽然相较于幂集的空间复杂度O(n^2)，子列表的空间复杂度只有O(n^2)。
```text
L = [a, b, c]
S_L = [
    [],
    [a],
    [a, b],
    [a, b, c],
    [b],
    [b, c],
    [c]
]
空间复杂度O(n^2)
```

代码实现上，采用stream
```java
public class Demo {
    public static void main(String[] args) {
        final List<String> list = Arrays.asList("a", "b", "c");
        sublist(list).forEach(System.out::println);
    }

    static <T> Stream<List<T>> sublist(List<T> li) {
        return Stream.concat(Stream.of(Collections.emptyList()), prefixes(li).flatMap(Demo::suffixes));
    }

    static <T> Stream<List<T>> prefixes(List<T> li) {
        Objects.requireNonNull(li);
        return IntStream.rangeClosed(1, li.size()).mapToObj(end -> li.subList(0, end));
    }

    static <T> Stream<List<T>> suffixes(List<T> li) {
        Objects.requireNonNull(li);
        int listSize = li.size();
        return IntStream.range(0, listSize).mapToObj(start -> li.subList(start, listSize));
    }
}
```
除了上述写法，其实可以直接使用两层for-loop来实现，同时可以基于两层循环的代码实现一版stream代码，
而且代码上会比上面实现的stream代码简洁，但是在可读性上，不及上面的stream版本。

在一个使用迭代更自然的场景下，对于stream版本代码，则需要使用stream-iterable的适配器来处理，这样
会一定程度使得代码杂乱，此外使用适配之后的迭代在执行速度上也有一定的额外开销。

#### 总结
当写一个方法，该方法返回的是一个元素序列时，用户对其的处理方式可能是stream也可能是以迭代方式处理。
可以尝试两种情况都是实现，当实现返回集合可行的话，可以去返回集合。若你已经知道一个集合中的元素或者这个
序列中的元素个数不大的话，考虑返回一个标准的集合比如ArrayList。否则，考虑实现一个自定义集合正如我们对于
本节中关于幂集的处理（仅对集合集合中若干方法进行实现）。若返回一个集合不可行的话，
可以考虑返回一个stream or iterable（前提是这样实现对于用户来说是自然的）。
若在java未来的发行版本中，将stream接口拓展继承Iterable，那么就可以直接返回返回stream了，因为可以直接进行stream以及迭代处理了。
