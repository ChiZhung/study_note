### 第43节 偏向于使用方法引用而不是lambda表达式
在使用lambda表达式的时候，一般情况下阅读性不是很好，但是可以将lambda中的代码筛选出来
放到一个新的方法中，并配以好的方法命名以及相关文档。

但是并不总是使用方法引用好于lambda表达式，比如当待使用的方法引用所在的类与使用lambda表达是同一个类中，则直接
使用lambda表示式较好，代码更短且更便于理解

```java
// case A
class GoshThisClassNameIsHumongous {
    void action() {
        // do sth
    }

    void func(Service service) {
        service.execute(GoshThisClassNameIsHumongous::action);
        // or
        service.execute(() -> action()); // 选择下者
    }
}
```

尽管Function.identity()提供了一个返回自己的功能，但是直接使用x -> x更好便于理解

| 方法引用类型 | 举例 | 等价的lambda表达式 |
| ---------- | --- | ---------------- |
| static | Integer::parseInt | str -> Integer.parseInt(str) |
| bound | Instant.now()::isAfter | Instant then = Instant.now(); t -> then.isAfter(t) |
| unbound | String::toLowerCase | str -> str.toLowerCase() |
| class constructor | TreeMap<K, V>::new | () -> new TreeMap<K, V> |
| array constructor | int[]::new | len -> new int\[len\] |

