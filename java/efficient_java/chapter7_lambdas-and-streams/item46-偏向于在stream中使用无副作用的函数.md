### 第46节 偏向于在stream中使用无副作用的函数
对于stream来说，最重要的组成部分就是将你的计算部分组织成stream管道中的一系列转换，每一个转运阶段尽可能放
在一个纯净函数中表示。

一个纯净函数其结果是依赖于其输入的，其不依赖与任何可修改状态，它也不修改任何状态。为了达到这种状态，
任何你传给stream操作的函数对象不管是中介函数终结操作，都应该是无副作用的。

```java

public class Demo {
    public static void main(String[] args) throws IOException {
        Map<String, Long> map = new HashMap<>();
        
        try (Stream<String> lines = Files.lines(Paths.get(args[0]))) {
            lines.forEach(word -> {
                // 涉及中间状态
                map.merge(word, 1L, Long::sum); // do not do like this
            });
        }
    }
}

// 改为
public class Demo {
    public static void main(String[] args) throws IOException {
        try (Stream<String> lines = Files.lines(Paths.get(args[0]))) {
            Map<String, Long> map = lines.collect(Collectors.groupingBy(x -> x, Collectors.counting()));
        }
    }
}
```
由于java程序员都知道如何去使用for-each循环，然后与之相似的就是forEach的终结操作，
**但是forEach操作在stream中介操作中是功能性最弱的，最不流式友好的**。它是显式迭代的，
因此其不适宜用来做并发处理。

**在stream中，forEach操作应该只被用于报告一个流式计算的结果而不是用来执行一个计算**。偶尔，使用
forEach在某些情形是有意义的，比如将一个流式计算的结果添加到一个已经存在的集合中。



