### 第48节 小心使用stream的并行模式
java与96年发行，它通过sync关键字以及wait/notify来内建支持线程；java5引入了并发库，支持了并发集合以及
执行器框架；java7中引入了fork-join包，一个高性能的并行分解框架。java8中引入了stream，仅通过调用parallel
方法即可让stream以并行模式执行。虽然在java中写并发程序变得更加简单，但是要让程序变的正确以及更快却仍然与以前一样
不容易。在并发编程中，常常会违反安全性（safety）以及存活性（liveness），这个在并行stream管道编程中同样存在。

在第45节中计算前20个梅森素数的示例中，使用stream管道的方式处理可以正常的结束，当使用并行stream管道的时候，
却出现了程序hang住的情况（liveness failure）。原因就是**若stream管道的源来于Stream.iterate或者执行了中间操作limit，
则使用并行模式并不会提升性能。** 理想情况，管道要解决这两个情况。糟糕的是，默认并行化策略通过假设处理
一些额外元素并丢弃任何不需要的结果没有害处来处理 limit 的不可预测性。在这种情况下，找到每个梅森素数所需的时间大约是找到前一个的两倍。
因此，计算单个额外元素的成本大致等于计算所有先前元素的总成本，而这个看似无害的管道使自动并行化算法陷入困境。
所以，**不要随意地并行化一个stream管道，否则性能结果可能将会是灾难性的。**

通常，**当stream基于ArrayList，HashMap，HashSet以及ConcurrentHashMap实例，数组，int ranges以及long ranges使用并行模式有着更好的性能效果。**
它们的公共特点就是可以精确地以及轻易地split成若干所需大小的subranges，这就使得并行线程执行分解工作变得容易。
stream中将执行拆分任务抽象成概念spliterator，对于Stream以及Iterable均有一个名为spliterator方法用于返回。
对于ArrayList，HashMap等数据结构一个重要的因素就是，对于顺序处理其提供一个良好的引用局部性。在内存中，顺序元素
是存储在一起的。而被这些顺序元素引用的对象在内存中并不连续，这会降低引用局部性。

事实上，引用的局部性对于并行化批处理式是非常重要的，没有它的话，线程将花费大量时间空闲，等待数据从内存传输到处理器的缓存中。
具有最佳引用局部性的数据结构是原始数组，因为数据本身在内存中是连续存储的。

流管道的终止操作符也可能会影响并行处理的效率，不要将大量的工作放置在终止操作中。对于并行来说，终止操作最好
只是做一个reduce的工作。对于像Stream::anyMatch, Stream::allMatch以及Stream::noneMatch这些短路操作
对于并行来说是合适的。对于那些由Stream::collect来执行的操作，被称为可修改的reduce，是不适合的，因为其在collect中的combine阶段的开销比较高。

当你在写属于自己的Stream/Iterable/Collection实现时，若你想要又一个较好的并行性能，你必须重写spliterator方法，
并广泛测试结果流的并行性能。写一个高质量的spliterator是困难的，也超出了本书的介绍内容。

对一个stream进行并行化，不仅可能会导致更差的性能，甚至会导致liveness failure以及不正确的结果以及不可预测的
结果（safety failures）。safety failures可能在用户mappers，filters，以及其他用户提供的函数对象没有成功地去遵循他们自己的规范的时候发生。
Stream规范对于其使用的函数对象有着严格的要求，比如对于accumulator与combiner函数被传给Stream的reduce操作，必须是关联的，不被外部干扰的，无状态的。
当你违反了这些规定，你可能在串行的时候可以得到正确结果但是在并行的时候，它将可能失败。

即使你使用了高效的splittable source stream，一个可并行的且代价小的终结操作以及一个不被干扰的函数对象，
你也可能不能从使用并行模式中受益，除非你流管道工作地非常好来抵消并行引入的开销。

一个经验估算值，对于并行模式，流处理的元素应该是处理每一个元素所需的代码行量的十万倍+

一定要记得，对于流并行来说，其是一个非常严格的性能优化。当要采用并行模式的时候，一定要先自己测试一下以保证其是值得的。
一般下，所有的并行流都是在一个公共的fork join池中运行的。单一的一个有着错误行为的流管道可能会伤及在pool中的其他不相关部分的性能。
一位维护大量使用流的数百万行代码库的熟人发现只有少数几个地方并行流有效。这并不意味着您应该避免并行化流。
在适当的情况下，只需向流管道添加并行调用，就可以在处理器内核数量上实现接近线性的加速。
某些领域，例如机器学习和数据处理，特别适合这些加速。

对于计算PI(n),表示计算n以内的素数的个数，使用并行流就非常合适
```java
public class Demo {
    public static void main(String[] args) {
        System.out.println(pi((long)10e8));
    }

    static long pi(long n) {
        return LongStream.rangeClosed(2, n)
                .parallel()
                .mapToObj(BigInteger::valueOf)
                .filter(i -> i.isProbablePrime(50))
                .count();
    }

}
```
要注意的是，虽然使用并行会比串行提速，但是不应该把这个方法作为计算PI(n)的方法，因为存在更好算法比如Lehmer算法

当你要并行化处理一个随机数流的时候，请使用SplittableRandom而不是ThreadLocalRandom，更不是Random
