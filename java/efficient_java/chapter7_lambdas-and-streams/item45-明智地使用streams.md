### 第45节 明智地使用streams
在java8中引入了stream API来简化若干操作，并以串行或者并行的方式进行。
这个API提供了两种关键抽象：stream表示一个有限或者无限的数据元素序列，stream pipeline
表示在其数据元素上的一个多阶段计算。

流元素可以来自任何地方，比如集合，数组，文件，正则表达式匹配匹配器，伪随机数生成器，或者是其他流。
数据元素可以是对象引用类型或者是原子类型，支持的三种原子包括int，long与double。

一个stream pipeline由一个source stream，零个或多个中间操作，和一个终结操作组成。其中每一个
中间操作将流以某种形式转运（比如map or filter）。中间操作将一个流转运为另一个流，新流中间的元素可能会发生变化
也可能不会发生变化。而终结操作是执行一个最终计算，通过将最后一个中间操作进行结果计算，比如将元素存储到集合中，返回一个确切的值亦或是打印其所有元素。

流管道是懒计算的，计算知道一个中介操作被调用的时候才会发生。通过懒计算是的对于无限元素的计算成为可能。
记住一个没有终结操作的流管道是不会发生计算的，所以对于流管道不要忘记带上一个终结操作。

stream API是丝滑的，一个stream pipeline仅仅只用一个表达式就可以表示出来。事实上，多管道也是
可以仅用一个表达式串在一起来表示。默认情况下，stream pipelines是串行执行的。若想使得一个stream pipeline
被并行执行，则紧急需要在pipeline中的任意一个流后面调用parallel方法即可，但是这种做法是不太合适的（详见第48节）。

stream API是十分多样化的，通过适当的使用流API可以使得代码更加简短以及清晰，
当不恰当地使用或造成程序阅读性差以及不易维护。虽然没有硬性或者简单的规则来指导何时来使用stream，但是仍存在
一些启发式的规则。

#### 一个例子
需求：从一个单词文件中读取单词，若一个单词组中任意两个单词的字母按字母表序排序后一致则视这个单词组
为字谜单词组。现在的需求就是找到所有的字谜组。并过字谜组大小小于阈值的字谜组。
```java
public class Demo {
    public static void main(String[] args) throws IOException {
        final String path = args[0];
        final int threshold = Integer.parseInt(args[1]);
        iterative(path, threshold);
        stream(path, threshold);
    }

    public static String alphabetize(String word) {
        final IntStream codePoints = word.chars();
        return codePoints
                .sorted()
                .collect(
                        StringBuilder::new, 
                        ((stringBuilder, value) -> stringBuilder.append((char) value)), 
                        StringBuilder::append
                )
                .toString();
    }

    // 常规做法
    private static void iterative(String path, int threshold) throws FileNotFoundException {
        File dict = new File(path);
        Map<String, Set<String>> groups = new HashMap<>();
        try (Scanner s = new Scanner(dict)) {
            while (s.hasNext()) {
                String w = s.next();
                groups.computeIfAbsent(alphabetize(w), (unused) -> new TreeSet<>())
                        .add(w);
            }
        }

        for (Set<String> group : groups.values()) {
            if (group.size() >= threshold) {
                // do sth.
            }
        }
    }

    // stream
    private static void stream(String path, int threshold) {
        Path dict = Paths.get(path);

        try (Stream<String> words = Files.lines(dict)) {
            words.collect(groupingBy(word -> alphabetize(word)))
                    .values()
                    .stream()
                    .distinct()
                    .filter(group -> group.size() >= threshold)
                    .forEach(li -> {
                        // do sth
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```

**在缺少显式类型的时候，一定要小心地对于lambda表达式的参数进行命名，使得其具有高可读性。**
通过使用helper方法可以帮助提升代码的可读性。

当你开始使用stream，你可能会感觉想去消除代码中的所有loop，但是一定程度上要抵抗这种想法。
因为这种特意消除可能会导致代码的可读性与可维护性变差。**一般上，复杂的任务最好的完成方式就是将
stream与iteration相结合使用**。

**仅在使用stream有意义的地方才把原有的代码改为stream，** stream pipeline使用函数对象来表示重复计算，
而对于迭代来说则是使用一段代码块来表示重复计算。

应该使用代码块而不是函数对象的情况：
意识代码块读改块外的局部变量，而对于函数对象只能进行final对象的只读；
其次还有一点就是代码块可以提前中断loop以及抛出可检查异常，而函数对象则不可以。

#### 使用stream便于的点
* 统一对序列中的元素进行处理
* 过滤序列中的元素
* 将序列进行combine
* 将序列中的元素组成一个集合，并且根据一些公共属性进行group by
* 从一个元素序列中搜寻一个满足一些标准的元素

若一个计算可以使用上述进行很好的表达，那么使用stream技术将会是一个很好的候选。对于stream来说，想同时访问对应元素的多个管道阶段
是困难的，比如一旦你将一个值map成另外一个值，那么这个原始值就丢失了。一个或许可行的方案就是当一个值map后，得到的是一个pair，
其由一个原始值与一个新值组成。但是这不是一个令人满意的解决方案，当pair对象被一个stream管道的多个阶段需要的时候，代码将变得很乱以及冗余（而stream的主要目的就是简化，直观）。


当你不确定是否使用stream还是迭代的时候，可以两者都试试。