## 第六章 枚举与注解
在java8中，函数接口，lambda表达式，方法引用被添加用来更方便地创建函数对象。
streams API与这些语言改变一起来提供系统库对于序列数据元素的处理支持。
在本章，将介绍如何去最好地利用这些工具。


---

### 第42节 偏向于使用lambda表达式而不是匿名内部类
历史上，接口或者带有一个抽象方法的抽象类被用来作为一个函数类型。它们的实例被作为是一个函数对象，用来代表函数或者
动作。自从97年jdk1.1的正式发布，创建一个函数对象的主要方式就是通过匿名内部类。

省略掉lambda表达式中参数的类型除非为了代码看上去更加清晰的目的。若编译器告知你不能断定lambda表达式参数的
类型，这个时候在加上参数类型即可。有时你不得不将返回值或者整个表达式进行强转换，但是这个情况比较少。

使用lambda表达式主要是代码最好是一行，或者代码最多是三行的情况，不然过多会影响代码的可阅读性。

注意，lambda表达式中的this指的是闭包实例,若要访问自身实例，请使用匿名内部类。需要注意的是
lambda表达式与匿名内部类一样，无法进行序列化，所以要想实现序列化的功能建议额外书写一个实现了Serializable接口的类

```java
public class Demo {
    static <A, B> Function<A, B> serialize(Function<A, B> func) {
        return (Function<A, B> & Serializable) func::apply;
    }

    static class TestA implements Serializable {
        // 不可序列化，由于func不是Serializable的实现类实例
        Function<Void, Void> func = (v) -> null;
    }

    static class TestB implements Serializable {
        // 可序列化，func是Serializable的实现类实例
        Function<Void, Void> func = (Function<Void, Void> & Serializable) (v) -> null;
    }

    static class TestC implements Serializable {
        // 想法很美好，就是使用序列化工具封装
        // 不可序列化，由于在serialize方法中func::apply中的func对象是不可序列化的
        Function<Void, Void> func = serialize((v) -> null);
    }

    public static void main(String[] args) throws IOException {
        Object[] objs = {
                new TestA(),
                new TestB(),
                new TestC(),
        };

        // 主要考察序列化与lambda表达式的知识
        ObjectOutput output = new ObjectOutputStream(System.out);
        for (Object obj : objs) {
            try {
                output.writeObject(obj);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

```

