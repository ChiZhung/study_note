### 第35节 对于枚举类型，获取每个枚举代表的int值不要使用其自带的ordinal方法而是使用自定义枚举实例字段代替

对于枚举，其自带一个ordinal方法就是enum.ordinal()返回其对应的int值，你或许通过这个来获取其
在枚举定义的中的所处位置。但是这样做并不好

```java
// Abuse of ordinal to derive an associated value - DON'T DO THIS
public enum Ensemble {
    SOLO, DUET, TRIO, QUARTET, QUINTET, SEXTET, SEPTET, OCTET, NONET, DECTET;
    public int numberOfMusicians() { return ordinal() + 1; }
}
```
虽然这样做可以,但是这样会暗藏噩梦，一旦常量重排序，使用这种方式就break了；所以绝对不要将一个通过enum.ordinal()来获取跟这个枚举挂钩的int值！
而是通过枚举实例字段来存储
```java
public enum Ensemble {
    SOLO(1), DUET(2), TRIO(3), QUARTET(4), QUINTET(5), SEXTET(6), SEPTET(7), OCTET(8), DOUBLE_QUARTET(8), NONET(9), DECTET(10), TRIPLE_QUARTET(12);

    private final int numberOfMusicians;

    Ensemble(int size) { this.numberOfMusicians = size; }
    
    public int numberOfMusicians() { return numberOfMusicians; }
}
```

对于枚举的ordinal方法，通常用法是被EnumSet/EnumMap来使用
