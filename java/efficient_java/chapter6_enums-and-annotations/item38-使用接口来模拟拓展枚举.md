### 第38节 使用接口来模拟拓展枚举

大多数情况下使用枚举都会比使用一些类型安全的枚举模式更好，但是使用枚举一个担忧点就是
其拓展性

```java
interface Operation {
    double apply(double a, double b);
}

enum BasicOperation implements Operation {
    ADD {
        @Override
        public double apply(double a, double b) {
            return a + b;
        }
    },
    MINUS {
        @Override
        public double apply(double a, double b) {
            return a - b;
        }
    },
    MULTIPLY {
        @Override
        public double apply(double a, double b) {
            return a * b;
        }
    },
    DIVIDE {
        @Override
        public double apply(double a, double b) {
            return a / b;
        }
    }
}
```
比如对于运算符api，有时候将其暴露给用户拓展是必要的。用户可以通过自定义新的枚举类型并实现Operation结果来做拓展
```java
enum ExtendedOperation implements Operation {
    EXP {
        @Override
        public double apply(double a, double b) {
            return Math.pow(a, b);
        }
    },
    REMAINDER {
        @Override
        public double apply(double a, double b) {
            return a % b;
        }
    }
}

public class Demo {
    // 使用特定枚举中的所有
    static <T extends Enum<T> & Operation> void test(Class<T> t, double a, double b) {
        for (Operation operation : t.getEnumConstants()) {
            System.out.println(operation.apply(a, b));
        }
    }
    
    // 更加灵活，还可以传入EnumSet
    static void test(Collection<? extends Operation> collection, double a, double b) {
        for (Operation operation : collection) {
            System.out.println(operation.apply(a, b));
        }
    }
}
```


对于使用接口来枚举拓展，一个缺点就是不可以继承另一个枚举