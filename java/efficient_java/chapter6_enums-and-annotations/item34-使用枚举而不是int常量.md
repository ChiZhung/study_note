### 第六章 枚举与注解
java支持两种特殊的引用类型：枚举与注解

---
### 第34节 使用枚举类型而不是使用int常量
* 联系方法跟字段到枚举
    * 对于starters，可能是想将数据与枚举常量相关联
* 对于一个rich枚举类型的好例子
    * 考虑太阳系的八大行星，每个行星都有一个质量与半径，通过这两个属性就可以计算其表面重力，这也就可以让你计算一个给定质量的物体在
        这个星球的表面重力。
```java
// Enum type with data and behavior
public enum Planet {
    MERCURY(3.302e+23, 2.439e6), 
    VENUS(4.869e+24, 6.052e6), 
    EARTH(5.975e+24, 6.378e6), 
    MARS(6.419e+23, 3.393e6), 
    JUPITER(1.899e+27, 7.149e7), 
    SATURN(5.685e+26, 6.027e7), 
    URANUS(8.683e+25, 2.556e7), 
    NEPTUNE(1.024e+26, 2.477e7);
    private final double mass; // In kilograms
    private final double radius; // In meters
    private final double surfaceGravity; // In m / s^2
    // Universal gravitational constant in m^3 / kg s^2
    private static final double G = 6.67300E-11;

    // Constructor
    Planet(double mass, double radius) {
        this.mass = mass;
        this.radius = radius;
        surfaceGravity = G * mass / (radius * radius);
    }

    public double mass() {
        return mass;
    }

    public double radius() {
        return radius;
    }

    public double surfaceGravity() {
        return surfaceGravity;
    }

    public double surfaceWeight(double mass) {
        return mass * surfaceGravity; // F = ma
    }
}
```

将数据与其对应的枚举常量相关联，声明实例字段并写一个构造器来传入数据并将其赋给那些字段；枚举由于其天生不能做修改的特征，所以
**所有枚举中的定义的字段都应该是final的，字段可以是public的但是最好是private然后使用accessor来访问**。

一般情况，对于枚举中的方法一般声明为private/private-package用于内部计算，除非迫于一些原因才将其暴露为public

若一个枚举是普遍有用的，那么这个枚举就应该是顶层类；若一个枚举仅仅跟一个顶级类紧密相关，则这个枚举就应该作为内部成员类；

#### 对于枚举不要为了方便使用开关控制
```java
// Enum type that switches on its own value - questionable
public enum Operation {
    PLUS, MINUS, TIMES, DIVIDE;
    // Do the arithmetic operation represented by this constant
    public double apply(double x, double y) {
        switch(this) {
            case PLUS: return x + y;
            case MINUS: return x - y;
            case TIMES: return x * y;
            case DIVIDE: return x / y;
        }
        throw new AssertionError("Unknown op: " + this);
    }
}
```
上述的写法并不好，为了偷懒将代码集中，通过switch来做分发，但是这样的代码是脆弱的，一旦新增了新的枚举类型，若没有增加对应的switch代码
就会出现编译可通过但是会发成runtime错误，所以不要这样做！
```java
// 好的做法
// Enum type with constant-specific method implementations
public enum Operation {
    PLUS {
        public double apply(double x, double y) {
            return x + y;
        }
    }, MINUS {
        public double apply(double x, double y) {
            return x - y;
        }
    }, TIMES {
        public double apply(double x, double y) {
            return x * y;
        }
    }, DIVIDE {
        public double apply(double x, double y) {
            return x / y;
        }
    };

    public abstract double apply(double x, double y);
}

// 带有class body与数据
public enum Operation {
    PLUS("+") {
        public double apply(double x, double y) {
            return x + y;
        }
    }, MINUS("-") {
        public double apply(double x, double y) {
            return x - y;
        }
    }, TIMES("*") {
        public double apply(double x, double y) {
            return x * y;
        }
    }, DIVIDE("/") {
        public double apply(double x, double y) {
            return x / y;
        }
    };
    private final String symbol;

    Operation(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() { // 返回这个枚举对应的标记
        return symbol;
    }

    public abstract double apply(double x, double y);

    /**
    考虑到枚举自带一个valueOf方法，就是提供一个字符串返回其对应的枚举，比如Operation.valueOf("PLUS"") -> Operation.PLUS
    是否可以通过一个方式就是通过比如'+'这个就可以得到Operation.PLUS这个枚举值，也就是说，通过Operation的toString反过来再得到
    其对应的枚举值, 以下提供fromString的实现。（你可能会想到直接在构造器中进行map注入不久ok了？
    *****可惜的是在枚举的构造器中不可以访问静态字段*****，因为对于枚举构造器的初始化早于静态字段，而且枚举的构造器中不能访问枚举值）
    */
    // Implementing a fromString method on an enum type
    private static final Map<String, Operation> stringToEnum =
        Stream.of(values()).collect(toMap(Object::toString, e -> e));
    // Returns Operation for string, if any
    public static Optional<Operation> fromString(String symbol) {
        return Optional.ofNullable(stringToEnum.get(symbol));
    }
}
``` 

那么使用switch开关合适的地方是哪里呢？一般用于枚举的静态方法中
```java
// Switch on an enum to simulate a missing method
public static Operation inverse(Operation op) {
    switch(op) {
        case PLUS: return Operation.MINUS;
        case MINUS: return Operation.PLUS;
        case TIMES: return Operation.DIVIDE;
        case DIVIDE: return Operation.TIMES;
        default: throw new AssertionError("Unknown op: " + op);
    }
}
```
