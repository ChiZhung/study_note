### 第39节 使用注解而不是命名模式

历史上一段时间，对于需要通过一个工具或者框架特殊对待的一些程序元素，通常通过命名模式
来标示。比如在junit4之前，junit test框架需要用户将其测试方法的命名设计为test开头。
若测试用户不小心将一个测试方法的命名前缀写成了tset则会导致这个测试被遗漏。

```java
// 指定本注解保留策略--在运行时保留
@Retention(RetentionPolicy.RUNTIME)
// 本注解修饰元素的类型--修饰方法
@Target(ElementType.METHOD)
@interface TestAnno {
    
}
```

1. 多值标示
```java
// 指定本注解保留策略--在运行时保留
@Retention(RetentionPolicy.RUNTIME)
// 本注解修饰元素的类型--修饰方法
@Target(ElementType.TYPE)
@interface TestAnno {
    // 注意返回值要是注解成员类型
    // 仅支持：1.原子类型 2.String 3.Class 4.enum 5.annotation 6.一到五各自的array
    String[] value();
}

@TestAnno({"China", "USA"}) // 使用一个注解，其中包含多个
class Country {

}

public class Demo {
    public static void main(String[] args) {
        if (Country.class.isAnnotationPresent(TestAnno.class)) {
            TestAnno testAnno = Country.class.getAnnotation(TestAnno.class);
            for (String s : testAnno.value()) {
                System.out.println(s);
            }
        }
    }
}
```
2. 通过重复的注解增加多值
```java
// 指定本注解保留策略--在运行时保留
@Retention(RetentionPolicy.RUNTIME)
// 本注解修饰元素的类型--修饰方法
@Target(ElementType.TYPE)
@Repeatable(TestAnnos.class)
@interface TestAnno {
    // 注意返回值要是注解成员类型
    // 仅支持：1.原子类型 2.String 3.Class 4.enum 5.annotation 6.一到五各自的array
    String[] value();
}

// 指定本注解保留策略--在运行时保留
@Retention(RetentionPolicy.RUNTIME)
// 本注解修饰元素的类型--修饰方法
@Target(ElementType.TYPE)
// TestAnnos称为容器注解类型，需要注意其保留策略以及注解目标
@interface TestAnnos {
    TestAnno[] value();
}


@TestAnno({"China", "USA"})
@TestAnno({"China", "USA"})
class Country {

}

public class Demo {
    public static void main(String[] args) {
        // ************
        // 注意一定要同时有TestAnno.class或TestAnnos.class的判断
        // 当在Country上面的同类型注解数量为一个的时候Country.class.isAnnotationPresent(TestAnno.class)==true
        // 当在Country上面的同类型注解数量为多个的时候Country.class.isAnnotationPresent(TestAnno.class)==false，而Country.class.isAnnotationPresent(TestAnnos.class)为true
        if (Country.class.isAnnotationPresent(TestAnno.class) || Country.class.isAnnotationPresent(TestAnnos.class)) {
            TestAnno[] testAnnos = Country.class.getAnnotationsByType(TestAnno.class);
            for (TestAnno testAnno : testAnnos) {
                for (String s : testAnno.value()) {
                    System.out.println(s);
                }
            }
        }
    }
}
```

如果您认为使用重复注解可增强源代码的可读性，请使用它们，但请记住，
在声明和处理可重复注解时有更多样板，并且可重复注释的处理容易出错。

可以说，除了工具匠之外，大多数程序员都没必要自己定义注解类型。但是所有的程序员
都应该去使用java自带的预定义注解类型。