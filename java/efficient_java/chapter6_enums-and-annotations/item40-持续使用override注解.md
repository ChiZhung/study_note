### 第40节 持续使用override注解

不使用Override的坏处举例
```java
public class Demo {
    public static void main(String[] args) {
        Set<Ch> set = new HashSet<>();
        for (int i = 0; i < 10; i++) {
            for (char c = 'a'; c <= 'z'; c++) {
                Ch ch = new Ch();
                ch.ch = c;
                set.add(ch);
            }
        }
        // 是260不是26
        System.out.println(set.size());
    }
}

class Ch {
    char ch;

    // 若equals加上了Override注解则会提示这个方法并不是重写方法
    // Object中的是equals(Object)
    public boolean equals(Ch ch) {
        return ch != null && ch.ch == this.ch;
    }

    public int hashCode() {
        return ch;
    }
}
```

