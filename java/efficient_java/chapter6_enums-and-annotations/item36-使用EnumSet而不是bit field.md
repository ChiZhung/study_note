### 第36节 使用EnumSet而不是Bit Fields
对于使用bitwise风格int枚举，比如
```java
// Bit field enumeration constants - OBSOLETE!
public class Text {
    public static final int STYLE_BOLD = 1 << 0; // 1
    public static final int STYLE_ITALIC = 1 << 1; // 2
    public static final int STYLE_UNDERLINE = 1 << 2; // 4
    public static final int STYLE_STRIKETHROUGH = 1 << 3; // 8
    
    // Parameter is bitwise OR of zero or more STYLE_ constants
    public void applyStyles(int styles) {
        // ...
    }

    main() {
        text.applyStyles(STYLE_BOLD | STYLE_ITALIC);
    }
}
```
虽然这样做可以让你完成集合操作比如并，交
使用这种方式的不足之处：首先就是在打印这些值的之后变成了整数值，可读性变差，对于int值，最多支持32种情况

对于EnumSet的内部实现，对于某个特定的枚举类，若其的类型不超过64种，则EnumSet的内部实现就是使用一个long来表示
