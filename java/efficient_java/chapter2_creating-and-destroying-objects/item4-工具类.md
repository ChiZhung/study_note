### 第四节 使用私有构造器使得不能实例化
* 不应该使用抽象类来充当工具类，因为其可以被继承，故使用一个私有构造器
* 在工具类的私有构造器中声明一个异常,这样做有点违反直觉，故最好，旁边添加一个注释
    ```java
    // Noninstantiable utility class
    public class UtilityClass {
      // Suppress default constructor for noninstantiability
      private UtilityClass() {
          throw new AssertionError();
      }
    }
  
    ```