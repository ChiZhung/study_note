### 第七节 淘汰废弃的对象引用
* 例子
    ```java
    public Object pop() {
        if (size == 0)
          throw new EmptyStackException();
        Object result = elements[--size];
        elements[size] = null; // Eliminate obsolete reference
        return result;
    }
    ```
    * 若不在pop后对数组中置null，就会导致引用链一直存在，就会导致存在其中的对象
        一直得不到GC回收
    * 这样做还有一个好处就是，若指针处理不当，那么程序获取了不当位置的refer，那么得到的就是null，
        在后续使用的时候就会抛出空指针异常，**程序设计就应该尽早的探测出错误**
* 无论何时，一个类管理自身的内存，一个程序员都应该警惕内存泄漏，一旦一个对象不再使用，就应该将其置空（临时变量除外）。
* 另一个常见的内存泄漏就是缓存，一旦将一个对象放置于缓存中，就会常常忘记它存在于缓存中，使得这个对象就算不再使用了
    仍得不到GC，常见的做法就是使用WeakHashMap作为cache，**它是仅仅当key失去引用链接的时候，这个entry（<k, v>）才会在gc时刻被回收**
    * 对于WeakHashMap的实现就是：
        1. WeakHashMap中的Entry继承于WeakReference，而仅仅使用entry的key通过super进行绑定。
        2. 每次操作map时，调用其绑定的引用队列的poll方法来判断并移除Entry
    * 更一般的，对于缓存使用超时机制，一般使用守护线程或者，在每次添加一个实例到cache的时候进行清理。可以参考
        LinkedHashMap类提供了removeEldestEntry
* 第三种常见的内存泄漏就是监听器以及其他回调,常常是因为只有对监听器以及回掉的注册而没有及时的取消注册，
    这里就可以使用类似WeakHashMap来动态回收