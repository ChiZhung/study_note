### 第九节 偏向于使用try-with-resource而不是使用try-finally
* 对于资源一般需要调用close方法进行关闭，但是由于cli的疏忽，常常导致忘记调用close方法，
    虽然存在像finalizer/cleaner这样的安全网，但是其工作起来的效果并不是太好，故推荐使用
    try-with-resource。
* 为了继续保持try-这样的结构，资源必须实现AutoClosable的接口，使用try-with-resource会压制后面产生的
    异常，但是并不会丢弃这些异常，能在stack trace种找到他们。
* Lesson：当面对需要关闭的资源时，更加偏向于使用try-with-resource而不是try-finally。