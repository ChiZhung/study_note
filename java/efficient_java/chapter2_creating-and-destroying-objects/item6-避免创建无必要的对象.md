### 第六节 避免创建没有必要的对象
* 尽量重用对象而不是创建一个新的对象，**对于不可修改的对象，一般都可以被重用**
    * 比如：不要使用String s = new String("abc"),而去使用String s = "abc"
    * 通常使用静态工厂方法来重用对象
        ```java
        // Reusing expensive object for improved performance
        public class RomanNumerals {
          private static final Pattern ROMAN = Pattern.compile( "^(?=.)M*(C[MD]|D?C{0,3})" + "(X[CL]|L?X{0,3})(I[XV]|V?I{0,3})$");
          static boolean isRomanNumeral(String s) { // 最好的方式
              return ROMAN.matcher(s).matches();
          }
          
          // 但是每次s.matches(...)都会编译生成一个不可变的Pattern实例，这是消耗资源的
          static boolean isRomanNumeral(String s) {
              return s.matches("^(?=.)M*(C[MD]|D?C{0,3})" + "(X[CL]|L?X{0,3})(I[XV]|V?I{0,3})$");
          }
        }
        ```
        * 对于以上方式，一个提升版本就是使用懒初始化ROMAN，仅仅当第一次使用该方法时初始化它，但是**这是不推荐的做法，因为这是一种并不能保证性能提升的复杂实现**
* 当一个对象是不可变时，显然其是可以安全重用的，但是存在一个更不明显的情况，其甚至更反直觉。考虑适配器模式，
    一个是配置对象底层依靠一个被适配的对象，适配的对象提供一个代替接口供外部调用。故对于一个底层被适配的对象，
    其只需要一个适配器实例即可，就像Map的keySet方法，虽然返回的set是可以修改的，但是最好还是同一对象最好。
* **注意：尽量使用原子类型，当心不必要的自动装箱**
* 对于重用对象于创建新的对象，需要鉴于二者的优劣性，比如一个场景就是需要一份拷贝就不要再重用了