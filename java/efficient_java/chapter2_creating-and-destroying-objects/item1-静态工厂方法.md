### 第二章 创建与销毁对象

讨论创建实例的时机以及如何去创建它，如何去确保及时地销毁对象以及如何去管理
在销毁之前的清理动作

#### 第一节 创建实例考虑使用静态工厂而不是构造器
* 注意：一个静态工厂方法与设计模式中的工厂方法模式不同，本节描述的静态工厂方法并不等价于
    设计模式中的。
* 通过静态工厂方法来创建对象
    * 好处：
        1. 不像构造器，其有名字，由于构造器的参数并不会描述返回的实例。使用静态工厂方法就
            对于创建不同的实例更加可读性。
        * **注意：**对于要创建不同类型的对象，使用带有不同参数的构造器来实现这是一个**坏主意**，因为
            其可读性不高。不利于用户的阅读调用。
        2. 不需要每次调用静态工厂方法都要初见一个新的对象，这就允许不可修改的类来使用预构建的实例，或者
            来缓存其构建的实例。
        3. 可以返回任何本类的自类型，这就允许来选择返回的对象类型，这使得可以返回一个非公共访问权限
            的子类，通过隐藏类实现，可以使得用户得到的就是紧凑的API，对于接口框架，比如：对于接口Type
            的静态工厂方法就放置在不能实例化的Types类中。（就像Collection于Collections），通过
            对于Collection框架API，若不是用静态工厂，则其会有45个不同的公共类，使得API大量增加。
        4. 返回的类型可以是任意子类型,这就使得可以在不同版本的API，静态工厂方法返回的实例类型不同。
            * 例如：对于EnumSet，其有两个子实现，通过静态工厂方法来获取其实例对象，当创建的枚举集合大小
                小于等于64时返回RegularEnumSet实例（底层依靠的时一个long）大于64时，返回JumboEnumSet
                底层依赖一个long数组.
        5. 静态工厂返回对象的类型可以运行时确定，就像SPF（service provider framework）见jvm/类加载/service_loader.md
    * 限制：
        1. 包含静态工厂的类没有公共或者保护构造器，故这个类不可以有子类
        2. 对于编程者很难去发现这些静态工厂
            * 常见的命名习惯
                1. Type.from(arg) 单个参数,常用于类型转换
                    * Date d = Date.from(instant)
                2. Type.of(arg1,...) 多个参数，聚集方法
                    * Set<Rank> set = EnumSet.of(JACK, QUEEN, KING)
                3. Type.valueOf 一个较之from与of更加细节的代替
                    * Integer.valueOf(1)
                4. Type.instance or Type.getInstance 返回有参数（若有的话）表述的实例
                5. Type.newInstance or Type.create 跟instance与getInstance类似，但是这个保证每次都返回新的实例
                6. Type.getXXXType 类似getInstance返回具体类型的实例
                    * FileStore fs = Files.getFileStore(path)
                7. Type.newXXXType 类似newInstance创建具体实例
                    * BufferedReader br = Files.newBufferedReader(path)
                8. Type.xxxtype 是getXXXType与newXXXType的精简代替版本
                    * List<Complaint> litany = Collections.list(legacyLitany)
* 总之，静态工厂与构造器都有其用途，通常优先考虑静态工厂，故在没有第一时间考虑使用静态工厂时，去避免使用反射来提供公共的构造器。
