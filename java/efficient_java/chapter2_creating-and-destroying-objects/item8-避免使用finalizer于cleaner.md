### 第八节 避免使用Finalizer于Cleaner
* Finalizer
    * 详细介绍见java/finalizer.md
    * finalizer是不可预测的，通常也是危险的，不必要的。队友Finalizer的有效使用少之又少，
        常规上，应该去避免使用finalizer，java9已经将finalizer标记为废弃，java9对其的代替方案就是
        cleaner
* Cleaner
    * 详细见java/Cleaner.java.md
    * 较之Finalizer，Cleaner更没那么危险使用起来，但是仍然是不可预测，慢，已经通常没必要的。
* 在java中，一般使用try-with-resource或者try-finally的方式去回收资源。对于finalizer于
        cleaner的一个缺点就是其不能够保证立即执行，在对象不可达到执行finalizer或者cleaner
        之间的时间是任意的。这意味着**不应该把任何时间敏感的内容放置到finalizer或者cleaner中**。
        **绝对不能依靠finalizer或者cleaner来更新持久化状态**，比如对于像是数据库这样的共享资源
        使用finalizer或者cleaner来释放其持久化锁会使得数据库停摆。
* 不要被System.gc与System.runFinalization所引诱，虽然其会增加执行finalizer与cleaner的可能性，但是
    并不能保证。有两个方法能够保证这个：System.runFinalizationOnExit与Runtime.runFinalizationOnExit,
    但是这两个方法存在致命的瑕疵，已经被废弃十多年了。
* finalizer的另一个问题就是finalization中发生的不可捕获的异常会被忽视，并导致那个对象的finalization终止执行。
    不捕获的异常会使得其他对象处于一个异常状态，若一个线程使用一个处于异常状态的对象可能会造成不确定的结果。
    通常，一个不能捕获的异常会终止线程并打印栈追踪。但是其出现在finalizer中，其甚至不会打印警告。而cleaner
    不会出现这个问题。
* 使用finalizer与cleaner会造成严重的性能惩罚：比如使用try-with-resource耗时12ns，使用finalizer耗时550ns。
* finalizer有一个严重的安全问题，它会使得你的类收到finalizer攻击，也就是使得你的类得不到回收

#### 代替方案
使自己的类继承AutoCloseable，使用try-with-resource
#### finalizer与cleaner的使用场景
* 两个合法使用：
    1.  安全网：用于防止忘记调用资源的close方法，但是finalizer与cleaner并不保证自身会立即执行甚至根本就不执行，
        但是相较之不处理肯定更好。使用这种方式来实现一个安全网（Safety net），一定要再三考虑这种保护方式是否值得。
        ```java
        // FileInputStream中的safety net
        protected void finalize() throws IOException {
            if ((fd != null) &&  (fd != FileDescriptor.in)) {
                /* if fd is shared, the references in FileDescriptor
                 * will ensure that finalizer is only called when
                 * safe to do so. All references using the fd have
                 * become unreachable. We can call close()
                 */
                close();
            }
        }
        ```
    2. 用于回收非java的不影响性能，非关键peer对象（指的是一个java peer obj通过原生方法代理这个非java的peer对象），
        因此gc不会去自动回收它，当性能允许，切资源不是关键资源时，使用可以考虑使用finalizer或者cleaner来回收这些资源。
        若性能要求高，是关键资源，则仍然需要立即回收（类应该有一个close放啊），不适合使用finalizer与cleaner。
#### Cleaner的使用的技巧
* 使用cleaner完成safety net，若忘记close，最终也会close较之finalizer，其不会污染API（利用的就是对于cleaner，其
    有两种方式，一种就是自己主动清理，另一种就是依靠jvm帮你清理）
    ```java
    // efficient java示例
    // An autocloseable class using a cleaner as a safety net
    // Cleaner可以是自己对jdk的Cleaner进行的一层封装
    public class Room implements AutoCloseable {
        private static final Cleaner cleaner = Cleaner.create();
    
        // Resource that requires cleaning. Must not refer to Room!
        // 注意在cleaner的传递进去的Runnable中不能再次引用到Room的对象，不然就产生了循环引用
        private static class State implements Runnable {
            int numJunkPiles; // Number of junk piles in this room
    
            State(int numJunkPiles) {
                this.numJunkPiles = numJunkPiles;
            }
    
            // Invoked by close method or cleaner
            @Override
            public void run() {
                System.out.println("Cleaning room");
                numJunkPiles = 0;
            }
        }
    
        // The state of this room, shared with our cleanable
        private final State state;
        // Our cleanable. Cleans the room when it’s eligible for gc
        private final Cleaner.Cleanable cleanable;
    
        public Room(int numJunkPiles) {
            state = new State(numJunkPiles);
            cleanable = cleaner.register(this, state);
        }
    
        @Override
        public void close() {
            cleanable.clean();
        }
    }
  
  
    // 自写代码事例
    class C {
        private C() {}
    
        public static C create() {
            return new C();
        }
    
        public Cleaner register(Object o, Runnable runnable) {
            return Cleaner.create(o, runnable);
        }
    }
    
    public class Demo implements Closeable {
        private static C c = C.create();
        private final Cleaner cleaner;
    
        Demo() {
            // 注意：不建议使用lambda表达式，因为在表达式中可以引用外部Demo的内容，容易产生循环引用
            // 故为了避免产生这种问题就仅仅使用向上面一样一个static类
            cleaner = c.register(this, () -> {
                System.out.println("close"); // 会在gc时打印
            });
        }
    
        // JVM OPTS：-Xmx10m -Xmn3m -XX:+PrintGCDetails
        public static void main(String[] args) {
            new Demo();
    
            byte[] bytes = new byte[1024 * 1024 / 2];
            bytes = new byte[1024 * 1024 / 2];
    
        }
    
        @Override
        public void close() throws IOException {
            cleaner.clean();
        }
    }
    ```