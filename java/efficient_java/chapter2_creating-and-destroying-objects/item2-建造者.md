#### 第二节 当一个构造器有多个参数时，考虑使用一个建造者来构建对象
* 静态工厂与构造器都有一个限制：他们不能很好得去伸缩大量的可选参数。
* 对于一个场景一个类有若干必选字段，若干可选字段
    1. 方式一： 远望构造器方式（telescoping constructor pattern）就是首先一个只带所有必选参数的构造器，
        第二个就是附带一个可选参数的构造器，以此类推。
        * 缺陷：这个模式对于cli代码很难书写，并且难以阅读
    2. 方式二：javabeans模式，就是声明一个无参的构造器，所有字段的数值都通过其setter来完成设置。
        * 缺陷：实例的构造过程经历多次的调用，这样就可能存在一个不一致的状态，还有就是这个模式
            使得对象是可变的。也需要开发者去提供线程安全保证。
* 建造者模式
    * 代替直接创建想要的对象，cli通过构造器或者是工厂方法（带有所有必须的参数），
        来来实例化一个建造者实例，然后通过在构造者上调用类似setter的方法来对原本的类设置
        其可选字段，最后调用Builder.build()来返回想要的对象，返回的对象就是不可变的
        ```java
        public class NutritionFacts {
           private final int servingSize;
           private final int servings;
           private final int calories;
           private final int fat;
           private final int sodium;
           private final int carbohydrate;
        
           public static class Builder {
               // Required parameters
               private final int servingSize;
               private final int servings;
               // Optional parameters - initialized to default values
               private int calories = 0;
               private int fat = 0;
               private int sodium = 0;
               private int carbohydrate = 0;
        
               public Builder(int servingSize, int servings) {
                   this.servingSize = servingSize;
                   this.servings = servings;
               }
        
               public Builder calories(int val) {
                   calories = val;
                   return this;
               }
        
               public Builder fat(int val) {
                   fat = val;
                   return this;
               }
        
               public Builder sodium(int val) {
                   sodium = val;
                   return this;
               }
        
               public Builder carbohydrate(int val) {
                   carbohydrate = val;
                   return this;
               }
        
               public NutritionFacts build() {
                   return new NutritionFacts(this);
               }
           }
        
           private NutritionFacts(Builder builder) {
               servingSize = builder.servingSize;
               servings = builder.servings;
               calories = builder.calories;
               fat = builder.fat;
               sodium = builder.sodium;
               carbohydrate = builder.carbohydrate;
           }
        }
        ```
    * 层次建造者
        ```java
        // Builder pattern for class hierarchies
        public abstract class Pizza {
            public enum Topping {HAM, MUSHROOM, ONION, PEPPER, SAUSAGE}
        
            // 是所有子类公共的必选/可选字段
            final Set<Topping> toppings;
        
            abstract static class Builder<T extends Builder<T>> {
                EnumSet<Topping> toppings = EnumSet.noneOf(Topping.class);
        
                public T addTopping(Topping topping) {
                    toppings.add(Objects.requireNonNull(topping));
                    return self();
                }
        
                abstract Pizza build();
        // Subclasses must override this method to return "this"
        
                protected abstract T self();
            }
        
            Pizza(Builder<?> builder) {
                toppings = builder.toppings.clone(); // See Item 50
            }
        }
        
        
        class NyPizza extends Pizza {
            public enum Size {SMALL, MEDIUM, LARGE}
        
            private final Size size;
        
            public static class Builder extends Pizza.Builder<Builder> {
                private final Size size;
        
                public Builder(Size size) {
                    this.size = Objects.requireNonNull(size);
                }
        
                @Override
                public NyPizza build() {
                    return new NyPizza(this);
                }
        
                @Override
                protected Builder self() {
                    return this;
                }
            }
        
            private NyPizza(Builder builder) {
                super(builder);
                size = builder.size;
            }
        }
        
        class Calzone extends Pizza {
            private final boolean sauceInside;
        
            public static class Builder extends Pizza.Builder<Builder> {
                private boolean sauceInside = false; // Default
        
                public Builder sauceInside() {
                    sauceInside = true;
                    return this;
                }
        
                @Override
                public Calzone build() {
                    return new Calzone(this);
                }
        
                @Override
                protected Builder self() {
                    return this;
                }
            }
        
            private Calzone(Builder builder) {
                super(builder);
                sauceInside = builder.sauceInside;
            }
        }
        ```
    * 建造者较之构造器还有一个好处就是：建造支持多个可变参数，其十分灵活，单一一个构造器可以用来
        构造重复的对象，在多次调用build方法之间builder的参数可以被修改。
    * 其也有一个缺点：为了创建一个对象，你不得不先创建一个builder，然而在实践中创建builder的开销
        是不能被忽略，这就可能造成一个性能问题。同时，建造者模式比远望模式（就是对于可选参数，每一个就是构造器）
        更加的冗长，故**一般在很多参数的时候才去使用builder，一般大于等于4**。但是记住**若在未来
        这个类可能会添加新的参数，若从构造器或者静态工厂切换到builder就会导致存在许多废弃的构造器及静态工厂**
#### 总结

当一个类的构造器/静态工厂有较多参数时，使用建造者模式是一个好选择，特别是当许多参数是可选的情况，这样
    客户端代码就较之远望构造器易于理解，并且，建造者模式比java bean模式更加安全。  