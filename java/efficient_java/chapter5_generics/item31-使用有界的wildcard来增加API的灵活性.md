### 第31节 使用bounded wildcard来增加API的灵活性

由于范型中List<integer>不是List<Number>的子类型，所以比如如下

```java
class Stack<E> {
    // ...
    void pushAll(Iterable<E> src) {
        // ...
    }
    // ...
    void popAll(Collection<E> dst) {
        // ...
    }
}
```
若E为Number类型，逻辑上，pushAll(Iterable<Integer> src)应该是可行的，但是由于
Iterable<Integer>不是Iterable<Number>的子类型，所以这样是不支持的，所以为了更加灵活，也是
为了回避像List<integer>不是List<Number>的子类型的这种类型，我们可以使得方法参数变为`Iterable<? extends E>`,
也就是使用bounded wildcard来解决这个问题

同样的问题，加入将stack中的元素pop到dst中：
```java
Stack<Number> numberStack = new Stack<Number>();
Collection<Object> objects = ... ;
numberStack.popAll(objects);
```
使用`Collection<E> dst`,会使得以上代码编译不通过，但是逻辑上，应该是行得通的。解决办法就是变为`Collection<? super E>`

为了最大程度的灵活性，**对于输入参数（用于代表producer/consumer）使用wildcard类型**。若一个输入参数是既是producer又是consumer，
对于这种情况，就应该使用一个具体类型E而不使用wildcard

#### PECS原则
在使用范型的时候，比如类的元素类型范型为E，对于一个方法，若方法的输入参数为在方法中作为producer的角色，则这个参数的范型使用? extends E (producer-extends),
输入参数在方法中作为consumer的角色，则这个参数范型使用? super E (consumer-super), 这就是所谓的PECS

注意：这里范型PECS指的是Type<E>中的E，不是单独的E
```java
// 大概代码
class Stack<E> {
    private Object[] array;

    Stack(Collection<? extends E> src) {
        array = new Object[src.size()];
        // ...
    }

    void push(E e) { // 单单的E类型，不使用PECS原则
        // ...
    }

    E pop() {
        // ...
    }

    // src作为producer角色，且符合Type<E>类型，使用PECS原则
    void pushAll(Iterable<? extends E> src) {
        // 将src中的元素放置到stack中
    }

    // dst作为consumer角色，且符合Type<E>类型，使用PECS原则
    void popAll(Collection<? super E> dst) {
        // 将stack中的元素放置都dts中
    }
}
```

**对于返回类型不要使用wildcard，对于返回类型返回的越具体越子类越好。** 也就是对于用户来说，返回不使用wildcard，也就是不提供额外的灵活性，
使得cli代码变得清晰简洁。**若一个类的用户考虑wildcard类型的使用，这就可能class的API存在一些问题**
```java
//
Set<? extends E> func() {} // 返回类型不使用wildcard
<T extends Comparable<? super T>> T max(List<? extends T> list) {} // 返回的是T，这个是可以的 
```

**在一个方法的声明中，若一个类型参数仅仅出现一次，使用wildcard而不是使用E，E是一个未绑定的类型参数，使用?代替，E是一个绑定了的（E extends T）使用? extends T代替**
```java
static <E> void swap(List<E> list, int i, int j); // 方法声明中只出现一次类型参数E
static void swap(List<?> list, int i, int j); 
```
但是由于对于List<?> list，其只能接受null val，故需要使用
```java
public static void swap(List<?> list, int i, int j) {
    swapHelper(list, i, j); // 使用helper方法来捕获具体的类型
}

// Private helper method for wildcard capture
private static <E> void swapHelper(List<E> list, int i, int j) {
    list.set(i, list.set(j, list.get(i)));
}
```
