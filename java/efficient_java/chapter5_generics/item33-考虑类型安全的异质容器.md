### 第33节 考虑使用类型安全的异质容器

对于常见的比如集合的范型，都限制了固定的参数（比如Set<E>, Map<K, V>）,但是有时为了更加灵活，比如数据库的一行有任意多列。
比如，我们可以利用Class类来标示不同的类型

```java
// 即保证了类型安全，又使得map可以存放不同类型的
public class Demo {
    public static void main(String[] args) {
        DiffMap diffMap = new DiffMap();
        diffMap.put(Integer.class, 1);
        diffMap.put(String.class, "hello");
        diffMap.put(Double.class, 3.14);

        System.out.println(diffMap.get(String.class));
        System.out.println(diffMap.get(Double.class));
    }
}

class DiffMap {
    private final Map<Class<?>, Object> map = new HashMap<>();

    public <T> void put(Class<T> clazz, T val) {
        Objects.requireNonNull(clazz);
        // 比如传进来的是一个Class raw类型，就导致
        // Class integerClass = Integer.class;
        // diffMap.put(integerClass, "123"); 
        // 这样使用put不会出问题，但是一旦get(Integer.class)就出问题
        // 使用clazz.cast(val)来保证在put的时候就报错
        map.put(clazz, clazz.cast(val)); // 确保val可以抛出为clazz的类型
    }

    public <T> T get(Class<T> clazz) {
        Objects.requireNonNull(clazz);
        return clazz.cast(map.get(clazz));
    }
}

// ****************

public class Demo {
    public static void main(String[] args) {
        // 也就是每次add的时候使用type.isInstance(o)来判断时候类型一致
        List<Integer> objects = Collections.checkedList(new LinkedList<>(), Integer.class);
        List list = objects;
        list.add("123"); // 抛出异常
    }
}
```
以上将val标示为Object，故kv的关系仅仅通过k的class来标示，且用户可以使用raw类型注入，
其次还有一个问题就是存储的应该是reifiable的（比如String, String[]但是List<String>不可以），

* 使用Class::asSubclass来做一个安全cast
```java
Class<?> clazz = Class.forName("xxx.A");
A a = clazz.asSubclass(A.class);
```
