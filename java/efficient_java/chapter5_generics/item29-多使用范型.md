### 第二十九节 偏向于写范型

#### 消除范型数组创建的错误
```java
// 实例一
class Stack<E> {
    private E[] array;

    @SuppressWarnings("unchecked")
    public Stack(int size) {
        // 由于不允许创建范型数组E[]
        // 采用不安全的方式
        array = (E[]) new Object[size]; 
    }

    void push(E e) {}
    E pop() {}
}

// 实例二
class Stack<E> {
    private Object[] array;

    @SuppressWarnings("unchecked")
    public Stack(int size) {
        // 由于不允许创建范型数组E[]
        // 采用不安全的方式
        array = new Object[size]; 
    }

    void push(E e) {} // 采用强转
    E pop() {} // 采用强转
}
```

以上两种方式都可以淘汰范型数组创建，且都有自己的特点，第一种更加可读，更简洁（仅仅需要一次强转），
而对于第二种其将强转分散到各个调用中，**所以一般优先使用第一种方式**。然而，对于第一种其会造成堆污染（item32），
因为array的编译器类型（E[]）与运行时类型（Object[]）是不同的，但是这种污染在这种情形下是无害的。