### 第32节 理性结合范型与可变参数

可变参数与范型在java5加入，但是这两个特性却不能良好的结合，由于可变参数是用一个数组来接受的，但是当可变参数类型为范型（e.g. List<E>）或者是参数类型(e.g. E)
的时候，会收到一个编译器警告（也就是范型跟数组之间的问题，数组是运行时，而范型是编译器擦除）。正如第28节提到的，non-reifiable（非具体的）的类型是指一个类型的实例其
运行时的信息量少于其编译时，这也就是对于范型(e.g. List<E>)与参数化类型(e.g. E)，几乎都是不具体的，无论是一个方法声明一个(E.g. `func(E... es)` or `func(List<E>... lists)`)
抑或是调用了这样的方法都是会有编译器警告的。

#### 堆污染
堆污染指的是：标示的类型与堆上的实际类型不符（比如`E[] es = (E[]) new Object[N]`, 标示的类型为E[]，而实际类型为Object[]）

```java
// 存在问题
// Mixing generics and varargs can violate type safety!
static void dangerous(List<String>... stringLists) { // 范型问题
    List<Integer> intList = List.of(42);
    Object[] objects = stringLists;
    objects[0] = intList; // Heap pollution，由于范型擦除放置的的List
    String s = stringLists[0].get(0); // ClassCastException
}
```
使用范型数组不安全

使用@SafeVarargs来消除堆与可变参数的警告，自己要确保代码的类型安全，若一个方法一定是类型安全的时候才去使用@SafeVarargs注释，
那如何去判断一个带可变范型参数的方法是安全的呢，并可以用@SafeVarargs注释来标示它。**当一个方法对可变范型参数对应的数组仅仅只是遍历，不做修改，且不让
这个数组从该方法中逃逸出去的话**，这样这个方法是安全的。
```java
// 存在问题
// 利用可变参数返回其对应的数组是不安全的，存在堆污染
static <T> T[] toArray(T... ts) {
    return ts;
}
```

#### 对于范型可变参数方法的使用
* 保证每个带有范型/参数化类型的可变参数方法保证：
    1. 在方法体内部对可变参数数组不做修改
        * it doesn’t store anything in the varargs parameter array
    2. 不讲可变参数数组暴露出去
        * it doesn’t make the array (or a clone) visible to untrusted code. If
          either of these prohibitions is violated, fix it
* 再对每个这样的方法前面加上@SafeVarargs注释来标示
```java
// Safe method with a generic varargs parameter
// 正确的示例
@SafeVarargs
static <T> List<T> flatten(List<? extends T>... lists) {
    List<T> result = new ArrayList<>();
    for (List<? extends T> list : lists)
        result.addAll(list);
    return result;
}
```

* 由于标示为safe varargs的方法在重写后就不一定可以保证安全了，所以在java8中@SafeVarargs的使用仅仅在静态/final实例方法是合法的，在java9
    增加了对private方法使用@SafeVarargs的支持
    
* 另一个替代可变参数的方法就是使用item28所提到的使用List
    ```java
    // List as a typesafe alternative to a generic varargs parameter
    static <T> List<T> flatten(List<List<? extends T>> lists) {
        List<T> result = new ArrayList<>();
        for (List<? extends T> list : lists)
          result.addAll(list);
        return result;
    }
    ```
    * 好处就是：代码不需要使用注解来压制警告
    * 不足就是代码会有一些冗余
    
