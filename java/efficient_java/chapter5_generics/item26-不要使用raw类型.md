### 第26节 不要使用raw类型

java的范型在java5引入，考虑到兼容性，采用的是伪范型（即范型编译器擦除的实现），但在现实使用中，
一定要去使用范型，而不要使用类似（List list, Set set等）的raw类型，使用范型可以使得一些错误
在编译器就可以定位，使用raw类型是危险的，因为出现问题也只会在运行处抛出异常

就算是元素是Object，也不要去使用raw类型，为了利用到范型的益处，要使用比如：Collection<Object>

就算不确定范型的类型，也不要使用raw类型，要使用比如：Collection<?> (标示任意类型，这样保证类型安全与灵活性)，

```java
class Demo {
    public static void main(String[] args) {
        List rawList = new ArrayList();
        List<?> wildcardList = new ArrayList<>();
    
        // 对于raw一下两行的写法是支持的
        rawList.add(1);
        rawList.add("123");

        // 对于wildcard type以下两行是不支持，编译器报错
        // 报错类似如下：
        // where CAP#1 is a fresh type-variable:
        // CAP#1 extends Object from capture of ?
        wildcardList.add(1);
        wildcardList.add("123");
    }   
}

// 总结：使用wildcard type，对于比如List<?> wildcardList, 编译器会保证其每次添加的元素类型必须一致，而raw则不保证
// 但是两种情况都支持null添加
```

* 一定要是用raw类型的情况
    * 但是对于raw类型的使用，对于class的字面量的书写是必须的：
        比如List.class, String[].class, and int.class。而List<String>.class and List<?>.class是不支持的。
    * 对于instanceof运算符，由于范型在运行时的擦除，所以也必须运算符的右边也必须使用raw类型
        ```java
        // 一旦使用instanceof若不确定类型，但是一定到使用wildcard转换，这不会抛出未check的警告
        if (o instancof Set) {
            Set<?> set = (Set<?>) o;  
        }
        ```



