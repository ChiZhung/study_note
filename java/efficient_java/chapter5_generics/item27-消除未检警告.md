### 第二十七节 消除未检警告

* 当使用范型编程的时候，可以看到许多编译器警告：
    * unchecked cast warnings
    * unchecked method invocation warnings
    * unchecked parameterized vararg type warnings
    * unchecked conversion warnings
#### 消除非检警告

尽你所能消除所有警告，当你消除了所有警告，你就可以确保你的代码是类型安全的，这就意味着你的代码不会抛出类抛异常

若不能消除一个警告，可以通过使用`@SuppressWarnings("unchecked")`的注解来标示类型安全的，若你没有证明自己的代码是类型安全的会，则使用这个
注解仅仅只是给自己一个错误的安全感。

SuppressWarnings注解可以用于任何**声明**（定义，不可以在一个方法调用，返回之上），从单独的一个局部变量到一个类，**尽可能在更小的作用域中使用SuppressWarnings注解**，比如：在一个局部变量
，一个较小的方法上，一个构造器上使用这个注解，**绝不在一个类上使用这个注解**。

当你发现自己使用的SuppressWarnings注解在一个大于一行代码的方法或者构造器上时，或许你可以将将这个注解挪到局部变量上，虽然这可能导致你需要
重新声明一个新的局部变量，再将这个注解冠于其上（这样做是值得的）。对于每一次SuppressWarnings注解的使用，用注释说明为什么这样使用的是类型安全的。