### 第22节 仅仅在定义类型上，使用接口

当一个类实现了一个接口，那么这个接口其实就充当着一个类型，这个实例是可以指向的。实现了接口的类应该能表现出cli使用这个类能做些什么事情。

当一些类可能用到自己私有的一些常量的时候，对于这些常量，**注意不要使用接口来充当常量接口**，常量接口模式是对接口使用不好的表现。我们
可能为了方便，将这些常量定义在接口中，然后对于那些需要使用到这些接口的实现类去实现它，但是这不是一个好ideal，因为这些常量按道理
应该是private static final的但是因为处在接口里，其就变成了外露的API。

#### 导出常量的方法
* 对于一个类/接口，其跟一个常量很密切，就应该建跟这个常量定义在这个类/接口中。
* 最好还是能够抽成枚举那是最好的
#### 导入外部常量
对于一个工具类，大量使用其中的静态常量字段，考虑直接使用静态导入。

#### 总结
接口仅仅应该用来定义类型，而不应该用来导出常量