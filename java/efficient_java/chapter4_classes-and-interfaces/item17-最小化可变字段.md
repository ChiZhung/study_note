### 第十七节 最小化可变字段

一个不可修改的类就是其所有字段都不可修改的，在这个对象的生命周期中，其
字段都是固定的

不可变类的好处：
    1. 不可变类较之于可变类来说，显著的一个特征就是更好设计，更好实现，更好使用。
    2. 更不易出错，更加安全

#### 为了使得一个类变得不可变(不可修改)，应该遵循以下五点
1. 不要提供修改对象状态的修改方法（修改器）
2. 确保这个类不能被拓展（继承）
    * 指的这个类为final class
3. 使得所有字段都是final的
4. 使得所有字段都是private的
5. 对于任何可修改的组件不可修改（虽然在类内部可以修改，但是对于类外部其是不可修改的）
    ```java
    public final class ImmutableClass {
       private final int[] privateArray = {1, 2, 3}; // 对于class内部其是可以修改的
       public List<Integer> array = Collections.unmodifiableList(Collections.singletonList(array));   
    }
    ```
    * 若一个类的某些字段已经引用了可变的对象，请确保cli不能够拥有这些可以修改的对象引用，若要返回要么返回一个封装为不可变的
        见上，要么就复制一份给cli。
* 比如final class Complex，两个复数相加并不是改变其中一个复数的状态，而是返回一个新的Complex
    * 注意不改变状态的命名规范：使用介词标示不改变状态的操作，使用动词就是改变状态的操作：比如使用complex1.plus(complex2)->complex3
        而num1.add(num2) -> num1，以及minus对于subtract，times对与multiple等
    * 这种不可变类的操作可能看起来有点不自然，但是这能够使得类变得不可变这就带来了许多好处，最常见的就是不可变对象是简单的，
        一个不可变对象又一个精确的状态，一个在对象创建时就确定的状态。对于可变对象，其有着任意复合的状态空间。若一个文档没有
        提供一个对于修改器方法操作的状态来说一个精确的状态修改，否则这将会变得可靠的修改对象的状态变得困难甚至是不可能的。
        
* 对于不可变对象，其本质上是线程安全的，他们不需要sync，当并发访问时，不会出现同步问题。
* 不可变对象可以自由的共享,故对于不可修改的类，不应该提供clone方法与一个copy构造器（见item13）
* 对于不可变对象，其不仅可以共享其对象，还可以共享其内部细节
    * 比如：对于java的BigInteger，一个值在内部由两部分标示，一个final int signum用来标示这个数字的正负，使用另外一个final int[] mag
        这个数组用来表示big integer的数值部分。比如对于BigInteger::negate方法，它不用创建新的mag数组，只需要返回new BigInteger(-signum, mag)
        即重用共享了不可修改类中的部分字段 
* 不可变对象可以做为其他对象的重要组成字段
* 不可变对象的缺点就是只要一个字段不同就需要创建一个新的不可变对象
* 对于创建不可变对象的最佳方法（使用静态工厂）
    ```java
    public class Complex {
      private final double real, image;
  
      Complex(double real, double image) {
          this.real = real;
          this.image = image;
      }
  
      public static Complex valueOf(double real, double image) {
          return new Complex(real, image); // 可以随意替换为Complex任意实现类，甚至增加一个缓存机制加以优化类似Integer的cache
      }
    }
    ```
    * 这样设计的好处：
        * 通过提供一个package-private的构造器，以及一个静态工厂方法valueOf，以及非final定义的class Complex，
            当这个类作为API暴露后，虽然这个类在内部不是final的，但是对于外部来说由于其不可继承，所以其是可以看作是一个final类，
            提供使用静态工厂，使得valueOf就变得将其灵活，使得其可以返回任何Complex的实现类，就使得优化这个类成为可能。
* **注意：对于不变类，一般一定要保证其final class，为了让其的方法不被重写（或者保证这个class不能有public or protected constructor）**，
    咱们以BigInteger举例，由于其存在public构造器，所以可以对其继承并重写方法，对于不可信环境传过来的BigInteger实例，其可能是
    BigInteger的一个子类，其可能就变成可修改类了，所以对于这种情形，一定要做一个defensively copy操作以确保这个实例是不可修改的
    ```java
    public static BigInteger safeInstance(BigInteger val) {
      return val.getClass() == BigInteger.class ?
          val : new BigInteger(val.toByteArray());
    }
    ```
* 对于不可变对象要求：没有方法可以修改这个对象没所有的字段必须为final，其实这个要求有时有点过强了，我们可以根据实际情况适当放松这个
    要求以提升性能。**只要确保，没有方法可以导致这个对象外部可见的状态修改即可**。然而，有时候一个不可修改类中存在一些非final的字段，
    比如用于cache的字段，用来缓存那些计算代价高的实例，再比如一个不可变对象的hashCode方法每次的计算代价较高，可以用一个可变字段来维护这个
    实例的hash值（就像String类的hashCode方法，由于是不可变对象，所以没有必要去同步锁它的hashCode方法）

#### 不可变对象的序列化注意点

当不可变类实现了Serializable接口，并且这个不可变类有若干个字段引用了可修改的字段，即使默认的序列化形式是可接受的，
也需要显式提供readObject或者readResolve方法，或者使用ObjectOutputStream.writeUnshared 
and ObjectInputStream.readUnshared方法（in item88）。

#### 总结

总的来说， 不要急着为每个getter都写一个setter方法，若没有好的原因让一个类要去修改，则尽量使其成为不可修改的类。
对于不可修改类，其为一个不足就是可能存在特定情形下的性能问题（由于一个字段不同就需要创建一个新的对象）。
当仅仅确信为了满足一个性能，你应该提供一个公共的可修改的伙伴类（见item67）。

* 若一个类不可以使得其变成不可修改，则尽可能限制其的修改性
    * 尽可能的去减少一个可修改类的状态字段，从而减少出错的可能性
    * 尽量使得每一个字段都是final的
* 尽量使得每一个字段都是private final的，除非有合理原因不去这样做
* 在初始化一个实例的时候，构造器应该完全构建其所有的不可变字段，**不要单独的提供一个初始化方法来，初始化那些作为的不可修改字段，
    除非你被迫去这样做**。相似的，不要提供一个类似重新初始化的方法来重设。
    * 比如类CountDownLatch，虽然其是可修改的，但是其保持了一个最小的可修改状态，一旦这个实例被使用，它的count字段到达0，
        你就不再可以重用它。
* 最后一个需要注意的就是，Complex不应该用于企业级强度的复数处理
