### 第21节 向接口中添加新的默认方法请三思而行

在java8之前是不可能单独地在接口中添加新的方法，因为这个实现这个接口的方法就会编译不通过。
但java8开始，开始支持了接口的默认方法，这就可以让为新接口添加新方法成为可能。虽然可以默认注入这些新的默认方法到已经存在的
接口中，但是对于先前就实现这些接口的类来说，这些默认接口是被忽视的，对于他们来说也是没有被承诺的。

#### 往接口中注入新的默认方法，可能会break子类的使用
在java8中，添加进核心集合接口中的默认方法都是为了配合lambda表达式的，这些默认的方法是高质量的，通用的，在绝大多数情形下都是不错的。
但是往接口中注入新的默认方法，将会break子类对这个默认方法的使用。由于子类的实现是并没有保证对这个方法的承诺，我们以一个包装类举例，将如
又一个包装类SyncList，内部包装了一个字段List来保证完成同步的操作。加入这个类是在java7设计的，那么其forwarding方法就没有考虑java8加入
的默认方法的问题，我们以java8加入的新默认方法removeIf(Predicate)举例，它将使用谓词遍历列表中所有元素，为真就移除那个元素。但是问题来了，
加入在java8的环境调用SyncList的removeIf方法，先并发环境将会出现问题。就是由于接口的默认方法的注入，导致接口实现类在某些场合出现异常。

**这也就导致，可能注入了新的默认方法的接口的实现类能够通过编译，但是可能在运行期出现问题。**

#### 除非必要，应该避免往接口中注入新的默认方法

由于上述的由于往接口中注入默认方法可能带来的问题的原因，所以**在向接口中注入新的默认方法的时候，一定要再三考虑**

