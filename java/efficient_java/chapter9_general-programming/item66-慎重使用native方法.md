### 第66节 慎重使用native方法
JNI允许java程序调用local方法，使用JNI的三种场景：

1. 它们提供对特定于平台的设施(如注册中心)的访问。
    * 使用本机方法来访问特定于平台的工具是合法的，但是很少有必要：随着Java平台的成熟，它提供了对许多以前只能在主机平台上找到的特性的访问。
      例如，在Java9中添加的进程API提供了对OS进程的访问。当Java中没有等价的库可用时，使用本机方法来使用本机库也是合法的。
2. 它们提供对现有本机代码库的访问，包括提供对遗留数据访问的遗留库。
3. 使用本机方法以本机语言编写应用程序的性能关键部分，以提高性能。
    * 为了提高性能，很少建议使用本机方法。在早期的版本中(Java 3之 前)，这通常是必要的，但是从那时起，JVM 变得更快了。对于大多数任务，现在可以在 Java 中获得类似的性能。例如，当 java.math 在版本1.1中，BigInteger 依赖于用 C 编写的当时快速的多精度算术库。在Java 3中，BigInteger 在 Java 中重新实现，并经过仔细调整，使其运行速度快于原始的本机实现。
      这个故事的一个可悲的结尾是，除了 Java 8 中大数的更快乘法之外，BigInteger 此后几乎没有变化。在此期间，在本机库方面的工作继续快速进行，尤其是 GNU 多精度算术库(GMP)。需要真正高性能多精度算法的 Java 程序员现在可以通过本机方法使用 GMP [Blum14]。

programs using native methods are less portable. They are also harder to debug. If you aren’t careful, native methods
原生方法的使用也有一些缺点，因为原生语言书写的代码是不安全的，应用使用原生方法可能会遭受内存崩溃的错误。
由于原生代码比java更加依赖平台，使用原生方法的程序移植性差，且难以debug。当你使用原生代码，垃圾收集器无法自动化跟踪原生内存的使用，此外进出JNI也有一定开销，这可能使得性能更差。
最后，本机方法需要“胶水代码”，读起来很困难，写起来也很繁琐。


#### 总结
总之，在使用本机方法之前要三思。您很少需要使用它们来提高性能。
**如果必须使用本机方法来访问底层资源或本机库，则应尽可能少地使用本机代码，并对其进行彻底测试**。
本机代码中的一个错误就可以破坏整个应用程序。