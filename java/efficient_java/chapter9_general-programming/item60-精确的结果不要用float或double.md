### 第60节 如果需要精确的结果，不要用 float 或 double

float 和 double 类型 特别 不 适合 进行货币计算，因为不可能将0.1(或任何其他 10 的负幂)精确地表示为 float 或 double。