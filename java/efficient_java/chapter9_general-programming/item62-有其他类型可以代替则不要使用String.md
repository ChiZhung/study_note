### 第62节 有其他类型可以代替则不要使用String类型

**字符串是用来表示文本的**，它们在这方面做得很好。因为字符串很常见，而且语言也很好地支持它，所以很自然地倾向于将字符串用于其他目的，而不是它们设计的目的。这一节讨论了一些不应该用字符串做的事情。

字符串是其他值类型的糟糕替代品。当一段数据从文件、网络或键盘输入进入程序时，它通常是字符串形式的。
有一种自然的倾向是这样的，但是这种倾向只有在数据实际上是文本的情况下才合理。
如果是数值型的，则应该将其转换为适当的数值类型，如 int、float 或 BigInteger。
如果是“是”或“不是”问题的答案，则应将其转换为适当的 Enum 类型或布尔值。
**更一般地说，如果有适当的值类型，无论是原语还是对象引用，都应该使用它;如果没有，你应该写一个**。虽然这个建议看起来很明显，但经常被违反。

#### 什么场景不适合使用字符串来替代
1. 字符串是枚举类型的糟糕替代品。正如第34节中所讨论的，枚举类型常量比字符串更适合枚举类型常量。
2. 字符串是聚合类型的糟糕替代品。如果一个实体有多个组件，将其表示为单个字符串通常不是一个好主意。
    ```java
    // Inappropriate use of string as aggregate type
    String compoundKey = className + "#" + i.next();
    ```
    * 这种方法有很多缺点。如果用于分隔字段的字符出现在其中一个字段中，可能会导致混乱。要访问各个字段，您必须解析字符串，这是缓慢、冗长且容易出错的。
      您不能提供 equals、toString 或 compareTo 方法，但必须接受 String 提供的行为。更好的方法是简单地编写一个类来表示聚合，通常是一个私有的静态成员类

3. 字符串不是性能的良好替代品
   ```java
   // Broken - inappropriate use of string as capability!
   public class ThreadLocal {
   private ThreadLocal() { } // Noninstantiable
       // Sets the current thread's value for the named variable.
       public static void set(String key, Object value);
       // Returns the current thread's value for the named variable.
       public static Object get(String key);
   }
   ```
   * 使用字符串来授予对于某些功能的访问，比如对于ThreadLocal的设计，在其在java2推出之前，有几个人提出了相同的设计，如上面的代码。
     这种方式的问题在于，字符串键表示线程本地变量的共享全局名称空间。为了使这种方法有效，客户机提供的字符串键必须是惟一的:如果两个客户机各自决定对它们的线程局部变量使用相同的名称，它们无意中共享一个变量，这通常会导致两个客户机都失败。而且，安全性很差。恶意客户机可以故意使用与另一个客户机相同的字符串密钥来非法访问另一个客户机的数据。
   ```java
   public class ThreadLocal {
       private ThreadLocal() { } // Noninstantiable
       public static class Key { // (Capability) Key() { }}
       // Generates a unique, unforgeable key
       public static Key getKey() { return new Key(); }
       public static void set(Key key, Object value);
       public static Object get(Key key);
   }
   ```
   * 上述API可以通过用一个不可伪造的键(有时称为功能)替换字符串来修复
   ```java
   public final class ThreadLocal<T> { 
       public ThreadLocal();
       public void set(T value);
       public T get();
   }
   ```
   * 将ThreadLocal本身就作为一个key，对于val的处理变成了键上的实例方法，这个也是java中的ThreadLocal的实际实现

#### Summery
总之，当存在更好的数据类型或可以编写更好的数据类型时，避免将对象表示为字符串的自然倾向。如果使用不当，与其他类型相比，字符串更麻烦、灵活性更差、速度更慢、更容易出错。字符串经常被误用的类型包括基本类型、枚举和聚合类型。

