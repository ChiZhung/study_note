### 第63节 注意字符串的拼接性能问题

字符串连接操作符(+)是将几个字符串组合成一个字符串的方便方法。对于生成单行输出或者构造一个小的、固定大小的对象的字符串表示形式，它是可以的，但是它不能伸缩。
重复使用字符串(+=)连接操作符来连接n个字符串时间复杂度是O(n^2)。

```java
public class Test {

    public static void main(String[] args) {
        List<String> strings = Arrays.asList("1", "2", "3");

        String ret = "";
        for (String s : strings) {
            ret += s; // 在反编译为字节码可以发现，这个时候每次都会new一个StringBuilder，并append
        }
    }
}
```

所以一定要记住对于string的+=号一定不要多用，尤其是在loop中

```java
// 正确的做法
public String statement() {
  StringBuilder b = new StringBuilder(numItems() * LINE_WIDTH);
  for (int i = 0; i < numItems(); i++)
    b.append(lineForItem(i)); 
  return b.toString();
}
```
