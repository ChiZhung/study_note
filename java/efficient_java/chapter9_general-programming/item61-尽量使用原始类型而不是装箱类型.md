### 第61节 尽量使用原始类型而不是装箱类型

原语和装箱类型之间有三个主要区别

1. 原语只有它们的值，而装箱的原语有不同于它们的值的标识
    ```java
    // Broken comparator - can you spot the flaw?
    Comparator<Integer> naturalOrder = (i, j) -> (i < j) ? -1 : (i == j ? 0 : 1);
   
    // 当传入以下比较的时候，将会不符合预期
    naturalOrder.compare(new Integer(42)， new Integer(42))
    // 参考Integer中的compareTo的实现
    public int compareTo(Integer anotherInteger) {
        return compare(this.value, anotherInteger.value); // just compare primitive value
    }
    ```
    * 也就是说对于存在boxed与unboxed的情况的比较，我们应该去使用`Comparator.comparingInt()`或者`Integer.compare()`等方法来定义比较器，
      而不应该自己显示地使用>, < or =
2. 原语类型只有全功能值，而每个装箱的原语类型除了对应原语类型的所有功能值外，还有一个非功能值，即null
    ```java
    public class Unbelievable { 
        static Integer i; // default is null, not 0
        public static void main(String[] args) {
            if (i == 42) {
                System.out.println("Unbelievable");
            }
        }
    }
    ```
    * 涉及boxed值的运算，都可能存在这样的潜在问题就是没有初始化的问题而触发NPE，修复这个问题很简单就是使用int而不是Integer来定义
3. 原语比盒装原语更节省时间和空间
    ```java
    // Hideously slow program! Can you spot the object creation?
    public static void main(String[] args) {
        Long sum = 0L;
        for (long i = 0; i < Integer.MAX_VALUE; i++) {
            sum += i;
        }
        System.out.println(sum);
    }
    ```
   * 频繁涉及拆装箱，导致性能下降


#### 什么情况下使用box呢？
* 第一种是作为集合中的元素、键和值。您不能将原语放入集合中，因此必须使用盒装的原语。
* 在参数化类型和方法(item 5)中，必须使用装箱的原语作为类型参数，因为该语言不允许使用原语。例如，不能将变量声明为 ThreadLocal<int> 类型，因此必须使用 ThreadLocal<Integer>
* 在进行反射方法调用时，必须使用装箱原语

#### Summery
总之，只要有选择，就应该优先使用原语，而不是装箱类型。原始类型更简单、更快。如果必须使用装箱类型，请小心！自动装箱减少了使用装箱类型的冗长，但没有减少危险。
当您的程序将两个装箱原语与 == 操作符进行比较时，它将执行标识比较，这几乎肯定不是您想要的。当您的程序执行包含已装箱和未装箱原语的混合类型计算时，它将进行拆箱，而当您的程序进行拆箱时，它将抛出NullPointerException。
最后，当您的程序将原语值框起来时，可能会导致代价高昂且不必要的对象创建。


