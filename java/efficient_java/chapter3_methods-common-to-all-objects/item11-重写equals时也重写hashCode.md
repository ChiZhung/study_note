### 第十一节 equals与hashCode是孪生兄弟
* 当你重写equals时，你必须重写hashCode，若不这要做就违反了hashCode的一般规则，
    这样就会影响集合的功能（当改写的类作为key或者元素时）
* 若在equals中作为比较的值没有修改的话，一个obj的hashCode数值应该在一个应用的执行期间保持不变，不同执行期可以不同。
* 当x.equals(y) == true则x.hashCode() == y.hashCode()
* 当x.equals(y) == false则二者的哈希值也可能相同
* hashCode尽量使得每个不同的对象对应的hash值都不同，尽量使得hash值均匀分布在整个int值域上

#### hash策略（使得hash值均匀分布在int值域上）
* 虽然绝对均匀是困难的，但是可以使用近似方案：
    * 可以简单的使用Objects.hash(Object...)来计算本类的hash值，将本类中所有在equals中用到作为计算的字段
        都传入这个方法。
        * 由于存在自动拆装箱，故对于性能要求不高的场景可以使用这个方法
    ```java
    public class Demo {
    
        public static void main(String[] args) throws IOException {
            int[] ints = IntStream.rangeClosed(1, 10).toArray();
            int rst = 1;
    
            long startTime = System.nanoTime();
            rst = Arrays.hashCode(ints);
            long endTime = System.nanoTime();
            System.out.println(endTime - startTime);
    
            System.out.println(rst);
    
            rst = 1;
            startTime = System.nanoTime();
            for (int i : ints) {
                rst = (rst << 5) - rst + i;
            }
            endTime = System.nanoTime();
            System.out.println(endTime - startTime);
    
            System.out.println(rst);
            
            /* 
            24236
            -975991962
            668
            -975991962
            速度是2个数量级
            * */
        }
    }
    ```
* 若class是不可修改的，且其的hash计算较为耗时，可以考虑将其hash值保存起来，若对于这个类的大多数都不会使用这个hash值
    可以考虑使用懒计算
    ```java
    private int hashCode; // Automatically initialized to 0
    
    @Override 
    public int hashCode() {
        int result = hashCode;
        if (result == 0) {
            result = Short.hashCode(areaCode);
            result = 31 * result + Short.hashCode(prefix);
            result = 31 * result + Short.hashCode(lineNum);
            hashCode = result;
        }
        return result;
    }
    ```
* 不要为了提升性能而从hashcode中移除重要字段的计算
* 不要使用一个细节的指定来作为hashcode的返回值，比如想integer，string的hashcode实现，也就是尽量不要
    外部去利用hash的实现细节及特征。比如不去利用integer的hashcode的等于自身的特征等，
    也就是hashcode最好就是用于hash。因为这样cli可能就会去利用它，使得未来某个版本倘若发现这个hash版本存在问题
    或者想更新一个更好的hash版本，就不得不为了兼容问题而放弃这个做法。