## 第三章 所有对象的公共方法

对象的方法：equals，hashCode，toString，clone，finalize都有一个一般约定，
因为它们设计的目的就是用来重写，本章将介绍如何来重写这些方法。

### 第10节 当重写方法equals时，遵循的一般约定
* equals用来表示两个对象就是同一个（默认的equals就是通过比较两个对象的地址判断）
    * 每一个对象都是独一无二的，比如线程 
    * 对于类没有必要来判断类的逻辑相等，比如对于Pattern，虽然可以实现对于相同的
        regex可以是实现逻辑相等，但是设计者仍然使用object的原始equals方法，认为
        客户端不需要也不想用这个特性所有没有重写。
    * 父类已经重写了equals方法，且重写的也符合本子类的使用
    * 私有类，或者是包私有的类，你可以确信类不会被外界调用，若你对风险极度敏感，害怕自己偶然调用，
        也可以对其进行重写。
        ```java
        @Override 
        public boolean equals(Object o) {
          throw new AssertionError(); // Method is never called
        }
        ```
* 重写的情况
    * 存在逻辑相等，比如值类型。equals不仅仅用于编程人员手动判等，在map的key，set的元素
        也都会使用equals。
* 重写equals必须坚持的一般原则：
    1. 反身性：对于非空引用x，用x.equals(x) == true
    2. 对称性：非空引用x，y：x.equals(y) == y.equals(x)
        * 违反对称性的例子
        ```java
        // E.g.1
        // Broken - violates symmetry!
        public final class CaseInsensitiveString {
           private final String s;
           public CaseInsensitiveString(String s) {
               this.s = Objects.requireNonNull(s);
           }
           // Broken - violates symmetry!
           @Override public boolean equals(Object o) {
               if (o instanceof CaseInsensitiveString)
                   return s.equalsIgnoreCase(((CaseInsensitiveString) o).s);
               if (o instanceof String) // One-way interoperability!
                   return s.equalsIgnoreCase((String) o);
               return false;
           }
           //... // Remainder omitted
        }
        // CaseInsensitiveString对String为真，但是反过来就不成立
        
        // E.g.2
        public class Point {
            private final int x;
            private final int y;
        
            public Point(int x, int y) {
                this.x = x;
                this.y = y;
            }
        
            @Override
            public boolean equals(Object o) {
                if (!(o instanceof Point))
                    return false;
                Point p = (Point) o;
                return p.x == x && p.y == y;
            }
        }
        
        public class ColorPoint extends Point {
            private final Color color;
        
            public ColorPoint(int x, int y, Color color) {
                super(x, y);
                this.color = color;
            }
        
            // Broken - violates symmetry!
            @Override
            public boolean equals(Object o) {
                if (!(o instanceof ColorPoint))
                    return false;
                return super.equals(o) && ((ColorPoint) o).color ==
                        color;
            }
            // 造成子类对象与父类实例判等的结果不等于父类实例与子类实例的判等
        }
        ```  
    3. 传递性：非空引用：x，y，z：若x.equals(y) == y.equals(z) == true, then x.equals(z) == true
        * 示例
        ```java
        // 使用上面的Point
        // Broken - violates transitivity!
        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Point))
                return false;
            // If o is a normal Point, do a color-blind comparison
            // 允许了ColorPointer与Pointer比较，但是丢失了ColorPointer的特征
            if (!(o instanceof ColorPoint))
                return o.equals(this);
            // o is a ColorPoint; do a full comparison
            return super.equals(o) && ((ColorPoint) o).color == color;
        }
        // 造成的结果就是ColorPointer1与Pointer为true，Pointer与ColorPointer2为真，但是ColorPointer1与
        // ColorPointer2为假，破坏传递性（虽然其提供了对称性）
        ```
    4. 一致性：非空引用：x，y：第一次x.equals(y) == 第i次x.equals(y) (x, y do not change)
        * 不管一个类是否是可修改的，不要去写一个依赖于不可靠资源的equals方法（比如依赖于IP的URL类，由于存在一个主机可能发生
            IP变化的问题）
    5. 非空x：x.equals(null) == false
    * 记住一定不要去违反这些规则，因为对象不是孤岛，违反了总会一天出现错误。
    * **重要结论**
        * 对一个可实例化的类，若对其子类添加新的数值字段，则无法保证equals的重写约定
            * 要么会破坏其对称性（子不等于父，父等于子），要么就会破坏传递性（子1等于父，父等于子2，但是子1不等于子2）
            * 可能可以通过getClass判断的手段来完成，但是它会破坏继承的特性    
            * 故没有较好的手段来完成一个可实例化的类，若对其子类添加新的数值字段，保证equals的重写约定
            * 较好的方式就是使用组合而不是继承，比如将ColorPoint声明为含有两个字段的一个类，一个是Point，一个是Color
        * 由于java.sql.Timestamp类是继承拓展了java.util.Date类并添加了一个nanoseconds字段，使得**Timestamp类存在一个
            违反对称性的问题（Timestamp.equals(Date)==false但是Date.equals(Timestamp)==true）**，
            所以在开发中使用请分开使用这两个类，不要把Timestamp作为Date来用。  
#### 建议一个equals的方法的步骤
1. 使用==来测试是否为同一引用
2. 使用instanceof来判断类型
3. 将o cast为目标类型
4. 对于这个类的每一个重要的字段，检查其是否匹配另一个对象的对应字段 
5. 自己检查一下是否满足对称性，传递性，一致性

#### 注意
* 当重写equals方法的同时一般总是同时也重写hashCode
* 不要太追求"绝对的"相等，这样做往往惹火上身，比如由于存在链接的问题，多个文件底层实际指的是同一个文件，
    但是在java File中，并没有将多个链接文件视为同一文件。
* 不要对equals方法进行重载（也就是equals方法的参数一定要是Object类型），比如写一个名为equals(MyClass clazz),这样做的后果就是常常这个方法效果不好，
    * 原因就是：当子类继承这个方法的时候，若不重写这个方法其实就存在与参数为Obj的方法重复，更重要的是子类使用这个方法常常结果不对   
