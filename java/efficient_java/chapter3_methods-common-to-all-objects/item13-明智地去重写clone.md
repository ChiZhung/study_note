### 第13节 明智地去重写clone方法

Cloneable接口被拓展作为mixin接口（mixin类/接口就是给目标类注入额外的方法，目的就是代码重用）
实现这个接口就是表明这个类可以克隆，不幸的就是，它并没有实现它的目的。主要的缺陷就是其缺少一个clone方法，
而obj的clone方法是protected的，所以你不能通过对象调用clone方法

#### 如何实现一个有着较好行为的clone方法
* 若一个类实现了一个Cloneable接口，则这个类的clone方法就会返回一个类一个拷贝（根据字段复制），
    否则，调用clone方法就会抛出一个克隆不支持异常。Cloneable接口是一个接口的典型使用，通常来说，
    实现一个接口就是告诉一个类它可以给cli提供些什么，在这个例子中，它修改了在父类中一个protected方法
    的行为。
* 尽管没有说明，但是在实践中，一个实现了Cloneable接口的类，它会提供一个public的clone方法。为了完成这，
    这个类以及其所有超类必须遵循一个复杂的，无法执行的，轻薄的文档式协议。结果就是导致这样的实现机制过于脆弱，
    危险以及语言外的：它创造了一个没有调用构造器。对于clone来说，其一般约定是很弱的，一般的约定就是：
    x.clone != x, but x.clone.getClass() == x.getClass() and x.clone().equals(x) == true,但是
    以上约定不是必须的。
* 习惯上，一个obj返回clone方法，这个方法应该包含super.clone(),若本obj以及所有父类都遵循这条约定，则
    会满足x.clone().getClass() == x.getClass()
* 习惯上，一个对象的克隆应该跟这个对象彼此独立，为了达到这个目的，就不得不实行深拷贝，也就是对本obj的字段
    在从super.clone()返回后，对其进行修改，然后再返回。
    这个机制有点像构造器链。
    * 若一个subclass其clone方法仅仅是调用自身的构造器，则可以不用去调用super.clone
    * 当一个final类有一个clone方法，这个clone方法不调用super.clone，这种情况则本class没有必要去实现Cloneable接口，
        因为它不依赖与Object's的clone实现（**表明一个类不调用super.clone则不必实现Cloneable接口**）
* 假如本类的父类又一个较好的clone实现，则对本类而言，本类的clone方法首先调用super.clone方法，则这个对象将
    在功能上完全复制。
* 注意：对于不可变class，其不应该提供一个clone方法，因为对于不可变，其应该得到重用，而不应该再对其进行拷贝，
    造成浪费的复制
* 若一个对象包含一个指向可修改对象的引用，简单的clone实现则会带来灾难性后果。因为这样的话仅仅是浅拷贝，拷贝前后的
    两个对象对其中的一个可修改的引用对象进行修改都会造成另一个对象产生修改。
* **注意的是：若对一个class进行深拷贝，则需要保证所有可变引用对象都是非final的，由于对于这些可变引用对象需要再一次拷贝赋值**
* 当设计一个类（即将来有子类来继承它）用于继承，你有两个选择：但是无论你选择哪个类，你都不应该去实现Cloneable接口，且本类应该抛出CloneNotSupportedException
    异常，仅由最终子类来处理这个异常。
    1. 通过实现一个合适的带CloneNotSupportedException抛出的clone方法，以模仿Object的行为，这就给继承它的子类
        更多的自由来实现/不实现Cloneable接口，仅仅就像这些子类直接拓展Object一样。
    2. 你可能选择不去实现一个可以正常工作的clone方法，通过提供一个对clone方法的降级实现来到达子类不可以
        实现clone方法的目的。
        ```java
        // 使得子类无法重写clone方法
        @Override
        protected final Object clone() throws CloneNotSupportedException {
           throw new CloneNotSupportedException();
        }
        ```
* 若你实现一个线程安全的实现了Cloneable的类，则其clone方法就应该标记为synchronized，
    所有实现了Cloneable接口的类，都应该重写其clone方法，让其返回自身类型，并为public。这个方法
    首先应该调用super.clone，然后调整那些需要调整的字段。由于可能存在递归的调用字段的clone方法，
    所以这不是最好的方法。若一个类包含的字段都是原子类型，或者其是不可改的引用，这就使得在super.clone
    后就没有字段需要修改。但是仍然有特例，比如虽然一个字段是原子类型/不可修改的引用类型，但是其标示为一个唯一的ID，所以clone
    后仍需修改该值。
* 当一个类已经实现了Cloneable接口，则该类的子类没太有选择来实现一个有着较好行为的clone方法，为此，一般提供obkect复制代替的方法
#### clone的更好替代方案
* **对于对象复制，一个更好的方式就是提供一个复制构造器，或者一个复制工厂**
    ```java
    // copy constructor
    public MyClass(MyClass clazz) {...}
  
    // copy factory
    public static MyClass copy(MyClass clazz) {...}
    ```
    * 复制构造器与复制工厂较之clone方法有诸多好处：
        1. 它不依赖于易产生风险的极度语言化的对象创建机制
        2. 实现方式自由，不像clone存在一定的约束规定
        3. 它与使用final字段不矛盾
        4. 不需要抛出不必要的检查异常
        5. 不需要强制转换
        * 更进一步，一个复制构造器/复制工厂可以使用这个类的实现接口作为其参数，就使得功能更加强大，
            变成了转化构造器/转化工厂，比如：很多Collection的实现类，都提供一个Collection/Map为单一参数的
            构造器，这就使得cli可以选择一个复制之后的实现类型，而不是强迫cli接受原始的类型，比如：
            我们想把HashMap复制到TreeMap中，我们就可以Map<X, Y> map = new TreeMap<>(hashMap);
            而clone就不能提供这个功能
#### 总结
* 鉴于Cloneable的诸多问题，新的接口不应该去继承它。一个可继承的类也不应该去实现Cloneable，而对于final类，则实现Cloneable接口无害。
* 为了提供一个复制的功能，最好就是提供一个构造器，或者工厂。**一个特例就是数组，其最好的复制方法就是调用clone**

    

    