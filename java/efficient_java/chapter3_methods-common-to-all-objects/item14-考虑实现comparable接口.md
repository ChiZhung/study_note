### 第十四节 考虑实现comparable接口
* 不像本章讨论的其他方法，compareTo方法并没有声明在Object中，这个方法就是comparable接口中唯一
    的方法。通过实现comparable接口，这使得本类的实例就有了自然排序。
* 若写的值类带有明显的自然序，时间序，你就应该实现comparable接口。
#### CompareTo方法的约定
* compareTo的约定跟equals类似，但是compareTo不能够比较不同类型的对象。
* 自反性，对称性，传递性
* 跟equals方法一样，同样无法去拓展一个可实例化类通过增加一个字段。故，若是想要为实现了
    comparable接口的类添加一个值组件，适合的做法就是不去拓展它。
* compareTo与equals
    * 比如对于BigDecimal，对于HashSet，添加一个new BigDecimal("1.0"),然后在添加一个
        new BigDecimal("1.00"),由于前后两个对象通过equals方法比较不同，故HashSet中存在
        两个元素，但是对于TreeSet，其是基于compareTo比较插入，故此时TreeSet中只有一个元素。
#### compareTo与equals的区别
* 由于Comparable接口是参数化的，compareTo方法是类型静态的，所以你不需要做类型检查或者强转参数类型，
    若参数类型不正确，编译都通不过。为了比较对象引用的字段，可以通过递归地去调用compareTo方法，若一个
    字段没有compareTo方法或者以需要一个非标准的排序，可以使用comparator。
#### 使用compareTo的注意项

在compareTo方法中使用大于小于号是冗余的，是易错的，故是不推荐的写法

* 在java8中，Comparator接口包装了一批比较器的构造方法，这些构造器可以用来实现一个compareTo方法，
    许多程序员喜欢这种方法的简易性，虽然其有一定的开销，**当使用这种方法时，为了清晰明了与简洁性，
    对与其的导入使用静态导入**
    ```java
    // Comparable with comparator construction methods
    // 对于两个对象，有多个字段需要比较
    private static final Comparator<PhoneNumber> COMPARATOR =
      comparingInt((PhoneNumber pn) -> pn.areaCode)
      .thenComparingInt(pn -> pn.prefix)
      .thenComparingInt(pn -> pn.lineNum);
      
    public int compareTo(PhoneNumber pn) {
      return COMPARATOR.compare(this, pn);
    }
    ```
* 错误示例
    ```java
    // 错误使用
    // BROKEN difference-based comparator - violates transitivity!
    static Comparator<Object> hashCodeOrder = new Comparator<>()
    {
        public int compare(Object o1, Object o2) {
          // 最可能的一个问题就是，出现数值溢出
        return o1.hashCode() - o2.hashCode();
        }
    };
  
  
    // 正确使用方法
    static Comparator<Object> hashCodeOrder = new Comparator<>()
    {
        public int compare(Object o1, Object o2) {
          return Integer.compare(o1.hashCode(), o2.hashCode());
        }
    };
  
      // 或者使用比较器
    static Comparator<Object> hashCodeOrder = Comparator.comparingInt(o -> o.hashCode());
    ```
#### 总结

当实例有着敏感的顺序，你就应该去实现comparable接口，这样就可以简单的进行排序，查找，在基于
比较的集合中使用。当使用compareTo的时候，方法中避免使用>, <加减等运算符，转而应该对于装箱的
原子类型应该使用其静态方法compare，或者使用comparator中的比较器构造方法。

