### 第73节 抛出符合本层抽象概念的异常
简单来说，就是每一层只应该抛出符合本层代码逻辑语意的异常，较高的层应该捕获较低级别的异常，并抛出可以根据较高级别的抽象加以解释的异常。这个习语被称为异常翻译：

```java
// Exception Translation
try {
    ... // Use lower-level abstraction to do our bidding
} catch (LowerLevelException e) {
    throw new HigherLevelException(...); 
}

/**
 * Returns the element at the specified position in this list.
 * @throws IndexOutOfBoundsException if the index is out of range
 * ({@code index < 0 || index >= size()}). 
 */
public E get(int index) {
    ListIterator<E> i = listIterator(index);
    try {
        return i.next();
    } catch (NoSuchElementException e) {
        throw new IndexOutOfBoundsException("Index: " + index);
    }
}
```

当低级异常可能有助于某人调试导致高级异常的问题时，需要调用一种称为异常链接的特殊形式的异常转换。低级异常(原因)传递给高级异常，高级异常提供一个访问器方法(Throwable的getCause方法)来检索低级异常：
```java
// Exception Chaining
try {
    ... // Use lower-level abstraction to do our bidding
} catch (LowerLevelException cause) { 
    throw new HigherLevelException(cause);
}
```

大多数标准异常都有可识别链的构造函数。对于不存在的异常，可以使用 Throwable 的initCause 方法设置原因。异常链接不仅允许您以编程方式(使用getCause)访问原因，而且还将原因的堆栈跟踪集成到更高级别异常的堆栈跟踪中。


总之，如果不能阻止或处理来自较低层的异常，那么就使用异常转换，除非较低层方法碰巧能够保证它的所有异常都适用于较高层。链接提供了两方面的优点：它允许您抛出适当的高级异常，同时捕获故障分析的底层原因(item 75)。
