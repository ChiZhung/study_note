### 第76节-尽力保障原子故障性

即使在一个对象在执行操作的过程中发生了故障，抛出异常之后，通常也希望该对象仍然处于定义良好的可用状态。
对于受控异常尤其如此，调用者需要从中恢复。一般来说，失败的方法调用应该使对象保持在调用之前的状态。
具有此属性的方法称为故障原子性方法（ failure-atomic）。

#### 保障原子故障性的方法
1. 设计不可变对象
2. 对于可变对象，实现故障原子性的最常见方法是在执行操作之前检查参数的有效性。
3. 与实现故障原子性密切相关的一种方法是对计算进行排序，使得那些可能失败的部分代码，安排在修改对象之前。比如对于tree map的插入，在真正插入之前会对于插入对象进行检测
4. 对对象的临时副本执行操作，并在操作完成后用临时副本替换对象的内容。比如Collections.sort方法，会首先将输入的待排序的列表copy到数组中，然后在数组中进行排序。排序成功
    然后会设置回原列表，这样就可以保证在排序失败后不会修改原列表。
5. 最后一种方法是编写恢复代码，拦截在操作过程中发生的故障，并导致对象将其状态回滚到操作开始前的状态。这种方法主要用于持久的(基于磁盘的)数据结构。

#### 不保证原子故障性的场景
虽然故障原子性通常是可取的，但它并不总是可以实现的。
例如，如果两个线程试图在没有适当同步的情况下并发地修改同一个对象，该对象可能会处于不一致的状态。因此，假设一个对象在捕获了 ConcurrentModificationException 之后仍然可用是错误的。错误是不可恢复的，因此在抛出 AssertionError 时，您甚至不需要试图保持故障原子性。

即使在可能存在故障原子性的地方，也不总是可取的。对于某些操作而言，它将显著增加成本或复杂性。
也就是说，**一般当您意识到问题的存在，通常可以轻松地实现故障原子性时才去实现故障原子性。**


#### 总结
总之，作为规则，作为方法规范一部分的任何生成的异常都应该使对象保持在方法调用之前的相同状态。
在违反此规则的地方，API文档应该清楚地指出对象将处于什么状态。不幸的是，现有的大量API文档都不能满足这一理想。