### 第70节 对可恢复条件使用检查异常，对编程错误使用运行时异常
Java提供了三种 Throwable：检查异常、运行时异常和错误。程序员对于什么时候适合使用每种throwable 有一些混淆。
虽然决定并不总是明确的，但有些一般性的规则可以提供强有力的指导。

决定是否使用检查异常或未检查异常的基本规则是：对调用者可以合理地期望恢复的条件使用检查异常。通过抛出检查异常，可以强制调用者在catch子句中处理异常，或者向外传播异常。因此，方法声明要抛出的每个检查异常都有力地向API用户表明，关联的条件是调用该方法的可能结果。
有两种未检查的可掷性:运行时异常和错误。它们在行为上是相同的:它们都是不需要也通常不应该被捕获的可抛掷对象。

如果程序抛出未检查的异常或错误，通常情况下恢复是不可能的，继续执行将弊大于利。

**如果您认为某个条件可能允许恢复，则使用已检查的异常;如果没有，使用运行时异常。如果不清楚是否可以恢复，您最好使用未检查异常，原因见第71节。**

虽然Java语言规范并不要求这样做，但是有一个严格的约定，即 JVM 使用 errors 以表示资源不足、不变故障或其他使其无法继续执行的条件。
鉴于这种约定几乎被普遍接受，最好不要实现任何新的错误子类。因此，您实现的所有未检查的 throwables 都应该(直接或间接)继承 RuntimeException。
不仅不应该定义错误的 Error 子类，而且除了AssertionError 之外，也不应该抛出它们。

API设计人员经常忘记，异常是可以定义任意方法的成熟对象。这类方法的主要用途是提供带有关于引发异常的条件的附加信息的捕获异常的代码。


由于已检查的异常通常表示可恢复条件，因此可以利用它们提供的信息来帮助调用者从异常条件中恢复。例如，当使用礼品卡进行购买，由于资金不足而失败时，会抛出一个检查异常。
异常应该提供一个访问器方法来查询差额的数量。这将使调用者能够将数量转发给购物者。关于这个话题的更多信息见第75节。

#### Summery
总而言之，对于可恢复条件抛出检查过的异常，对于编程错误抛出未检查过的异常。当有疑问时，抛出未检查的异常。不要定义任何既不是检查异常也不是运行时异常的可掷性。为已检查异常提供方法以帮助进行恢复。

