### 第74节 在文档中记录方法抛出的所有异常
对方法抛出的异常的描述是该方法文档的重要部分。因此，花时间仔细记录每个方法抛出的所有异常是非常重要的(item 56)。

我们应当声明已检查的异常，并使用 Javadoc @throws标记精确记录抛出每个异常的条件。
不要采取这样的捷径：声明一个方法抛出它可以抛出的多个异常类的某个超类。
作为一个极端的例子，不要声明一个公共方法会抛出异常，或者更糟的是，抛出 Throwable。
除了拒绝向用户提供任何关于方法能够抛出异常的指导之外，这样的声明还会极大地阻碍方法的使用，因为它掩盖了在同一上下文中可能抛出的任何其他异常。
这个建议的一个例外是 main 方法，它可以安全地声明为抛出 Exception，因为它只被VM调用。


虽然 Java 不要求程序员声明方法能够抛出的未检查异常，但是明智的做法是像记录已检查异常一样仔细地记录它们。