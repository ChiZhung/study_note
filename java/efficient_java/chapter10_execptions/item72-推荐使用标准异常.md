### 第72节 推荐使用标准异常
复用标准异常有几个好处。其中最主要的一点是，它使您的 API 更容易学习和使用，因为它符合程序员已经熟悉的既定约定。
其次，使用您的API的程序更容易阅读，因为它们不会充斥着不熟悉的异常。最后(也是最不重要的)，更少的异常类意味着更小的内存占用和更快的类load加载。

#### 常见的抛出几种Exception
可以论证的是，每个错误的方法调用都归结为非法的参数（抛出IllegalArgumentException）或状态（抛出IllegalStateException），但是其他例外通常用于某些类型的非法参数和状态。
**如果调用者在某些禁止 null 值的参数中传递 null，则此时约定应当抛出NullPointerException，而不是 IllegalArgumentException。**
同样，**如果调用者在表示索引的参数中传递一个超出范围的值到序列中，则应该抛出 IndexOutOfBoundsException，而不是抛出 IllegalArgumentException。**

另一个可重用异常是 ConcurrentModificationException。如果为单个线程(或具有外部同步)使用而设计的对象检测到正在并发地修改它，则应该抛出它。这个异常充其量只是一个提示，因为不可能可靠地检测并发修改。

最后一个需要注意的标准异常是 UnsupportedOperationException。这是在对象不支持尝试的操作时抛出的异常。它的使用很少，因为大多数对象都支持它们的所有方法。如果类无法实现由其实现的接口定义的一个或多个可选操作，则使用此异常。例如，如果有人试图从列表中删除元素，仅添加的列表实现将抛出此异常。


#### 注意点
**不要直接在你的代码中使用 Exception, RuntimeException, Throwable, 或 Error**。
将这些类视为抽象类，您无法可靠地测试这些异常，因为它们是方法可能抛出的其他异常的超类。

虽然这些是目前为止最常见的重用异常，但在环境允许的情况下可以重用其他异常。
例如，如果您正在实现诸如复数或有理数等算术对象，那么重用 ArithmeticException 和NumberFormatException 将是合适的。
如果一个异常符合您的需要，那么就继续使用它，但前提是抛出它的条件必须与异常的文档一致：重用必须基于文档化的语义，而不仅仅是名称。
另外，如果你想添加更多的细节，可以随意地子类化一个标准异常(item 75)，但是记住异常是可序列化的(item 12)。这就是没有充分理由不编写自己的异常类的原因。



