### 第52节 谨慎的使用重载（OVERLOADING）
```java
// Broken! - What does this program print?
public class CollectionClassifier {
  public static String classify(Set<?> s) {return "Set"; }
  public static String classify(List<?> lst) { return "List";}
  public static String classify(Collection<?> c) { return "Unknown Collection";}
  public static void main(String[] args) { 
    Collection<?>[] collections = { 
      new HashSet<String>(),
      new ArrayList<BigInteger>(),
      new HashMap<String, String>().values()
    };
    for (Collection<?> c : collections) 
      System.out.println(classify(c));
  } 
}
```
这个程序的行为是违反直觉的，因为 overloaded 的选择是静态的，而 override 的选择是动态的。涉及到静态解析与动态分派。

假设需要一个静态方法，修复 CollectionClassifier 程序的最佳方法是用一个执行显式instanceof 测试的方法替换这三个 classification 重载:
```java
public static String classify(Collection<?> c) { 
  return c instanceof Set ? 
            "Set" 
            : c instanceof List ? 
              "List" 
              : "Unknown Collection"; 
}
```

**一个安全、保守的策略是永远不导出具有相同数量参数的两个重载。** 如果一个方法使用了 varargs，保守策略是根本不重载它，除非如 item 53 所述。
如果您坚持这些限制，程序员将永远不会怀疑哪个重载适用于任何一组实际参数。例如，考虑 ObjectOutputStream 类。
对于每个基本类型和几个引用类型，它都有一个不同的写方法。这些变量都有不同的名称，如 writeBoolean(boolean)、writeInt(int) 和 writeLong(long)，
而不是重载 write 方法。与重载相比，这种命名模式的另一个好处是可以提供具有相应名称的read 方法，例如 readBoolean()、readInt() 和 readLong()。实际上，ObjectInputStream 类确实提供了这样的读取方法。
 
对于构造方法，您没有使用不同名称的选项：一个类的多个构造函数总是重载的。在很多情况下，你可以选择导出静态的工厂而不是构造方法(item 1)

```java
public class SetList {
  public static void main(String[] args) {
    Set<Integer> set = new TreeSet<>(); 
    List<Integer> list = new ArrayList<>();
    for (int i = -3; i < 3; i++) {
      set.add(i);
      list.add(i); 
    }
    for (int i = 0; i < 3; i++) { 
      set.remove(i);  // 调用重载的Collection::remove(T)
      list.remove(i); // 调用List::remove(int)
    }
    System.out.println(set + " " + list); 
  }
}
// 注意同参数数量的重载
// 换句话说，向语言中添加泛型和自动装箱破坏了列表接口
// 但是这个故事清楚地表明，自动装箱和泛型增加了重载时谨慎的重要性
```
不要重载方法来将不同的功能接口放在相同的参数位置，通常最好避免使用具有相同数量参数的多个签名的方法重载。
                                    
