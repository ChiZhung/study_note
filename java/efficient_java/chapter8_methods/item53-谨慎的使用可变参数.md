### 第53节 谨慎的使用可变参数

在定一个带可变参数的方法时，尽量去设计最少需要1个参数，而不是最少0个参数的方法，因为一般0个参数的方法，一般需要对可变参数进行运行时check。

**在性能关键的情况下使用 varargs 时要小心。每次调用 varargs 方法都会导致数组分配和初始化。**
如果你已经从经验上确定你负担不起这个成本，但是你需要 varargs 的灵活性，有一种模式可以让你鱼与熊掌兼得。假设您已经确定95%的方法调用有三个或更少的参数。然后声明该方法的5次重载，每次重载0到3个普通参数，当参数数量超过3个时使用一个 varargs 方法:
```java
public void foo() { }
public void foo(int a1) { }
public void foo(int a1, int a2) { }
public void foo(int a1, int a2, int a3) { }
public void foo(int a1, int a2, int a3, int... rest) { }
```
EnumSet 的静态工厂使用这种技术将创建 enum 集的成本降到最低。