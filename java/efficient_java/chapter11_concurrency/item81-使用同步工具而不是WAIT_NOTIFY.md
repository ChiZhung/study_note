### 第81节 使用同步工具而不是wait and notify
自 Java 5 以来，该平台提供了更高级别的并发实用程序，可以完成以前必须在 wait 和 notify 之上手工编写的那些事情。考虑到正确使用等待和通知的困难，您应该使用更高级的并发实用程序。

java.util.concurrent 中的高级实用工具分为三类：
1. executor
2. concurrent collection
3. synchronizer

对于像ConcurrentHashMap这些内部实现了单操作并发安全的集合，它仅仅只保证单个操作是线程安全的，
但同时，在实现上，将几个常见的原语组合成单个原子操作，比如：putIfAbsent(key, value) 

同步器是允许线程彼此等待的对象，允许它们协调自己的活动。最常用的同步器是 CountDownLatch 和 Semaphore。比较不常用的是 CyclicBarrier 和 Exchanger。最强大的同步器是 Phaser 。

* CountDownLatch
  * 一次性使用的栅栏，允许一个或多个线程等待一个或多个其他线程做某事。CountDownLatch 的唯一构造函数接受一个int 类型，它表示在允许所有等待线程继续执行之前，必须在 countDown 上调用倒计时方法的次数。
  * 场景：一个任务需要多个线程同时工作，计算所有worker线程ready，到所有线程均完成时之间的耗时
  
    ```java
        // Simple framework for timing concurrent execution
        public static long time(Executor executor, int concurrency, Runnable action) throws InterruptedException {
        CountDownLatch ready = new CountDownLatch(concurrency);
        CountDownLatch start = new CountDownLatch(1);
        CountDownLatch done = new CountDownLatch(concurrency);
        for (int i = 0; i < concurrency; i++) {
        executor.execute(() -> {
        ready.countDown(); // Tell timer we're ready
        try {
        start.await(); // Wait till peers are ready
        action.run();
        } catch (InterruptedException e) {
        Thread.currentThread().interrupt();  
        } finally {
        done.countDown(); // Tell timer we're done
        }
        });
        }
        ready.await(); // Wait for all workers to be ready
        long startNanos = System.nanoTime();
        start.countDown(); // And they're off!
        done.await(); // Wait for all workers to finish
        return System.nanoTime() - startNanos;
        }
    ```
    还有一些细节值得注意。传递给 time 方法的执行程序必须允许创建至少与给定并发级别相同的线程，否则测试将永远不会完成。这就是所谓的线程饥饿死锁


对于是使用notify还是notifyAll，有时有人说，应该始终使用 notifyAll。这是一个合理而保守的建议。它总是会产生正确的结果，因为它确保您将唤醒需要被唤醒的线程。您也可以唤醒其他一些线程，但这不会影响程序的正确性。这些线程将检查它们正在等待的条件，如果发现该条件为假，将继续等待。
**作为一种优化，如果等待集中的所有线程都在等待相同的条件，并且每次只有一个线程可以从条件变为真中获益，那么可以选择调用 notify 而不是 notifyAll。**
即使满足了这些前提条件，也可能有理由使用 notifyAll 代替 notify。就像在循环中放置等待调用可以防止公共可访问对象上的意外或恶意通知一样，使用 notifyAll 代替 notify 可以防止不相关线程的意外或恶意等待。否则，这样的等待可能会“吞下”一个重要通知，让预定收件人无限期地等待。


#### 总结
总之，与 java.util.concurrent 提供的高级语言相比，直接使用 wait 和 notify 就像是在“并发汇编语言”中编程。在新代码中很少有理由使用wait 和 notify 。如果您维护使用 wait 和 notify 的代码，请确保它始终使用标准习语从 while 循环中调用 wait。
通常应该优先使用 notifyAll 方法，而不是 notify。如果使用 notify，则必须非常小心，以确保活性。



