### 第82节 使用文档记录线程安全
* 线程安全级别
  * 不可变，这个类的实例看起来是不变的。不需要外部同步。示例包括String、Long和BigInteger
  * 无条件线程安全的类的实例是可变的，但是类有足够的内部同步，它的实例可以并发使用而不需要任何外部同步。示例包括 AtomicLong 和 ConcurrentHashMap。
  * 有条件线程安全，类似无条件线程安全，除了一些方法需要外部同步来安全并发使用。示例包括 Collections.synchronized ，其迭代器需要外部同步。
  * 非线程安全的，实例是可变的。要并发地使用它们，客户端必须将每个方法调用(或调用序列)与客户端选择的外部同步放在一起。示例包括通用的集合实现，比如ArrayList 和 HashMap。
  * 线程不安全的，这个类对于并发使用是不安全的，即使每个方法调用都被外部同步包围。线程敌意通常是由于修改静态数据而没有同步造成的。没有人是故意编写线程敌对类的；此类类通常是由于没有考虑并发性而产生的。当一个类或方法被发现是线程不相容的，它通常是固定的或不赞成使用的。

注意锁对象一定要声明为final