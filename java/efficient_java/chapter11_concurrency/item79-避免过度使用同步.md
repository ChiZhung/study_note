### 第79节 避免过度使用同步

根据不同的情况，过度的同步可能会导致性能下降、死锁甚至不确定性行为。

为了避免活动性和安全性故障，永远不要在同步方法或同步块中将控制权交给客户端。
**换句话说，在同步区域内，不要调用被设计为要重写的方法，或者客户端以函数对象的形式提供的方法**。

见下面的代码
```java
// 一个观察者模式
// 观察元素的加入时候触发notify
class ObservableSet<E> extends HashSet<E> {
    ObservableSet(Set<? extends E> set) {
        super(set);
    }

    @FunctionalInterface
    public interface SetObserver<E> {
        void added(ObservableSet<E> observableSet, E elem);
    }

    List<SetObserver<E>> observers = new ArrayList<>();

    // 增加同步来阻止并发修改（不好的做法）
    public synchronized void addObserver(SetObserver<E> observer) {
        observers.add(Objects.requireNonNull(observer));
    }

    // 增加同步来阻止并发修改（不好的做法）
    public synchronized void removeObserver(SetObserver<E> observer) {
        observers.remove(observer);
    }

    // 增加同步来阻止并发修改（不好的做法）
    synchronized void notifyElementAdded(E elem) {
        for (SetObserver<E> observer : observers) {
            observer.added(this, elem);
        }
    }

    @Override
    public boolean add(E e) {
        if (super.add(e)) {
            notifyElementAdded(e);
            return true;
        }
        return false;
    }
}
```

1. 情境一，在触发通知的时候，尝试remove部分观察者
```java
public class Test {

    public static void main(String[] args) {

        ObservableSet<Integer> set = new ObservableSet<>(new HashSet<>());
        set.addObserver(new ObservableSet.SetObserver<Integer>() {
            @Override
            public void added(ObservableSet<Integer> observableSet, Integer elem) {
                System.out.println(elem);
                if (elem == 5) {
                    observableSet.removeObserver(this);
                }
            }
        });

        IntStream.rangeClosed(1, 5).forEach(set::add);
    }
}

// 实际情况是在执行observableSet.removeObserver(this);会触发并发修改异常，主要原因就是在通知所有观察者的时候
// 是以迭代器的方式进行遍历所有观察者特定的处理方法，而处理方法里面又含有对于列表元素remove，从而触发并发修改异常

// 那我在另一个线程中remove？（bad idea）
```
2. 在情形一的基础上进行修改，通过另外一个线程来进行remove
```java
public class Test {

    public static void main(String[] args) {

        ObservableSet<Integer> set = new ObservableSet<>(new HashSet<>());
        set.addObserver(new ObservableSet.SetObserver<Integer>() {
            @Override
            public void added(ObservableSet<Integer> observableSet, Integer elem) {
                System.out.println(elem);
                if (elem == 5) {

                    ExecutorService executor = Executors.newSingleThreadExecutor();
                    try {
                        executor.submit(() -> {
                            observableSet.removeObserver(this);
                        }).get();
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        executor.shutdown();
                    }
                }
            }
        });

        IntStream.rangeClosed(1, 5).forEach(set::add);
    }
}

// 最终导致死锁
```

注意：在一个同步块中调用其类的方法的时候，那么这部分代码保护的临界资源将会失去一致性，通常的做法就是将其他类的方法调用移除同步代码块，同时对于
临界资源使用副本模式

```java
void notifyElementAdded(E elem) {
    List<SetObserver<E>> snapshot = null;
    synchronized (this) {
        snapshot = new ArrayList<>(observers);
    }
    // 使用副本
    // 使用副本
    // 消除并发修改异常以及，没有死锁
    for (SetObserver<E> observer : snapshot) {
        observer.added(this, elem);
    }
}
```

实际上，有一种更好的方法可以将其他类方法调用移出同步块。标准库提供了一个称为CopyOnWriteArrayList 的并发集合(item 81)，它是为此目的而定制的。
这个列表实现是 ArrayList 的一个变体，其中所有修改操作都是通过对整个底层数组进行新复制来实现的。因为从来没有修改过内部数组，所以迭代不需要锁，而且非常快。

在大多数情况下，CopyOnWriteArrayList 的性能会非常糟糕，但是它对于观察者列表来说非常完美，观察者列表很少被修改，而且经常被遍历。

```java
List<SetObserver<E>> observers = new CopyOnWriteArrayList<>();

public void addObserver(SetObserver<E> observer) {
    observers.add(Objects.requireNonNull(observer));
}

public void removeObserver(SetObserver<E> observer) {
    observers.remove(observer);
}

void notifyElementAdded(E elem) {
    // 不会有并发异常
    for (SetObserver<E> observer : observers) {
        observer.added(this, elem);
    }
}
```

在同步区域之外调用的外来方法称为 open call，除了防止失败之外，open call 还可以极大地提高并发性。一个陌生的方法可能运行任意长的周期。如果从同步区域调用了异类方法，那么其他线程将不必要地拒绝访问受保护的资源。

应该在同步区域内做尽可能少的工作。获取锁，检查共享数据，必要时转换它，并删除锁。如果您必须执行一些耗时的活动，请找到一种方法将其移出同步区域，而不违反 item 78 中的指导原则。

过度同步的另一个潜在代价是，它会限制虚拟机优化代码执行的能力。

####编写可变并发安全类的注意点时
如果您正在编写一个可变类，您有两个选择：
* 如果需要并发使用，您可以省略所有同步并允许客户端在外部同步
* 或者您可以在内部同步，使类线程安全(item 82)。

只有当使用内部同步可以实现比让客户端在外部锁定整个对象高得多的并发性时，才应该选择后一个选项。java.util (过时的Vectorand Hashtable 除外) 采用前一种方法（bad idea），
   而 java.util.concurrent 的方法则采用第二种方法。

**当编写一个可变类保证同步产生疑惑的时候，就不要去使用同步，而是文档说明它不是线程安全的**

如果在内部同步类，可以使用各种技术来实现高并发性，比如锁分割、锁分段和非阻塞并发性控制。这些技术超出了本书的范围，但是它们在其他地方被讨论。


如果一个方法修改了一个静态字段，并且有可能从多个线程调用该方法，那么您必须在内部同步对该字段的访问(除非类能够容忍非确定性行为)。


#### Summery
总之，为了避免死锁和数据损坏，永远不要从同步区域内调用异类方法。更进一步，将您在同步区域内所做的工作量保持在最小。在设计可变类时，请考虑它是否应该进行自己的同步。在多核时代，不过度同步比以往任何时候都更重要。只有在有很好的理由时才在内部同步类，并清楚地记录您的决定(item 82)。

