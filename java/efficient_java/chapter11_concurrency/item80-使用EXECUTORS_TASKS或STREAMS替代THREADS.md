### 第80节 使用executors，tasks，streams来代替threads
对于ExecutorService，比如其ExecutorService::submit来提交一个任务，然后通过Future::get方法来同步得到结果；
通过ExecutorService::invokeAny提供一个任务列表，当任意一个执行完了，则方法返回；
通过ExecutorService::invokeAll提供一个任务列表，当所有任务执行完了，则方法返回；

#### 如何选择executor
* 对于一个小程序，或者一个轻负载的服务器，Executors.newCachedThreadPool 通常是一个不错的选择，因为它不需要配置，而且通常“做正确的事情”。
* 在高负载的生产服务器中，最好使用 Executors.newFixedThreadPool，它为您提供一个具有固定数目线程的池，或者直接使用ThreadPoolExecutor 类，以实现最大的控制。


#### 对于thread与executor
您不仅应该避免编写自己的工作队列，而且通常应该避免直接使用线程。当您直接使用线程时，线程既充当工作单元，又充当执行工作单元的机制。

在 executor 框架中，工作单元和执行机制是分开的。关键的抽象是工作单元，即任务。
有两种类型的任务：Runnable 和它的近亲 Callable (它类似于 Runnable，只是它会返回一个值并抛出任意异常)。

执行任务的一般机制是 executor 服务。如果您从任务的角度进行思考，并让 executor 服务为您执行它们，
那么您就可以灵活地选择合适的执行策略来满足您的需求，并在需求发生变化时更改策略。本质上，Executor 框架执行的功能与集合框架执行聚合的功能相同。

在 Java 7 中，Executor 框架被扩展为支持 fork-join 任务，这些任务由一种称为 fork-join 池的特殊类型的执行器服务运行。
fork-join任务, 由 ForkJoinTask 实例表示，可能分成更小的子任务，以及线程组成 ForkJoinPool 不仅处理这些任务，
还会“偷”任务从一个另一个，以确保所有线程保持忙碌，提高CPU利用率，使程序获得更高的吞吐量和更低的延迟。
编写和调优 fork-join 任务是需要技巧的。并行流(item 48)是在 fork 连接池上编写的，它允许您通过很少的努力来利用它们的性能优势，假设它们适合当前的任务。


